#!/bin/bash
# treat unset variables as an error when substituting.
set -u
# exit immediately if a command exits with a nonzero exit status.
set -e
# deploy
uv run make html
rsync -avc --force  ./build/html/ web@kb-a.kb.vpn:/home/web/repo/project/www.kbsoftware.co.uk/docs/
