Logging
*******

.. highlight:: python

I am not very happy with my logging set-up at the moment.  Need to spend time
working out how to use it properly.

Development
===========

::

  LOGGING = {
      "version": 1,
      "disable_existing_loggers": False,
      "handlers": {
          "file": {
              "level": "DEBUG",
              "class": "logging.FileHandler",
              "filename": "logger.log",
          }
      },
      "loggers": {
          "": {"handlers": ["file"], "level": "DEBUG", "propagate": True}
      },
  }

Live
====

.. warning:: Thus you should be very careful using
             'disable_existing_loggers': True; it's probably not what you want.
             https://docs.djangoproject.com/en/3.2/topics/logging/#configuring-logging

This is my set-up at the moment in ``base.py``::

  LOGGING = {
      "version": 1,
      "disable_existing_loggers": False,
      "formatters": {
          "standard": {
              "format": "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
              "datefmt": "%d/%b/%Y %H:%M:%S",
          }
      },
      "handlers": {
          "logfile": {
              "level": "DEBUG",
              "class": "logging.handlers.RotatingFileHandler",
              "filename": os.path.join(
                  get_env_variable("LOG_FOLDER"),
                  "{}-{}-logger.log".format(
                      get_env_variable("DOMAIN").replace("_", "-"),
                      get_env_variable("LOG_SUFFIX"),
                  ),
              ),
              "maxBytes": 100000000,
              "backupCount": 10,
              "formatter": "standard",
          },
          "console": {
              "level": "INFO",
              "class": "logging.StreamHandler",
              "formatter": "standard",
          },
      },
      "loggers": {
          "django": {"handlers": ["console"], "propagate": True, "level": "WARN"},
          "django.db.backends": {
              "handlers": ["console"],
              "level": "DEBUG",
              "propagate": False,
          },
          "": {"handlers": ["console", "logfile"], "level": "DEBUG"},
      },
  }

.. tip:: See :doc:`dev-dramatiq` for ``DOMAIN``, ``LOG_FOLDER`` and ``LOG_SUFFIX``.

To add logging to a module::

  import logging

  # after imports
  logger = logging.getLogger(__name__)

  # in the code
  logger.critical(
      'payment check: invalid {} != {}'.format(
          payment_pk, payment.pk,
  ))
