fail2ban
********

- :doc:`sys-wordpress-security`
- `Digital Ocean - Fail2ban Service Settings`_

Installation
============

Install using salt by adding the following to the server (or server group) 
definition in top.sls::

  - config.fail2ban

View Status
===========

Since ubuntu 20.04 does not provide a method to show all jails we provided a
script to do this it's installed using salt - to use type::

  f2b-list

The standard fail2ban method to list jails is::

  sudo fail2ban-client status

To see all banned IPs on Ubuntu 22.04 (``fail2ban`` 0.11.2 and above, displayed
in pseudo json format)::

  sudo fail2ban-client banned

For older versions of Ubuntu e.g. 20.04 it's only possible to list individual
jails (this is still available on 22.04 and provides a little more
information than the ``banned`` option)::

  sudo fail2ban-client status <jail name>

E.g::

  sudo fail2ban-client status nginx-wp-login
  sudo fail2ban-client status nginx-wp-xmlrpc
  sudo fail2ban-client status nginx-4xx

Log
===

The log for ``fail2ban`` is ``/var/log/fail2ban.log``


.. _`Digital Ocean - Fail2ban Service Settings`: https://www.digitalocean.com/community/tutorials/how-fail2ban-works-to-protect-services-on-a-linux-server
