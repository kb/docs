PHP - Development
*****************

.. highlight:: bash

Links

- `How To Install PHP and a Development Environment on Ubuntu 22.04`_
- `How to install php without Apache webserver?`_

Install
=======

Install PHP::

  # install this first - or ``php`` will auto-install Apache
  apt install php-cgi
  apt install --no-install-recommends php

Check PHP version::

  php -v
  # PHP 8.1.2-1ubuntu2.14 (cli) (built: Aug 18 2023 11:41:11) (NTS)

Install *useful* modules::

  apt install -y php8.1-cli php8.1-common php8.1-mysql php8.1-zip php8.1-gd php8.1-mbstring php8.1-curl php8.1-xml php8.1-bcmath
  # to list the installed modules
  php -m

Composer for Dependency Management::

  curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php

  # check the hash
  HASH=`curl -sS https://composer.github.io/installer.sig`
  echo $HASH

  # compare the hash to the one on this page
  # https://composer.github.io/pubkeys.html

  # check the script is safe to run
  php -r "if (hash_file('SHA384', '/tmp/composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

  # install composer (global)
  sudo php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

  # check your composer install
  composer

.. tip:: To have separate Composer executable for each project, you can
         install it locally, on a per-project basis.
         To do this, use the command ``php /tmp/composer-setup.php`` without
         any arguments. This command will generate a ``composer.phar`` file in
         your current directory, which you can run using ``php composer.phar``.

Composer
========

Using Composer in a PHP Project::

  cd ~/dev/learn/
  mkdir learn-php-composer
  cd learn-php-composer/

  composer init

.. warning:: The ``vendor`` folder should never be committed to your version
             control system. The ``vendor`` folder only contains packages you
             have installed from other vendors.

To check for outdated packages::

  composer outdated --minor-only

And to update::

  composer update

To install a package::

  composer require --dev fakerphp/faker

.. tip:: Remove ``--dev`` if this is not only for development.

PHP Script
==========

Test PHP Script:

.. code-block:: php

  # vim hello.php
  <?php
    echo 'Hello World!';
  ?>

Run the script::

  php hello.php

phpunit
=======

Add ``phpunit`` to your project (using ``composer``)::

  composer require --dev phpunit/phpunit
  # check the version
  ./vendor/bin/phpunit --version

To run the tests (after installed ``phpunit`` using ``composer``)::

  ./vendor/bin/phpunit tests/GreeterTest.php

.. tip:: The tests script is from
         https://docs.phpunit.de/en/10.4/writing-tests-for-phpunit.html
         We can use Malcolm's repository (which works nicely)
         https://github.com/mdinsmore/phpunit-simple-example

Database
--------

I reviewed `PHPUnit, Chapter 8. Database Testing`_ and it says,

  PHPUnit assumes that the database schema with all its tables, triggers,
  sequences and views is created before a test is run. This means you as
  developer have to make sure that the database is correctly setup before
  running the suite.

  There are several means to achieve this pre-condition to database testing.

  If you are using a persistent database (not Sqlite Memory) you can easily
  setup the database once with tools such as phpMyAdmin for MySQL and re-use
  the database for every test-run.

So I think we can manually create the tables for now...

PHP
===

Class
-----

To auto-load a class i.e. no need to import it::

  spl_autoload_register

.. tip:: For further information,
         `spl_autoload_register - Register function as __autoload()`_

Logging
--------

Use ``print_r`` for logs etc.

To assign the string created by ``print_r`` to a variable::

  print_r($database, true);


.. _`How To Install PHP and a Development Environment on Ubuntu 22.04`: https://www.digitalocean.com/community/tutorials/how-to-install-php-8-1-and-set-up-a-local-development-environment-on-ubuntu-22-04
.. _`How to install php without Apache webserver?`: https://askubuntu.com/questions/1160433/how-to-install-php-without-apache-webserver
.. _`PHPUnit, Chapter 8. Database Testing`: http://www.phpunit.cn/manual/current/en/database.html
.. _`spl_autoload_register - Register function as __autoload()`: https://www.php.net/manual/en/function.spl-autoload-register.php
