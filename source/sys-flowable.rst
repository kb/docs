Flowable
********

- :doc:`dev-flowable`

.. _sys-flowable-value-too-long:

value too long
--------------

I spent several hours diagnosing a ``value too long`` error e.g::

  workflow.activiti.ActivitiError:
  'ActivitiError, 500 error: for url:
  http://localhost:8080/flowable-rest/service/runtime/tasks/10a5a39-8ea7-11ee:
  {"message": "Internal server error",
    "exception":
      ### "Error updating database. Cause: org.postgresql.util.PSQLException:
      ERROR: value too long for type character varying(255)
      ### The error may exist in org/flowable/task/service/db/mapping/entity/HistoricTaskInstance.xml
      ### The error may involve org.flowable.task.service.impl.persistence.entity.HistoricTaskInstanceEntityImpl.insertHistoricTaskInstance-Inline
      ### The error occurred while setting parameters
      ### SQL: insert into ACT_HI_TASKINST (
        ID_, REV_, TASK_DEF_ID_, PROC_DEF_ID_, PROC_INST_ID_, EXECUTION_ID_,
        SCOPE_ID_, SUB_SCOPE_ID_, SCOPE_TYPE_, SCOPE_DEFINITION_ID_,
        PROPAGATED_STAGE_INST_ID_, NAME_, PARENT_TASK_ID_, DESCRIPTION_,
        OWNER_, ASSIGNEE_, START_TIME_, CLAIM_TIME_, END_TIME_, DURATION_,
        DELETE_REASON_, TASK_DEF_KEY_, FORM_KEY_, PRIORITY_, DUE_DATE_,
        CATEGORY_, TENANT_ID_, LAST_UPDATED_TIME_
      ) values ( ?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )
      ### Cause: org.postgresql.util.PSQLException:
      ERROR: value too long for type character varying(255)"}'

- I checked the Postgres logs (:ref:`sys-postgres-logging-sql`)
  but this didn't help because the parameters are not written if the query
  fails.
- I also checked the data in the python code and none of it was over 255
  characters.

The ``documentTitle`` field was 179 characters long:

- The next task in the workflow had a fairly long ``name`` which also used the
  ``documentTitle`` e.g. ``Please review ${documentTitle} etc etc...``
- When combined, the ``name`` is too long (over 255 characters)

For more information, see https://www.kbsoftware.co.uk/crm/ticket/6958/
