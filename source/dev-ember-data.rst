Ember Data
**********

- :doc:`dev-ember`
- :doc:`dev-ember-addons-kb`
- :doc:`dev-ember-addons`
- :doc:`dev-ember-auth`
- :doc:`dev-ember-patterns`
- To test the JSON API without authentication, see
  :ref:`contact-app-test-rest-framework-json-api`

.. _ember_data_date_time:

Date / Time
===========

To format dates and times in your template, use ``format-date`` e.g::

  {{format-date
    task.created
    day="numeric"
    month="numeric"
    year="numeric"
    hour="numeric"
    minute="numeric"
  }}

JSON API
========

Create an application adapter::

  # app/adapters/application.js
  export default class ApplicationAdapter extends JSONAPIAdapter {
    @service session;

    pathForType(type) {
      /** PJK 13/02/2023, I don't think we should need this... */
      return dasherize(type);
    }

    get headers() {
      let { token } = this.session.data.authenticated;
      return { Authorization: 'Token ' + token };
    }

    host = ENV.APP.API_URL;
    namespace = ENV.APP.API_NAMESPACE;
  }

Create an application serializer::

  # app/serializers/application.js
  import JSONAPISerializer from '@ember-data/serializer/json-api';

  export default class ApplicationSerializer extends JSONAPISerializer {}

Testing
-------

To test the JSON API without authentication, see
:ref:`contact-app-test-rest-framework-json-api`

Models
======

For a Django model e.g:

.. code-block:: python

  created = models.DateTimeField()

To check the ``json`` returned by a Django API:

.. code-block:: python

  from api.tests.fixture import api_client, date_to_ember_iso

  assert {
      "count": 1,
      "next": None,
      "previous": None,
      "results": [
          {
              "id": note.pk,
              "created": date_to_ember_iso(note.created),
              "title": "Apple",
          },
      ],
  } == response.json()

The Ember model::

  created: DS.attr('date'),
