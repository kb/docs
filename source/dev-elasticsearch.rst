ElasticSearch
*************

.. highlight:: bash

See the doc:`app-search` app documentation for python code.

For building the fuzzy match code, I found the following articles very useful:

- `ElasticSearch - Searching For Human Names`_
- `Fuzzy Problem in Elasticsearch`_
- `phonetic token filter`_

Pillar
======

Create a ``search`` ``sls``:

.. code-block:: yaml

  # config/search.sls
  search:
    True

And add it to the config for the server e.g:

.. code-block:: yaml

  # top.sls
  'test-a':
    - config.search
    - sites.my

Install
=======

For a development workstation, see elasticsearch_.

Install the phoentic search plugins (See *Plugins*, *Re-Install* below).

Plugins
=======

Re-Install
----------

Elasticsearch version 6 (and later)::

  cd /usr/share/elasticsearch
  sudo bin/elasticsearch-plugin remove analysis-phonetic
  sudo bin/elasticsearch-plugin install analysis-phonetic

  # or...
  # cd /usr/share/elasticsearch/ && bin/elasticsearch-plugin remove analysis-phonetic && bin/elasticsearch-plugin install analysis-phonetic

  sudo systemctl restart elasticsearch.service

.. tip:: For more information on the Phonetic Plugins, see `Custom Analyzers`_
         and `Phonetic Analysis Plugin`_

.. warning:: The plugins will not update when a new version of ElasticSearch is
             installed by ``apt``.

.. note:: Older versions of elasticsearch (version 5 and earlier) the plugin
          maintenance program was called ``plugin`` not
          ``elasticsearch-plugin``.

List
----

::

  cd /usr/share/elasticsearch
  bin/plugin list

Background Tasks
================

See doc:`app-search`

.. _elasticsearch_version_6:

Update
======

I couldn't upgrade from an old version to 8.x and didn't need to preserve the
data, so I deleted the data folder:

1. ``apt remove elasticsearch``
2. Find the location of the data folder in ``/etc/elasticsearch/elasticsearch.yml``
3. Delete the folder.
4. ``apt install elasticsearch``
5. Recreate the Elasticsearch indexes.

As an alternative::

  apt purge elasticsearch

Update to version 6
-------------------

No handler for type [string]
----------------------------

From `No handler for type [string] declared on field [name]`_

Elasticsearch has dropped the ``string`` type and is now using ``text``, so
your code should be something like this::

  'mappings': {
      self.DOC_TYPE: {
          "properties": {
              "job_role": {
                  "type": "text",
                  "analyzer": "my_analyzer",

Plugin
------

If you cannot install a plugin because a previous version is still installed,
then remove ElasticSearch before re-installing.

Will not start (after installing an earlier version)
----------------------------------------------------

Check the logs in::

  /var/log/syslog

It seems that the config is driven from::

  /etc/default/elasticsearch

To solve this issue I ran the following commands::

  apt purge elasticsearch
  rm -r /var/lib/elasticsearch/


.. _`Custom Analyzers`: https://www.elastic.co/guide/en/elasticsearch/guide/current/custom-analyzers.html
.. _`ElasticSearch - Searching For Human Names`: http://stackoverflow.com/questions/20632042/elasticsearch-searching-for-human-names
.. _`Fuzzy Problem in Elasticsearch`: http://www.basistech.com/fuzzy-search-names-in-elasticsearch/
.. _`No handler for type [string] declared on field [name]`: https://stackoverflow.com/questions/47452770/no-handler-for-type-string-declared-on-field-name
.. _`Phonetic Analysis Plugin`: https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-phonetic.html
.. _`phonetic token filter`: https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-phonetic-token-filter.html
.. _elasticsearch: https://www.pkimber.net/howto/java/apps/elasticsearch/getting-started.html
