Captcha
*******

.. highlight:: python

For documentation, see https://github.com/praekelt/django-recaptcha

Sign up to reCAPTCHA on https://www.google.com/recaptcha/admin.

You will receive a site key and a secret key...

.. tip:: If you want to use the Captcha on your laptop from
         http://localhost:8000/, then add ``localhost`` to the list of domains.

In the salt ``sls`` file for your site, add the ``norecaptcha_site_key`` and
the ``norecaptcha_secret_key`` e.g:

.. code-block:: yaml

  norecaptcha_site_key: <your site key>
  norecaptcha_secret_key: <your secret key>

.. note:: Replace ``<your site key>`` and ``<your secret key>`` with the
          actual reCAPTCHA keys.

Add the following to ``requirements/base.txt``::

  django-recaptcha

.. tip: See :doc:`requirements` for the current version.

Add the following to ``settings/base.py``::

  THIRD_PARTY_APPS = (
      "django_recaptcha",
      # ...
  )

Add the following to ``settings/production.py``::

  # https://github.com/praekelt/django-recaptcha
  RECAPTCHA_PRIVATE_KEY = get_env_variable('NORECAPTCHA_SECRET_KEY')
  RECAPTCHA_PUBLIC_KEY = get_env_variable('NORECAPTCHA_SITE_KEY')
  # https://github.com/torchbox/django-recaptcha#recaptcha-v3-score
  RECAPTCHA_REQUIRED_SCORE = 0.85

.. tip:: For testing, do **not** set ``RECAPTCHA_PRIVATE_KEY`` or
         ``RECAPTCHA_PUBLIC_KEY``.
         Google provides test keys which are set as the default.

To add a captcha field to your form::

  from django_recaptcha.fields import ReCaptchaField
  from django_recaptcha.widgets import ReCaptchaV3

  class EnquiryForm(RequiredFieldForm):
      captcha = ReCaptchaField(widget=ReCaptchaV3)

Testing
=======

Add the following to your local settings file e.g. ``settings/dev_patrick.py``::

  SILENCED_SYSTEM_CHECKS = ["django_recaptcha.recaptcha_test_key_error"]

From `Django reCAPTCHA, Unit Testing`_::

  data = {
      "name": "Patrick",
      "captcha": "123",
      "g-recaptcha-response": "PASSED",
  }
  response = client.post(reverse("example.enquiry.create"), data)

.. tip:: Make sure to add ``captcha`` to the ``data``.

.. note:: No need to use ``os.environ["RECAPTCHA_TESTING"] = "True"`` in tests
          any more!

.. note:: ``g-recaptcha-response`` has replaced ``recaptcha_response_field``.

.. note:: For testing, do **not** set ``RECAPTCHA_PRIVATE_KEY`` or
          ``RECAPTCHA_PUBLIC_KEY``.
          Google provides test keys which are set as the default.


.. _`Django reCAPTCHA, Unit Testing`: https://pypi.org/project/django-recaptcha/#unit-testing
