ManoMano
********

Suspended Products
==================

17/10/2022, Looking at the product catalogue on ManoMano, there is a column
for *Suspended products* and a toggle switch for each product.

We have eight products in that column that are marked as *Suspended products*.
Can you give us more detail on what that means?

  From @manomano.com

  This means that you or someone from your team has manually suspended these
  products from MM (put them offline).

  You can have more info in the attached file, page 25.
  You can manually un-suspend them to put them back online.

  Important note: I do not recommend doing Manual Updates on your products,
  this will only cause confusion in your catalog as maybe some information is
  updated manually and forgotten about, then looking at the feed the
  information won't make sense, etc...
  The best way to change your product's information (price, stock) should always be your Feed.

:download:`misc/manomano/ManoMano-Sellers-Toolbox-Manual-v1.0.pdf`
