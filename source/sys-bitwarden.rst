Bitwarden
*********

After adding a *Member* in the *Admin Console* for an organisation, give the
user permission to see the default collection:

- *Collections*
- *Default collection*
- Click the three dots
- *Access*
- *Edit Collection*
- Set *Permission* to *Can edit*
- *Select groups and members*

.. image:: ./misc/bitwarden/2024-09-03-edit-collection.jpg
