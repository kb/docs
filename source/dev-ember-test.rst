Ember Testing
*************

Run in a browser using this URL,
http://localhost:4200/tests

``ember-cli-code-coverage``
===========================

- `ember-cli-code-coverage`_
- `Test Coverage in Ember Applications with CI and Code Climate Integration by Kamil Ejsymont`_

Run the tests using::

  pnpm test:ember

View the test results::

  firefox coverage/index.html

.. tip:: Check https://www.kbsoftware.co.uk/crm/ticket/6728/ on 11/03/2024
         for notes on how to configure.

MSW
===

To use MSW mocks for development, set the ``loginRoute`` to ``login-pass``::

  // front/config/environment.js
  if (environment === 'development') {
    ENV.APP.loginRoute = 'login-pass'


.. _`ember-cli-code-coverage`: https://github.com/ember-cli-code-coverage/ember-cli-code-coverage
.. _`Test Coverage in Ember Applications with CI and Code Climate Integration by Kamil Ejsymont`: https://www.netguru.com/blog/test-coverage-in-ember-applications-with-ci-and-code-climate-integration
