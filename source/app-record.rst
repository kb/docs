Record Management
*****************

https://gitlab.com/kb/record

A *record* is (something) in written, photographic, or other form.

This app will process *records* in the background e.g. convert a Word document
into a PDF file (so it can be displayed in a browser).

Test PDF Conversion
===================

The ``example`` app has a ``convert_file_to_pdf`` management command::

  django-admin.py convert_file_to_pdf ~/Downloads/fruit.doc

Example output::

  Convert document to PDF
  Created 2 files
  /home/patrick/dev/app/record/media-private/record/converted/fruit_IMd1GxG.jpg
  /home/patrick/dev/app/record/media-private/record/converted/fruit_IMd1GxG.pdf
  Convert document to PDF - Complete
