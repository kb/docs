PHP - Issues
************

No such file or directory
=========================

From `Warning: mysqli_connect(): (HY000/2002): No such file or directory`_::

  Message:  No such file or directory
  #0 /home/patrick/dev/project/my/mydatabase.php(9): mysqli->__construct()

``No such file or directory`` is actually a failed to connect to database error!

PHPUnit - ``global`` variable is ``null``
=========================================

Error ``Call to a member function safe_query() on null``::

	global $database;
	$database->safe_query("ROLLBACK");

To solve the issue, I added the following to ``site_config.php``::

  global $database;

.. warning:: ``site_config.php`` should not be added to the GIT repository.

This StackOverflow article *may help*,
`global variables are null when using PHPUnit`_


.. _`global variables are null when using PHPUnit`: https://stackoverflow.com/questions/9073144/global-variables-are-null-when-using-phpunit
.. _`Warning: mysqli_connect(): (HY000/2002): No such file or directory`: https://www.dpscomputing.com/blog/2021/02/11/warning-mysqli_connect-hy000-2002-no-such-file-or-directory/
