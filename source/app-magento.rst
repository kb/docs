Magento App
***********

.. highlight:: bash

- Click here for the :doc:`dev-magento`

Icon
====

::

  <i class="fa fa-shopping-cart"></i>

Management Commands
===================

Invoice and Credit Notes
------------------------

To download a single invoice or credit note, pass in the ``increment_id`` e.g::

  django-admin.py download_magento_credit_note 100018188
  django-admin.py download_magento_invoice 100001906

Sites
-----

Magento sites can be setup by a ``superuser`` in *Settings*, *Magento Sites*.

We have a Django management command to verify access to the API::

  django-admin.py login_magento_sites

Sync
----

To sync countries from Magento to the ``Country`` model in the ``finance`` app::

  django-admin.py magento_sync_settings

To initialise European Union Countries so they can use *Zero Rated EC VAT*::

  django-admin.py init_finance_app_european_union_countries

.. tip:: I would only run ``init_finance_app_european_union_countries`` once.
         Future updates to the countries in the European Union should probably
         be done through *Settings*, *VAT Settings*.

Templates and Mixins
====================

.. tip:: All projects which use the ``invoice`` app will provide a *detail*
         URL named ``invoice.detail``.

``InvoiceDetailMixin``
----------------------

This (Magento) app has a template which can be included to display Magento
related information for an invoice::

  <div class="pure-u-1 pure-u-md-1-2">
    <table width="100%" class="pure-table pure-table-bordered small-margin-top">
      <tbody>
        {% include "magento/_invoice_detail.html" %}
      </tbody>
    </table>
  </div>
