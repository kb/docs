Finance
*******

Notes
=====

- The ``finance`` app expects the countries to be populated by an external
  source e.g. the ``magento`` app.
- The ``finance`` app has a management command,
  ``init_european_union_countries``
  which can be run to set the European Union Countries who  can use the
  *Zero Rated EC VAT code*
  (see the ``ZERO_RATE_EC_VAT_CODE`` constant in the ``VatCode`` model)
- We decided **not** to use the ``django-countries`` app.
  It seemed a little *magical* as countries are not added to the database.
  We built our own ``Country`` model which just does the simplest thing...

Vat Codes
=========

From our finance_ app and `VAT rates on different goods and services`_::

  Code  Description         The way we use it...
  ====  ==================  ===================================================
  E     Exempt              Foreign or Exempt
  L     Legacy              For internal use on our old/legacy systems.
  S     Standard
  Z     Zero                Zero-rated goods and services
                            eg most food and children’s clothes
  ZEU   EU Zero Rated       EU customers who are registered for VAT


.. _finance: https://gitlab.com/kb/finance/blob/master/finance/models.py
.. _`VAT rates on different goods and services`: https://www.gov.uk/rates-of-vat-on-different-goods-and-services::
