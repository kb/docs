CKEditor
********

Build
=====

Browse to http://ckeditor.com/download

Click ``Online Builder`` in the ``Customize`` panel.

Choose the ``Standard`` preset.

Select the following from ``Available plugins``::

  Color Button
  Color Dialog
  justify
  Youtube Plugin

Select ``Moono`` (not ``Moono-Lisa``) from available skins.

Download the ``Optimized`` version of the CKEditor.

Remove the current ``ckeditor`` folder from the ``base`` app::

  base/static/vendor/js/ckeditor/

Extract the archive to the ``ckeditor`` folder::

  base/static/vendor/js/ckeditor

Remove the ``samples`` folder::

  rm -r base/static/vendor/js/ckeditor/samples/

Configure
=========

The ``base`` app has three standard templates for CKEditor:

+--------------------------+-----------------------------------------------+-------------------------+
| Template                 | Description                                   | JavaScript              |
+==========================+===============================================+=========================+
| ``_ckeditor.html``       | Our standard CKEditor                         | ``config_simple.js``    |
+--------------------------+-----------------------------------------------+-------------------------+
| ``_ckeditor_color.html`` | Our standard editor with the option of        | ``config_colortext.js`` |
|                          | colouring the foreground and background text. |                         |
+--------------------------+-----------------------------------------------+-------------------------+
| ``_ckeditor_user.html``  | A more limited editor                         | ``config_user.js``      |
|                          | (we use this for candidates and recruiters)   |                         |
+--------------------------+-----------------------------------------------+-------------------------+

.. note:: The Django templates are in the standard ``templates`` folder:
          ``base/templates/base/``

.. note:: The JavaScript files are in the ``base/js/ckeditor`` folder:
          ``base/static/base/js/ckeditor/``,
          https://gitlab.com/kb/base/tree/master/base/static/base/js/ckeditor

The ``_ckeditor.html`` template can be included on a page as follows:

.. code-block:: html

  {% block script_extra %}
    {{ block.super }}
    {% include 'base/_ckeditor.html' %}
  {% endblock script_extra %}

.. note:: Any block with an ID of ``id_description`` will become a rich text
          editor.

To include ``_ckeditor_color.html`` or ``_ckeditor_user.html`` replace the
``{% include 'base/_ckeditor.html' %}`` line with one of the following:

.. code-block:: html

    {% include 'base/_ckeditor_color.html' %}
    {% include 'base/_ckeditor_user.html' %}

If you don't have a text field with the ID of ``id_description``, you can
change the ID e.g::

  {% block script_extra %}
    {% include 'base/_ckeditor.html' with id_textarea='id_notes' %}
    <!-- ...

To make any other text field into rich text, add ``CKEDITOR.replace`` sections
below the ``include`` section e.g:

.. code-block:: html

  {% block script_extra %}
    {{ block.super }}
    {% include 'base/_ckeditor.html' %}
    <script type="text/javascript">
      CKEDITOR.replace('id_footer_left');
      CKEDITOR.replace('id_footer_right');
    </script>
  {% endblock script_extra %}

.. note:: The default ``config.js`` file can be found in the
          ``base/static/vendor/js/ckeditor/config.js`` folder.

Template
--------

To display the rich text on an HTML template::

  {% autoescape off %}
    {{ c.description }}
  {% endautoescape %}

.. warning:: This by-passes the Django auto-escaping and it therefore a
             security risk.  Don't display user input using this method.

License
=======

https://ckeditor.com/legal/ckeditor-oss-license/

.. warning:: We will **not** use CKEditor **5** because it is licensed under the
             terms of GNU General Public License Version 2 or later.

**CKEditor 4** is licensed under the terms of any of the following licenses at
your choice:

- GNU General Public License Version 2 or later
- GNU Lesser General Public License Version 2.1 or later
- Mozilla Public License Version 1.1 or later

I think we are fine with the *GNU Lesser General Public License* and the
*Mozilla Public License* (although I am no expert on licences).

.. note:: If you want to use a Commercial Licence, see pricing,
          https://ckeditor.com/pricing/#ckfinder-pricing

.. Further discussion at ref Licensing, CKEditor etc on ticket
.. https://www.kbsoftware.co.uk/crm/ticket/3968/

User Documentation
==================

Spell Check
-----------

Hold down the ``Ctrl`` key to view the browser spell check options.
