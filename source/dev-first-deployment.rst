Deploy a project for the first time
***********************************

.. highlight:: yaml

Generate a secret Key
=====================

:ref:`generate_secret_key`

The secret key should be added to the pillar entry for the server as::

  secret_key: <generated secret key>

Create an Opbeat App
====================

Use the opbeat wizard to create a new app

Add the APP_ID provided as::

  opbeat: <app_id provided by opbeat>

Generate a recaptcha key
========================

Log on to the google account and create a recaptcha key for the site.  Note
the site key and secret key and enter them into the pillar.


Create pillar entry
===================

Here is a sample entry for a pillar entry for a django project::

  <FQDN of the server>:
    package: <package name>
    profile: django
    redirect: <domain name (usually FQDN excluding the www)>
    celery: <True or False depending if your project uses celery>
    db_pass: <a secure password>
    db_type: psql
    norecaptcha_site_key: <recaptcha site key>
    norecaptcha_secret_key: <recaptcha secret key>
    opbeat: <app_id provided by opbeat>
    secret_key: <generated secret key>
    # initial state to create a certificate 
    ssl: False
    # letsencrypt: True
    uwsgi_port: <unique port number for server starting at 3040>
    env:
      sparkpost_api_key: <api key from sparkpost>

Add the server to the DNS for the domain
========================================

Log on to the control panel and set up A records for the <FQDN of the server>
and if approprate for the domain these correspond to the section name and
redirect entry in the pillar above usually for a live server you will have ::

  www.<your domain name>  <IP Address of server>
  <your domain name>  <IP Address of server>

User salt to create the site on the server
==========================================

Copy the pillar to the master server and test the settings::

  salt <minion name> state.apply --state-verbose=False test=True

Apply the settings::

  salt <minion name> state.apply --state-verbose=False

Release the project
===================

The normal release command does not cater for registering the package with the
devpi server so the first time project is released there are some extra steps.

Use the release script (from https://github.com/mdinsmore/dev-scripts) to
generate the package N.B this will fail with an error that the package is not
registered::

  release

To register the package with devpi run the following commands::

  devpi use <devpi repository>
  devpi login <repository section> --password <password>
  devpi upload

.. note: The repository is the same as the one listed in your .pypirc file.
         The repository section is usually the first part of the path after the
         server name.

Create a letsencrypt certificate
================================

Log on to the server and verify the nginx settings::

  sudo nginx -t

This should report::

  nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
  nginx: configuration file /etc/nginx/nginx.conf test is successful

Reload the nginx configuration::

  sudo service nginx reload

Create a certificate for the <FQDN of the server> and if appropriate the
value defined in the ``redirect`` entry of the pillar as follows::

  init-letsencrypt <FQDN of the server> <value defined in redirect entry>

e.g.::

  init-letsencrypt www.hatherleigh.info hatherleigh.info

See page on letsencrypt_ for more information

After successfully obtaining a certificate amend the entries in the pillar on
the master server. Change::

  ssl: False
  # letsencrypt: True

To::

  ssl: True
  letsencrypt: True

Now perform a ``state.apply`` to apply these settings::

  salt <minion name> state.apply --state-verbose=False

Create the database
===================

.. note:: Ensure that you have an up to date pillar on your client machine
          i.e. it should be the same as the master server.
          To use the fab command you need to setup the fabric environment
          or use the ``run-fab`` command if you have dev-scripts installed.


To create the database run the command::

  fab domain:<FQDN server name> create_db

Deploy
======

Before deploying ensure that you have a test file which lists pages to be
checked on deploy.  Create the file using the command::

  touch ~/dev/module/deploy/test/<domain>.yaml

Deploy the project using the command::

  fab domain:<domain> deploy:<versions in the format 0.0.01>

The site check at the end may fail if your init_project does not create any
pages you can add some pages and re-run the check as follows::

  fab domain:<FQDN server name> ok

Set up initial data
===================

If you have created initial data from your development environment using the
command similar to::

  django-admin dumpdata --exclude  contenttypes > project/tests/data/initial-data.json

Copy this data to the new server and run the command as user web::

  <domain>.sh loaddata <absolute-path-to-data>/initial-data.json

If this data uses pictures you'll need to copy these from your media directory
e.g. using tar::

  cd <project directory>/media
  tar -cvzf ../project/tests/data/media-files.tar.gz)

and copy to the server and run these commands as user web::

  cd ~/repo/files/<domain>/public
  tar -xczf <path-to-media-files>/media-files.tar.gz

Set up the notify address
-------------------------

Log in as a super user and navigate to the admin interface and set up the
notifys to the appropriate address.

initial Full Backup
===================

The site will automatically be backuped up overnight however to start thes
backup cycle you should now perform an initial full backup using the command::

  backup.<domain>.sh full


.. _letsencrypt: https://www.kbsoftware.co.uk/docs/ssl-letsencrypt.html
