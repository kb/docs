Backup
******

.. highlight:: bash

We are Using http://rsync.net/ for backups.

Pillar
======

To enable backups for a server, add the following to the pillar config for your
server e.g:

.. code-block:: yaml

  # top.sls
  'test-a':
    - global.gpg

05/04/2023, The backup configuration for a site has changed.
I found the following options in the ``pillar``:

.. code-block:: yaml

  # remote backup
  sites:
    backup: remote

.. code-block:: yaml

  # local (weekly) backup
  sites:
    hatherleigh_info:
      backup: local
      local_backup_weekly: True

.. code-block:: yaml

  # no backup
  sites:
    hatherleigh_info:
      backup: none

.. code-block:: yaml

  # see warning below...
  sites:
    hatherleigh_info:
      backup: local
      local_backup_weekly: False

.. warning:: I think ``local_backup_weekly: False`` and ``backup: False``
             (the old configuration) will create a new backup file every day
             potentially filling the disk.

.. tip:: For more information, see our discussion at
         https://www.kbsoftware.co.uk/crm/ticket/6515/

.. tip:: For ``local_backup_weekly: True``, the backup script will use ``%A``
         to format the date in the backup file name.
         ``%A`` is the *locale’s full weekday name (e.g. Sunday)*

Restic
======

Initialise the repository using ``fabric``::

  fab init-backup www.hatherleigh.info backup
  fab init-backup www.hatherleigh.info files

.. warning:: If your site uses ``workflow``, then I am not sure our
             ``fabfile.py`` handles this!

Getting Started
===============

- `How-To: Import/Export GPG Key Pair`_

.. note:: You will receive your account details from rsync.net.
          Please refer to the :doc:`checklist` and fill in your own details.

SSH Key
-------

Create an SSH key on your laptop::

  ssh-keygen -o -a 100 -t ed25519

.. note:: Do not enter a password here.

.. note:: The old command line was ``ssh-keygen -t rsa``.  We updated on the
          advice of this article, `Upgrade your SSH keys!`_

Upload your key to the rsync.net server::

  cat ~/.ssh/id_rsa.pub | ssh 123@usw-s001.rsync.net 'dd of=.ssh/authorized_keys oflag=append conv=notrunc'

If you are setting up the rsync.net server for the *first time ever*:

.. danger:: Do not run the following command unless you are the first person
            (or computer) to use this rsync.net server.  If you upload another
            key using this command, they will overwrite the first key.

::

  scp ~/.ssh/id_rsa.pub 123@usw-s001.rsync.net:.ssh/authorized_keys

Test your ssh login to the server::

  ssh 123@usw-s001.rsync.net ls

These instructions are copied from `Generating SSH Keys for Automated Backups`_
(up to and including *Testing Your Passwordless Login*).

.. _backup_cloud_server_gpg:

GPG Key
-------

To encrypt the backups we need a gpg key.  This key will be shared with all the
web servers and with any laptops which need to decrypt (and restore) the data.

To create the gpg key::

  gpg --gen-key

  # defaults...
  Please select what kind of key you want:
  (1) RSA and RSA (default)
  RSA keys may be between 1024 and 4096 bits long.
  What keysize do you want? (2048)
  Please specify how long the key should be valid.
  0 = key does not expire
  You need a Passphrase to protect your secret key.

Accept the defaults (as above) and enter a passphrase for your gpg key.

List the keys, and make a note of the key number (in this example, the key is
``ABCD1234``)::

  gpg --list-keys
  # --------------------------------
  # pub   2048R/ABCD1234 2014-10-30

Export the public and private keys and add them to your pillar::

  cd ~/repo/dev/module/deploy/pillar/
  gpg --armor --export ABCD1234 >> global/gpg.sls
  gpg --armor --export-secret-key ABCD1234 >> global/gpg.sls

Edit the ``global/gpg.sls`` file so it is in the following format e.g:

.. code-block:: yaml

  gpg:
    rsync.net:
      user: 123
      server: usw-s001.rsync.net
      key: ABCD1234
      public: |
        -----BEGIN PGP PUBLIC KEY BLOCK-----
        Version: GnuPG v1

        ABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCA
        -----END PGP PUBLIC KEY BLOCK-----
      private: |
        -----BEGIN PGP PRIVATE KEY BLOCK-----
        Version: GnuPG v1

        ABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCA
        -----END PGP PRIVATE KEY BLOCK-----

.. tip:: Multiline strings in YAML files are started with the ``|`` character
         and are indented two characters.

Cloud Server
------------

Log into the Salt master and update your cloud server.  Salt will do the
following tasks:

- create an ``ssh`` key
- copy the GPG keys to the ``~/repo/temp/`` folder.
- create a backup script for each site on the server
- create a cron script for each site on the server

Add the ``ssh`` keys to the rsync.net server::

  ssh server
  sudo -i -u web
  cat ~/.ssh/id_rsa.pub | ssh 123@usw-s001.rsync.net \
    'dd of=.ssh/authorized_keys oflag=append conv=notrunc'
  # enter your rsync.net password

Check that you can connect to the rsync.net server without a password::

   ssh 123@usw-s001.rsync.net ls -la

Import the GPG keys::

  ssh server
  sudo -i -u web
  gpg --import ~/repo/temp/pub.gpg
  gpg --allow-secret-key-import --import ~/repo/temp/sec.gpg

List the keys, and then mark the rsync.net key as trusted::

  gpg --list-keys
  gpg --edit-key ABCD1234
  > trust
  # Select option 5 = I trust ultimately
  > q

Do an initial ``full`` backup.  The Salt states will create a backup script in
the ``/home/web/opt/`` folder
e.g:: ``/home/web/opt/backup.hatherleigh.info.sh``.

To initialise the backup run the script with the ``full`` argument e.g::

  /home/web/opt/backup.hatherleigh.info.sh full

Usage
=====

To check the usage on your rsync.net account, follow the :ref:`rsync_usage`
instructions...


.. _`Generating SSH Keys for Automated Backups`: http://www.rsync.net/resources/howto/ssh_keys.html
.. _`How-To: Import/Export GPG Key Pair`: http://www.debuntu.org/how-to-importexport-gpg-key-pair/
.. _`Upgrade your SSH keys!`: https://blog.g3rt.nl/upgrade-your-ssh-keys.html
