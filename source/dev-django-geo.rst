GeoDjango
*********

.. highlight:: python

From the `GeoDjango Tutorial`_

.. code-block:: bash

  apt install binutils libproj-dev gdal-bin
  apt install postgresql-10-postgis-2.4

.. code-block:: bash

  service postgresql restart

Django

``settings.py``::

  DATABASES = {
      "default": {
          "ENGINE": "django.contrib.gis.db.backends.postgis",

To run the migrations, you need to be a Postgres superuser.  If you don't, you
will get this error:

.. code-block:: bash

  django.db.utils.ProgrammingError: permission denied to create extension "postgis"
  HINT:  Must be superuser to create this extension.

To solve this issue, I temporarily updated my database user to ``postgres``::

  "USER": "postgres",
  # "USER": get_env_variable("DATABASE_USER"),


.. _`GeoDjango Tutorial`: https://docs.djangoproject.com/en/2.2/ref/contrib/gis/tutorial/
