Web Fonts and favicons
**********************

Web fonts
=========

Browse for fonts on `Google Fonts`_ or `Font Squirrel`_

If only the otf fonts are available use the `Font Squirrel Generator`_ to
generate the webfonts

On this page Click Upload Fonts (seems counter intuitive to download the font
and reupload it but does not appear to be an option to create them directly)

Click "Expert"

In "Font Formats" choose the following: TrueType, WOFF, WOFF2, EOT Compressed,
SVG

In "Protection" choose "Web Only (disable Desktop use)"

In "OpenType Features" choose "Keep All Features"

In CSS change the CSS Name to "<name of your font>.css"

In Agreement click "Yes, the fonts I'm uploading are legally eligible for web
embedding."

Click "Download your kit"

Once downloaded extract the contents of the zip file.  I usually move the .html
files, the generator_config.txt and the specimen_files directory to a
sub-directory called "demo" leaving only the font files and css in the top
directory.

The generator does not change the html files to use the new name given to the
css file.  To use the demo files edit the files and change "stylesheet.css" to
"../<name of your font>.css"

You may want to edit the css file to use title case for the font family names
e.g the gererator may have called the bold version of the montserrat font
"montseratbold" it is clearer to change this to "MontserratBold" making it
easier to read.  Also change montseratregular to simply "Montserrat" (To use
the demo files you will also need to change the reference to the fonts)

To use the fonts on your page copy then to web/static/web/font and include the
line::

  <link rel="stylesheet" href="{% static 'web/font/<name of your font>.css' %}" type="text/css" charset="utf-8" />

In your style sheet include the font using the names defined in the font css
file e.g.::

  body {
    font-family: 'MontserratBold' sans-serif;
  }

Favicons
========

.. tip:: 07/03/2022, This article may be useful,
         `How to Favicon in 2022: Six files that fit most needs`_

Go to the `Favicon Generator`_ site

Click *Select your Favicon picture* and select the image you want as your
favicon (this should be square at least 260x260 in size).

A new page is displayed you can normally accept the default options with the
exception that in the *Favicon Generator Options* section  Click *I cannot
or I do not want to place favicon files at the root of my web site. Instead
I will place them here:* and enter ``/static/ico/`` in the box below it (if you
don't do this you will need to edit ``manifest.json`` and ``browserconfig.xml``
to fix the paths). Then go to the bottom of the page and click
*Generate your Favicons and HTML Code*.

The *Install your favicon* page is displayed, click on the button called
*Favicon Package* to retrieve the zip file containing the favicons and config
files.

The rest of the instructions on the favicon generator page need to be modified
slightly for a django project.  Use the instructions below instead

Create a the directory in ``project/static`` for the favicon files if it does
not already exist using the command::

  mkdir -p project/static/ico

Extract the contexts of the zip file to this directory.

Copy the following text into the head section of your base template (preferably
near the start of the head section)::

  <link rel="shortcut icon" href="{% static 'ico/favicon.ico' %}">
  <link rel="apple-touch-icon" sizes="180x180" href="{% static 'ico/apple-touch-icon.png' %}">
  <link rel="icon" type="image/png" sizes="32x32" href="{% static 'ico/favicon-32x32.png' %}">
  <link rel="icon" type="image/png" sizes="16x16" href="{% static 'ico/favicon-16x16.png' %}">
  <link rel="manifest" href="{% static 'ico/manifest.json' %}">
  <link rel="mask-icon" href="{% static 'ico/safari-pinned-tab.svg' %}" color="#5bbad5">
  <meta name="msapplication-config" content="{% static 'ico/browserconfig.xml' %}">
  <meta name="theme-color" content="#ffffff">

.. tip:: The ``head`` section is included in our ``base`` app:
         https://gitlab.com/kb/base/blob/master/base/templates/base/base.html

Essentially this is simply the code shown in step 3 of the favicon generator
site but using the static template tag e.g.::

  href="/static/ico/apple-icon-57x57.png"

is changed to::

  href="{% static 'ico/apple-icon-57x57.png' %}"

After you deploy check that your project conforms to standards by using the
`favicon checker`_


.. _`Favicon Checker`: https://realfavicongenerator.net/favicon_checker
.. _`Favicon Generator`: https://realfavicongenerator.net/
.. _`Font Squirrel Generator`: https://www.fontsquirrel.com/tools/webfont-generator
.. _`Font Squirrel`: https://fontsquirrel.com
.. _`Google Fonts`: https://fonts.google.com
.. _`How to Favicon in 2022: Six files that fit most needs`: https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs
