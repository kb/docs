postfix
*******

Monitoring
==========

To view the queue::

  postqueue -p

Send
====

To check if the system is correctly configured to send system emails try::

  mail root
  mail patrick
  # or...
  mail patrick@kbsoftware.co.uk

.. tip:: Fill in the details, then *Ctrl D* when you finish writing the
         message.

.. warning:: You may need to add your domain if the default is something else
             e.g. ``mail patrick@hatherleigh.info``

.. note:: To check email from Flowable you need to set up a workflow.

Errors
======

Postfix writes to ``syslog``. To extract all the postfix entries::

  sudo cat /var/log/syslog | parse-log - postfix

.. tip:: I think all the postfix programs (eg ``qmngr``, ``smtpd``, etc) prefix
         their program name in the log with ``postfix``.

bounce
------

The ``bounced`` status appears in ``/var/log/syslog`` like this::

  Nov 12 08:05:51 UKKB01A200 postfix/smtp[1646047]: 743AC171BC7:
  to=<patrick.kimber@kbsoftware.co.uk>,
  relay=10.3.3.3[10.3.3.3]:25, delay=0.8, delays=0.74/0.02/0.04/0, dsn=5.3.4,
  status=bounced (message size 17023981 exceeds size limit 10485760 of server 10.3.3.3[10.3.3.3])

.. warning:: ``bounced`` message are not sent, retried or notified to the
             administrator.
             We would like to be notified.  See ticket 5908 for info.

Configuration
=============

Application (Django / Flowable)
-------------------------------

To use ``postfix`` installed using our Salt states (as detailed below)::

  Use TLS           False / No
  Host              localhost
  Port              25

pillar
------

.. tip:: Check existing sites for ``mail_server`` configuration.

The SMTP server details are stored in the site file
(``server_meta``, ``mail_server``).

Add the following to the ``sites`` pillar e.g. ``sites/kb101.sls``::

  server_meta:

    mail_server:
      smtp_relay_host: smtp.sparkpostmail.com
      smtp_relay_port: 587
      smtp_username: SMTP_Injection
      smtp_password: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      admin_email: patrick@kbsoftware.co.uk
      message_size_limit: 25000000
      sender_rules:
        /.*@kbsoftware.co.uk/: patrick@kbsoftware.co.uk

- ``admin_email`` is the address used for issues with the server etc.
- ``message_size_limit`` is the *maximal size in bytes* and is an *optional*
  parameter.  The default size limit is 10240000 bytes.
- ``sender_rules`` is the *from* address.  This address will be used for sending
  all the emails.  This is useful for Office 365 SMTP servers, because all email
  must be sent from a specific domain.
  (I think the regular expression, ``/.*/:``, matches all emails).

.. tip:: If using SparkPost, then you will need to configure the
         :ref:`mail-sparkpost-domains`

To check the ``message_size_limit`` has been changed::

  postconf | grep size_limit

Linux
-----

The main configuration file is ``/etc/postfix/main.cf``.  It includes the
following:

- ``/etc/aliases``, where mail to root should go.
- ``smtp_generic_maps``, a catch all (for ``/etc/aliases``)
- ``sender_canonical_maps``, is used to change the from address.
- ``relayhost`` instructs Postfix to forward everything.
- If using SparkPost (see :doc:`app-mail` for more information),
  authentication details will be added to ``/etc/postfix/main.cf``.

Maintenance
===========

.. warning:: Think carefully before running the following command...

Delete all messages in the queue::

  postsuper -d ALL

Questions
=========

Are these error messages?::

  match_list_match: permit_mynetworks: no match

Testing
=======

Remove the ``smtp_relay_host`` from the pillar and apply the Salt states e.g::

  server_meta:
    mail_server:
      smtp_relay_host:
      smtp_relay_port: 25
      smtp_tls_security_level: may

.. tip:: I removed ``smtp_relay_host``, ``smtp_relay_port`` and
         ``smtp_tls_security_level`` and it all seems to be working nicely.

For more information, see `Make sure we don't send emails for test servers`_...


.. _`Make sure we don't send emails for test servers`: https://www.kbsoftware.co.uk/crm/ticket/6954/
