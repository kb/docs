CodeMirror Editor
*****************

.. highlight:: html

Usage
=====

Add the following to your form template
(probably below the ``{% endblock content %}`` block)::

  {% block script_extra %}
    {{ block.super }}
    {% include 'base/_codemirror.html' %}
  {% endblock script_extra %}

Any element with an ID of ``id_code`` will become a code editor e.g::

  <textarea id="id_code">
    {{ xero_invoice_data }}
  </textarea>

To create another code editor e.g ``id_code_2``::

  <textarea id="id_code_2">
    {{ xero_payment_data }}
  </textarea>

Add a ``script`` element add call the ``codeEditor`` function e.g::

  {% block script_extra %}
    {{ block.super }}
    {% include 'base/_codemirror.html' %}
    <script type="text/javascript">
      var textArea = document.getElementById("id_code_2");
      codeEditor(textArea);
    </script>
  {% endblock script_extra %}

Example
-------

- `Example Template`_
- `Example View`_


.. _`Example Template`: https://github.com/pkimber/compose/blob/master/example_compose/templates/example/dash.html
.. _`Example View`: https://github.com/pkimber/compose/blob/master/example_compose/views.py
