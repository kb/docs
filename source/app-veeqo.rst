Veeqo
*****

.. highlight:: bash

https://gitlab.com/kb/veeqo

API
===

https://developer.veeqo.com/docs

.. warning:: The API has rate limits (powered by a Leaky Bucket algorithm).
             If requests come too frequently, they are queued in a bucket.
             If the queue reaches the bucket limit, the API responds with
             HTTP 429 error.
             Current limit is 5 requests per second with a bucket size up to
             100 requests.

Configuration
=============

We are using an API Key for authentication...

Generate an API Key
-------------------

- Log into Veeqo
- Settings
- Users
- *Select your user*...
- API Key
- Refresh API Key

Settings
--------

::

  VEEQO_API_KEY = get_env_variable("VEEQO_API_KEY")
  VEEQO_URL = get_env_variable("VEEQO_URL")

Management Commands
===================

These commands retrieve the data via the Veeqo API, but they do nothing else::

  django-admin veeqo-customers
  django-admin veeqo-orders
  django-admin veeqo-products
