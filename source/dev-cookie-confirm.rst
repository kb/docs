Cookie Confirm
**************

.. highlight:: python

In your browser navigate to::

  https://silktide.com/tools/cookie-consent/download/

1. Use Wizard to select the options closest e.g. light-top (5th item)
2. Add link in step 2 (if required)
3. Copy the code in step 3 e.g.:

.. code-block:: html

  <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
  <script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"light-top"};
  </script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
  <!-- End Cookie Consent plugin -->

Get the javascript file (see the ``src`` in second script tag above e.g.)::

  wget https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js

Get the theme css file (name of theme from the with ``.css`` added)::

  wget https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/light-top.css

Edit ``cookieconsent.min.js`` find the text::

  i="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/"

and change to the directory you plan to place the css (for same directory as
html file set to an empty string) to specify the static directory of a django
project e.g.::

  i="/static/vendor/css/"

Edit the css file as required and place in the correct directory

Insert the code retrieved from the wizard (alter the ``src`` in the second
script tag to match the path from your server) e.g.::

  /static/vendor/js/
