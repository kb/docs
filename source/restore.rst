Restore (as a task)
*******************

.. highlight:: bash

`Install Restic`_

Using fabric::

  cd ~/dev/module/fabric/

Restore the database::

  fab domain:www.hatherleigh.info restore:backup

Restore files::

  fab domain:www.hatherleigh.info restore:files

To check the *date* of all your backups::

  fab domain:www.hatherleigh.info check

Issues
======

There are several ways to diagnose a problem with the backup:

1. When you start a ``files`` restore, you can see the date of the backup e.g::

     restoring <Snapshot 52bb5c15 of [/home/web/repo/files/www.hatherleigh.info] at 2019-02-25 01:04:54.115006483 +0000 UTC by @cloud-a> to /tmp/tmp5gitu21o

  This backup was taken on the 25th February which would indicate either no
  backup since the 25th February or no files have changed since then.

2. There is a fabric command which will display the date and time of all the
   backups::

     cd ~/dev/module/fabric/
     fab check

   .. image:: ./misc/fabric-check-backups.png

  The backup date is highlighted in red when it isn't *today*.
  A red date doesn't necessarily indicate a problem, it might just mean no files
  have changed since that date.

3. You can ``ssh`` into the server and create a new backup::

     ssh www.hatherleigh.info
     sudo -i -u web
     backup.www.hatherleigh.info.sh

   The backup command will start by creating a database backup and will then
   create a ``files`` backup.
   If you check the output of the command, you can see if the restic repository
   is locked (or not).

Unlock
======

To unlock the restic repository you can run the following from your laptop::

  export RESTIC_PASSWORD="r35T1C"
  # backup
  restic -r sftp:123@usw-s001.rsync.net:restic/www.hatherleigh.info/backup unlock
  # files
  restic -r sftp:123@usw-s001.rsync.net:restic/www.hatherleigh.info/files unlock

Other Notes
===========

- :doc:`detail/restore`


.. _`Install Restic`: https://www.pkimber.net/howto/linux/apps/restic.html#install
