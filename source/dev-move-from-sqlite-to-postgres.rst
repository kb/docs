Move a site from Sqlite to Postgresql
*************************************

.. highlight:: bash

This procedure assumes you have suitably configured project and virtual
environment (with ``DJANGO_SETTINGS_MODULE`` environment variable set up) and
that your settings module follows the pattern ``settings/dev_<username>.py``.
You'll also need to include the postgresql connector in your project if you've
not already done so.

Extract the data from the existing database::

  django-admin dumpdata > data.json

Create a new postgresql database::

  psql -U postgres -c "CREATE DATABASE <database name> TEMPLATE=template0 ENCODING='utf-8';"

Amend your ``settings/dev_<username>.py`` file to configure the postgresql
database::

  vi settings/dev_<username>.py

Configure postgresql settings as follows::

  DATABASES = {
      'default': {
          'ENGINE': 'django.db.backends.postgresql_psycopg2',
          'NAME': '<database name>',
          'USER': '<database user name>',
          'PASSWORD': '<database password>',
          'HOST': '',
          'PORT': '',
      }
  }

Create the database tables::

  django-admin migrate

Flush the database ready to receive the data::

  django-admin sqlflush

Load the data::

  django-admin loaddata data.json
