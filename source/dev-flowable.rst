Flowable Development
********************

- :doc:`sys-flowable`

.. highlight:: python

- Install :ref:`kubernetes_install_flowable` on Kubernetes
- Here are the :doc:`old/dev-activiti` ...

In ``example/base.py`` for an app, ``settings/base.py`` for a project::

  ACTIVITI_HOST = get_env_variable("ACTIVITI_HOST")
  ACTIVITI_PASS = get_env_variable("ACTIVITI_PASS")
  ACTIVITI_PATH = get_env_variable("ACTIVITI_PATH")
  ACTIVITI_PORT = get_env_variable("ACTIVITI_PORT")
  ACTIVITI_USER = get_env_variable("ACTIVITI_USER")

Development
===========

If you are using :ref:`kubernetes_install_flowable` on Kubernetes, then update
your environment variables e.g.
https://gitlab.com/kb/workflow/blob/4645-ember-workflow/.env.fish

Testing
=======

If you are using ``responses`` to mock Flowable HTTP requests, then add the
following to your ``dev_test.py`` file::

  ACTIVITI_HOST = "localhost"

Add the following to your ``.env.fish`` file::

  set -x ACTIVITI_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x ACTIVITI_PASS "test"
  set -x ACTIVITI_PATH ""
  set -x ACTIVITI_PORT "8080"
  set -x ACTIVITI_USER "rest-admin"

Add the following to your ``.gitlab-ci.yml`` file:

.. code-block:: yaml

  test:
    script:
    - export ACTIVITI_HOST=localhost
    - export ACTIVITI_PASS=
    - export ACTIVITI_PATH=
    - export ACTIVITI_PORT=8080
    - export ACTIVITI_USER=

Deploy
======

To create a Flowable server, start by creating a ``config/workflow.sls`` file
in your pillar containng the following:

.. code-block:: yaml

  # config/workflow.sls
  workflow:
    libreoffice: False

.. note:: If you are using LibreOffice to convert documents to PDF format, then
          set ``libreoffice`` to ``True``.

Add ``config.workflow`` to the ``top.sls`` file for your server e.g.

.. code-block:: yaml

  # top.sls
  base:
    '*':
      - global.users
    'server':
      - config.workflow

In the salt pillar ``sls`` file for your site, add ``workflow`` and the
``activiti_`` variables as follows:

.. code-block:: yaml

  sites:
    my_site:
      package: bpm
      profile: django
      workflow: True
      env:
        activiti_host: "localhost"
        activiti_pass: "kermit"
        activiti_path: "flowable-rest"
        activiti_port: "8080"
        activiti_user: "kermit"

To test the deployment on your live server, install ``httpie`` in your virtual
environment e.g::

  # as the 'web' user...
  cd ~/repo/project/my_site/live/
  source venv/bin/activate
  pip install httpie
  http GET http://kermit:kermit@localhost:8080/flowable-rest/service/repository/deployments/

You should get a positive response e.g::

  HTTP/1.1 200
  Cache-Control: no-cache, no-store, max-age=0, must-revalidate
  Content-Type: application/json;charset=UTF-8
  Date: Mon, 30 Dec 2019 14:20:15 GMT
  Expires: 0

  {
    "data": [
      {
      "category": null,
      "deploymentTime": "2019-12-30T12:55:14.068Z",
      "id": "9effc56b-2b03-11ea-aadf-000d3a7ea435",
      "name": "Demo processes",
      "parentDeploymentId": null,
      "tenantId": "",
      "url": "http://localhost:8080/flowable-rest/service/repository/deployments/9effc56b-2b03-11ea-aadf-000d3a7ea435"
      }
    ],
    "order": "asc",
    "size": 1,
    "sort": "id",
    "start": 0,
    "total": 1
  }
