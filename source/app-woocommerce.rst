WooCommerce
***********

.. highlight:: bash

https://gitlab.com/kb/woocommerce

Icon::

  <i class="fa fa-shopping-cart"></i>

- :doc:`dev-wordpress`
- :doc:`sys-wordpress`
- :doc:`sys-wordpress-woocommerce`

Summary
=======

Use the WooCommerce API to read product information and create a spreadsheet
suitable for import into ManoMano.

- The WooCommerce app is configured with a list of WooCommerce sites
  (``WooCommerceSite``).
- To access the WordPress API, :doc:`dev-wordpress`
- Each WooCommerce site has a ``select_class`` which is used to *select* the
  product to include in the ManoMano spreadsheet (e.g. ``SelectAlphaOnly`` and
  ``SelectMinimumValue`` in ``woocommerce_app.service.py``).

Configure
=========

.. warning:: Some of the WooCommerce fields are **read-only** and cannot be
             updated using the REST API e.g. ``products``, ``related_ids``.
             For more information, see `Product properties`_

- Log into your WordPress / WooCommerce website.
- Browse to *Dashboard*, *WooCommerce*, *Settings*, *Advanced*, *REST API*
- Click *Add key* and create a *Read* only key
  (make a note of the key and the secret)
- Add the *key* and *secret* to your ``.private.fish`` file e.g::

    set -x WOOCOMMERCE_SITE_1_API_KEY "ck_123"
    set -x WOOCOMMERCE_SITE_1_SECRET "cs_4567"

- If you want to access the WordPress REST API (perhaps to upload images),
  then configure an *Application Password* for the WordPress API,
  :doc:`dev-wordpress`.

- Add the ``wordpress_application_password`` to ``.private.fish`` file e.g::

    set -x WOOCOMMERCE_SITE_1_WP_APP_PASS "wp-app-pass-89"

Create your WooCommerce sites e.g::

  # example_woocommerce/management/commands/demo-data-woocommerce.py
  django-admin demo-data-woocommerce

The user can update the ``title``, ``minimum_value`` and ``percent_increase``
via the dashboard.

Management Commands
===================

.. warning:: Some of the WooCommerce fields are **read-only** and cannot be
             updated using the REST API e.g. ``products``, ``related_ids``.
             For more information, see `Product properties`_

``6245-product-export`` to export all of the products from an existing
WooCommerce site::

  # management/commands/6245-product-export.py
  django-admin 6245-product-export www-hatherleigh-info

``6245-product-extract-related-skus`` to extract product related SKUs::

  # woocommerce_app/management/commands/6245-product-extract-related-skus.py
  django-admin 6245-product-extract-related-skus 2022-07-18-woocommerce-products-www-hatherleigh-info.json


In summary to transfer products from ``www-site-one`` to ``www-site-two``::

  # export products from site one
  django-admin 6245-product-export www-site-one
  # import products to site two
  django-admin 6245-product-import www-site-two 2022-07-18-woocommerce-products-www-site-one.json

  # extract related skus from site one
  django-admin 6245-product-extract-related-skus 2022-07-18-woocommerce-products-www-site-two.json
  # will create '2022-07-18-woocommerce-products-www-site-two-related-skus.json'

  # export products from site two
  django-admin 6245-product-export www-site-two
  # will create '2022-07-18-woocommerce-products-www-site-two.json'

  # update related skus for site one
  django-admin 6245-product-update-related-skus www-site-two 2022-07-18-woocommerce-products-www-site-two.json 2022-07-18-woocommerce-products-www-site-one-related-skus.json


.. _`Product properties`: https://woocommerce.github.io/woocommerce-rest-api-docs/#product-properties
