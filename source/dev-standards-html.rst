HTML Code Standards
*******************

.. highlight:: html

- Click here for :doc:`dev-standards`
- Click here for :doc:`dev-standards-code`
- Click here for :doc:`dev-standards-doc`
- Click here for :doc:`dev-standards-js`
- Click here for :doc:`dev-standards-security`
- Click here for :doc:`dev-standards-sys`
- Click here for :doc:`dev-standards-tech`
- Click here for :doc:`dev-standards-ui`

Attributes
==========

``id``
------

Use dash to separate e.g::

  <input id="file-upload" name="file-upload" type="file" class="sr-only">

``rel``
-------

We received a report from Google of ``Failed: Redirect error`` for a URL
(forwarding to a page on an external website).
e.g. https://www.hatherleigh.info/189147/redirect/

If we follow it - we get the appropriate page on the website but if we test it
on Google using the live URL it fails with
``Failed: Blocked due to access forbidden (403)``.

We have decided to add ``rel="nofollow"`` to these links e.g::

  <a rel="nofollow" href="https://cheese.example.com/Appenzeller_cheese">Appenzeller</a>

For more information, see ``nofollow`` on:

- `MDN, Link types`_
- `Qualify your outbound links to Google`_


.. _`MDN, Link types`: https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types
.. _`Qualify your outbound links to Google`: https://developers.google.com/search/docs/advanced/guidelines/qualify-outbound-links
