Drupal
******

Logging and Log Files
=====================

Log into the admin area of the site:

1. *Manage*
2. *Reports*
3. *Recent log messages*

Update
======

.. warning:: **Better to use** `Malcolm's Drupal Update Notes`_

- `Malcolm's Drupal Update Notes`_
- `Drupal 8 User Guide: 13.5. Updating the Core Software`_
- `Update core manually`_

.. warning:: Make sure you have a recent backup of your site.

To check the current version of your site, browse to
https://www.mydrupalsite.com/admin/reports/status

Download the latest Drupal update e.g. ``drupal-8.6.10.tar.gz``.

Copy the ``tar.gz`` file to the server::

  scp drupal-8.6.10.tar.gz  www.mydrupalsite.com:repo/temp/

Copy the update archive to the ``deploy`` folder::

  sudo -i
  cd /home/web/repo/project/www.mydrupalsite.com/deploy
  mv /home/patrick/repo/temp/drupal-8.6.10.tar.gz .

Make sure the folder doesn't already contain a ``drupal-8.6.10`` folder.

Extract the update::

  tar xzf drupal-8.6.10.tar.gz
  cd drupal-8.6.10/

  # copy our code from the previous version of the site
  cp -r ../../live/sites/* sites/
  # note: our site does not use 'themes', 'profiles' or 'modules'

Make a note of where the 'live' folder is currently pointing to (so you can
put it back if the update fails).

Put the site into maintenance mode:

.. image:: ./misc/drupal-maintenance-mode.png

Link the new code to the ``live`` folder::

  ls -l /home/web/repo/project/www.mydrupalsite.com/
  # e.g. live -> deploy/0_0_01__20190117_135225_malcolm

Link::

  rm /home/web/repo/project/www.mydrupalsite.com/live
  ln -s /home/web/repo/project/www.mydrupalsite.com/deploy/drupal-8.6.10 /home/web/repo/project/www.mydrupalsite.com/live

Clear the cache (and update the database) by browsing to
https://www.mydrupalsite.com/update.php

.. note:: I didn't think I needed to clear the cache, but I think it rebuilds
          the ``twig`` files which are the Drupal templates!

Update permissions::

  chown www-data:www-data sites/default/files
  chown -R www-data:www-data sites/default/files/

Issues
------

1. The update needs to be done by the ``root`` user.
   I think it should be ``web``?
2. The ``files`` folders do not contain any files:
   ``/home/web/repo/files/www.mydrupalsite.com/public/``
   ``/home/web/repo/files/www.mydrupalsite.com/private/``
   Why are the files for the Drupal site not in these folders?

Salt
====

.. note:: See the :doc:`sys-php` documentation

With regard to the Salt scripts you asked about:

- `restore.sh`_ restores the drupal files from a compressed tar backup
- `restore_db.sh`_ restores a mysql database from an SQL script.

I created them so we could restore the journal system from a snapshot and
modified them for use in the upgrade to Drupal 8.  I thought the best place for
them was in the Salt configuration.

To use them in the current configuration we would need to restore the files
backup and create a compressed tar and restore the database backup.


.. _`Drupal 8 User Guide: 13.5. Updating the Core Software`: https://www.youtube.com/watch?v=eLzAD83a9BY&t=74s
.. _`Malcolm's Drupal Update Notes`: https://www.kbsoftware.co.uk/crm/ticket/1018/
.. _`restore.sh`: https://gitlab.com/kb/salt/blob/2684-backup-restic/web/drupal/restore.sh
.. _`restore_db.sh`: https://gitlab.com/kb/salt/blob/2684-backup-restic/web/drupal/restore_db.sh
.. _`Update core manually`: https://www.drupal.org/docs/8/update/update-core-manually
