Salt Cloud - Install
********************

.. highlight:: bash

Create Server
=============

Login as ``root`` using the IP address or domain name::

  sudo -i -u root
  ssh the.server.ip.address

Install Salt
============

.. note:: To update to latest repository, see Malcom's comments here:
          https://chat.kbsoftware.co.uk/kb/pl/xmc5567oabdtfkmrb63475fobc

From https://docs.saltproject.io/

Run the following command to import the SaltStack repository:

For Ubuntu 22.04::

  # Ensure keyrings dir exists
  sudo mkdir -p /etc/apt/keyrings
  # Download public key
  sudo curl -fsSL https://packages.broadcom.com/artifactory/api/security/keypair/SaltProjectKey/public | sudo tee /etc/apt/keyrings/salt-archive-keyring.pgp
  # Create apt repo target configuration
  sudo curl -fsSL https://github.com/saltstack/salt-install-guide/releases/latest/download/salt.sources | sudo tee /etc/apt/sources.list.d/salt.sources

Next Steps
==========

1. To setup a :doc:`salt-master`
2. To setup a Minion, :doc:`salt-provision`

Legacy Notes
============

:doc:`old/salt-cloud-install`
