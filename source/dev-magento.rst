Magento XMLRPC API
******************

.. highlight:: python

- Click here for the :doc:`app-magento`

Magento Dashboard
=================

From `Magento API setup instructions - SOAP/XML`_

Creating a SOAP/XML Role
------------------------

- In the Magento admin panel choose *System* > *Web Services* > *SOAP/XML* -
  *RPC Roles*
- Choose *Add New Role*
- In the *Role Info* tab create a *Role Name* and choose *Save Role*
- In the *Role Resources* tab change the *Resource Access* drop down option to
  *ALL* and then choose *Save Role*

.. image:: ./misc/magento-role-access.png

Creating a SOAP/XML User
------------------------

- In the Magento admin panel choose *System* > *Web Services* > *SOAP/XML* -
  *RPC Users*
- Choose *Add New User*
- Fill out the *Account Information* form generated.
  Both the *User Name* and the *API Key* are created by you.
  You will need both of these when setting up the connection.
- Then choose *Save User*.

.. image:: ./misc/magento-user.png

- In the *User Role* tab allocate the User to the Role (you created earlier)
  and then choose *Save User*

.. image:: ./misc/magento-user-role.png

Permissions
-----------

.. image:: ./misc/magento-api-role.png

API
===

Connect to the XMLRPC API and retrieve a sales order::

  import json
  import urllib.parse
  import xmlrpc.client

  api_url = "http://my-project.co.uk/test/"
  url = urllib.parse.urljoin(api_url, "api/xmlrpc")
  client = xmlrpc.client.ServerProxy(url)
  session = client.login('pkimber', 'my-new-api-key-see-above')

  # retrieve an order
  params = [{"increment_id": "100015171"}]
  response = client.call(session, "sales_order.list", params)
  print(json.dumps(response, indent=4))


.. _`Magento API setup instructions - SOAP/XML`: https://tradebox.zendesk.com/hc/en-gb/articles/207166936-Magento-API-setup-instructions-SOAP-XML
