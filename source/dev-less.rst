LESS
****

.. note:: We did use LESS for a couple of legacy projects, but we are now using
          :doc:`dev-sass` (with ``.scss`` extension).

Links
=====

- :doc:`dev-sass`

Usage
=====

In our ``LESS`` project, the process is run by the gruntfile - so there's no
need to run any commands except::

  npm install

Then::

  grunt

This rebuilds ``LESS`` and ``JS`` in one easy process and watches any changes
to auto-rebuild if you make any changes.
