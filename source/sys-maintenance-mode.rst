Site Maintenance Mode
*********************

.. highlight:: html

Introduction
------------

All our django sites that are configured using salt can be set to maintenance
mode.  A page called ``maintenance.html`` in the root directory of the site is
displayed to a site user when the site is in maintenance mode.

The salt configuration provides a sample maintenance page this can be customised
as required for the site. Salt will not overwrite the customised maintenance
page.

A script enables and disables maintenance mode.

Enable and disable maintenance mode
-----------------------------------

.. warning:: Background tasks will continue to run while your site is in
             maintenance mode.

The maintenance mode script is used as follow::

  maintenance-mode <domain> on | off

e.g to enable maintenance mode for *hatherleigh.info*::

  maintenance-mode hatherleigh.info on

To disable maintenance mode for *hatherleigh.info*::

  maintenance-mode hatherleigh.info off

Create a customised maintenance page
------------------------------------

If you have not created a maintenance page the first time you try to enter
maintenance mode the script will ask if you wish to clone the sample page.

Answer ``y`` to create a the maintenance page.  Modify this as appropriate for
the site and run the command again to enter maintenance mode.
