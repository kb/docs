Salt Master
***********

.. note:: A Salt master starts life as a minion.

To create the minion (which will become a master), follow the instructions in
:doc:`old/salt-cloud-install`, followed by :doc:`old/salt-cloud-digitalocean` or
:doc:`old/salt-cloud-rackspace`

Log into your new master server as ``root``...

Install
-------

Install salt master::

  apt update
  apt install salt-master

Configuration
-------------

::

  vim /etc/salt/master
  # uncomment the following lines:
  # file_roots:
  #   base:
  #     - /srv/salt

  # and uncomment the following lines:
  # pillar_roots:
  #   base:
  #     - /srv/pillar

Then re-start the salt master::

  systemctl restart salt-master.service

Check out the ``sls`` and pillar files from your repository so they are in the
following folder structure::

  ├── srv
  │   ├── pillar
  │   │   ├── db
  │   │   ├── global
  │   │   ├── README.rst
  │   │   ├── service
  │   │   ├── sites
  │   │   └── top.sls
  │   ├── salt
  │   │   ├── db
  │   │   ├── default
  │   │   ├── devpi
  │   │   ├── nginx
  │   │   ├── README.rst
  │   │   ├── solr
  │   │   ├── ssh
  │   │   ├── supervisor
  │   │   ├── top.sls
  │   │   ├── uwsgi
  │   │   └── web
  │   └── ssl

Provision
---------

The Salt pillar should be set-up to install the services you require for your
master e.g::

  base:
    'master':
      - global.yourcompany.users
      - service.yourcompany.devpi

To set-up your ``master``, follow the instructions in :doc:`salt-provision`
remembering to use ``localhost`` as the IP address of the master.


.. _`Adding a Formula directory manually`: https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#adding-a-formula-directory-manually
.. _`openjdk-10.sls`: https://github.com/salt-formulas/salt-formula-java/blob/master/tests/pillar/openjdk-10.sls
.. _`salt-formula-java`: https://github.com/salt-formulas/salt-formula-java
