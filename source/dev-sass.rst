sass
****

.. highlight:: bash

.. note:: We are using ``sass`` (with the ``.scss`` extension) in our projects.
          (we did use :doc:`dev-less` for a couple of legacy projects).

Links
=====

- Ember :ref:`ember_cli_sass`
- :doc:`dev-less`
- YouTube, using ``node-sass``,
  https://www.youtube.com/watch?v=GMSkKaGk1D8

Quick guide to node-sass with our django projects
=================================================

``node-sass`` requires node and npm to be installed see :ref:`install_node`

Using in an existing project
----------------------------

When you clone a project that's already using scss you can configure the
development environment as follows::

  npm install --only=dev

.. _using_sass_in_development:

Using sass in development
-------------------------

After configuring your development environment, to enable continuous
compilation of your scss changes to css, in the root directory of the project
type::

  npm run serve

This will compile the scss to css, monitor the scss directory for changes and
start the django development server.  Alterantively, if you do not require the
development server you can compile the scss to css once using::

  npm run scss-compile

Or continuously monitor changes to the scss directory using::

  npm run scss-watch

Add sass to an existing django project
======================================

For our django projects we create a scss directory at the top
level and use the 7-1 pattern.  You can use the dev-scripts ``create-scss``
script to set this up::

  create-scss <project group> <project name> "<description>" [<master scss file> <output css file>]

The project group is the project group on your git repository provider,
project name and description are self explanatory e.g. if your repo is at
gitlab.com:kb/example_com.git and your css file is web/static/web/web.css::

  create-scss kb example_com "Website for example.com" styles.scss web/static/web/css/web.css

This will create a directory structure of::

  package.json
  scss/
    |
    + abstracts/
      |
      +_mixins.scss
      |
      +_functions.scss
      |
      +_variables.scss
    |
    + base/
      |
      +_animations.scss
      |
      +_base.scss
      |
      +_typography.scss
      |
      +_utilities.scss
    |
    + components/
      |
      +_button.scss
      |
      +_form.scss
      |
      +_menu.scss
    |
    + layouts/
      |
      +_grid.scss
      |
      +_container.scss
      |
      +_header.scss
      |
      +_footer.scss
    |
    + pages/
      |
      +_home.scss
    |
    + theme/
    |
    + vendor/
    |
    + styles.scss

This script runs ``npm install`` using the generated ``package.json`` so you
will also see a node_modules in your project directory and if using node v8+
a package-lock.json you should add /node_modules/ to the .gitignore and add
node_modules to the norecursedirsi directive in the setup.cfg 

Depending of the name of the stylesheet you have in your project you may need
to edit the scripts section of package.json to update the correct css file.

You can you use sass in you development as described above
:ref:`using_sass_in_development`

General notes about node-sass
=============================

.. _install_node:

Install Node
------------

If you don't already have node installed you can install the ubuntu package::

  sudo apt update
  sudo apt install nodejs

As of ubuntu 18.04 this will install v8.  If your version of ubuntu does not
install npm - if the following reports a version it's installed::

  npm --version

If you get command not found can install npm using::

  sudo apt install npm

Alternatively you can install the latest nodejs from nodesource repository as
follows::

  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  sudo apt update
  sudo apt install nodejs

You'll probably need build-essentials if you don't already have in installed::

  sudo apt-get install build-essential

Install node-sass
-----------------

If you want to install node-sass globally you can do this using - we tend to
use per project installation with package.json so this is not necessary::

  npm install -g node-sass

If you have trouble installing ``node-sass``, see
:ref:`node_sass will not install`

node-sass usage
---------------

Compile a scss file one time::

  node-sass -o <output directory> <relative path to scss file>

Compile all scss files in a directory file one time::

  node-sass -o <output directory> <relative path to directory containing scss files>

Watch a directory containing scss files and compile on the fly::

  node-sass -o <output directory> <relative path to directory containing scss files> -w

node-sass Options
-----------------

By default ``node-sass`` generates css with each attribute on a new line and the
closing brace on the same line as the last attribute. Use the expanded output
style to generate pretty output::

  --output-style=expanded

For a compact output use::

  --output-style=expanded

Sass
====

Sass is available in two flavours we use the extension of css syntax which is
stored in a file with the extension ".scss"

Simple example
--------------

The ``scss/style.scss`` file containing the following:

.. code-block:: scss

  .example {
    p {
      color: green;
    }
  }

Can be compiled (in watch mode) to ``css/style.css`` using the command::

  node-sass -o css scss -w --output-style=expanded

The ``css/style.css`` file will contain:

.. code-block:: css

  .example p {
    color: green;
  }
