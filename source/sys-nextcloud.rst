Nextcloud
*********

.. highlight:: bash

.. tip:: I started by creating a server with 1GB of RAM.
         We were getting out of memory errors, so I upgraded to 2GB and all
         seems fine for now.

I used these instructions:
`How to Install Nextcloud with Nginx and Lets Encrypt on Ubuntu 20.04 LTS`_

I also used these for a more upto date installation guide, not much changes:
`Example installation on Ubuntu 22.04 LTS`_

To create these Salt states for install:
https://gitlab.com/kb/salt/-/merge_requests/29

To download::

  wget -q https://download.nextcloud.com/server/releases/latest.zip
  cd ~/repo/project/nextcloud.{ domain }/
  unzip -qq latest.zip
  # create the 'live' folder
  mv nextcloud live
  # tidy up
  rm latest.zip

Use ``fabric`` to create the database::

  fab create-db nextcloud.{ domain }

Installation
============

Docker Setup
------------

.. note:: There are many different docker images, 
          go through and select the one you want.
          For the example it's aio maintained by nextcloud,
          However I'd suggest linuxserver.io as they have great work too.

If you wish to use docker, then there is a much easier way of installing this::

  #If you haven't installed docker already...
  curl -fsSL https://get.docker.com | sudo sh

  # For Linux and without a web server or reverse proxy (like Apache, Nginx and else) already in place:
  sudo docker run \
  --sig-proxy=false \
  --name nextcloud-aio-mastercontainer \
  --restart always \
  --publish 80:80 \
  --publish 8080:8080 \
  --publish 8443:8443 \
  --volume nextcloud_aio_mastercontainer:/mnt/docker-aio-config \
  --volume /var/run/docker.sock:/var/run/docker.sock:ro \
  nextcloud/all-in-one:latest

That's pretty much it!

If you get errors stating that ports are in use, disable certain services..
::

  sudo systemctl stop apache2
  #Or
  sudo systemctl stop tomcat9

These will already be running on ports, but after relaunching the container with
::

  sudo docker start nextcloud-aio-mastercontainer

It should launch without any options and utilise whichever web hosting service you have installed.

Next is just onto the websetup as normal.

Setup
-----

Here we want to make sure we have installed our dependancies::

  sudo apt update && sudo apt upgrade
  sudo apt install php-gd php-curl php-mbstring php-intl php-gmp php-bcmath php-xml php-imagick php-zip

You will also need to pick your database type (mysql, sqlite, postgres)
Then also install your web engine (apache2, nginx)

Database Setup
--------------

Here you need to create the database that you will be using.

MySQL is suggested here, so the example will follow that::

  sudo mysql
  CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
  CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
  GRANT ALL PRIVILEGES ON nextcloud.* TO 'username'@'localhost';
  FLUSH PRIVILEGES;
  quit;

Permissions Setup
-----------------

We need the majority of our root directory owned by::

  sudo chown -R www-data:www-data */*/nextcloud

If you encounter errors then you may want to 
change the permissions back to the original user

Installing via occ
------------------

It seems the easiest way with the least issues is to use php for the install
They have documentation on this to help us along, the example is::

  $ cd /var/www/nextcloud/
  $ sudo -u www-data php occ  maintenance:install \
  --database='mysql' --database-name='nextcloud' \
  --database-user='root' --database-pass='password' \
  --admin-user='admin' --admin-pass='password'

Replace any variable you need to, and for help just use::

  sudo -u www-data php occ help

Supported databases are::

  - sqlite (SQLite3 - Nextcloud Community edition only)
  - mysql (MySQL/MariaDB)
  - pgsql (PostgreSQL)
  - oci (Oracle - Nextcloud Enterprise edition only)

If all goes well the configuation file should be filled out with some details
and you should be able to visit the web page and start using nextcloud

Import
======

Contacts
--------

Export from Google in ``vcf`` format, *vCard (for iOS Contacts)*.

Google Drive
------------

From `Adding files to Nextcloud using the command line`_::

  # log in as yourself (e.g. ``pat``)
  mkdir /home/web/repo/project/nextcloud.{ domain }/live/data/pat/files/yb

.. tip:: ``yb`` is the folder where you want to put the files
         (e.g. short version of your company name).

https://rclone.org/

.. tip:: I haven't done this yet, but it would be far better to download onto
         the Nextcloud server (to save upload bandwidth).

Download and install ``rclone``::

  curl https://rclone.org/install.sh | sudo bash

Configure::

  rclone config

.. tip:: Use a simple name for your drive e.g. ``pk-drive``.

.. tip:: Choose *Read-only access to file metadata and file contents*.

Sync::

  rclone sync -P pk-drive:/ /home/web/repo/project/nextcloud.{ domain }/live/data/pat/files/yb/

  # to download in LibreOffice format
  rclone sync -P --drive-export-formats ods,odt,odp pk-drive:/ .

.. warning:: Think carefully about the download format.  Nextcloud says,
             *You can even collaborate with others on ODT and Markdown files!*
             It looks as if Collabora Online and ONLYOFFICE are only free for
             personal use,
             https://nextcloud.com/onlyoffice/
             https://nextcloud.com/collaboraonline/
             Perhaps we collaborate using Markdown only!

Scan (scan for new files and update the file cache)::

  sudo -u www-data php /home/web/repo/project/nextcloud.{ domain }/live/occ files:scan --path="pat/files/kb/"

.. tip:: ``pat`` is the Nextcloud user name.


.. _`Adding files to Nextcloud using the command line`: https://medium.com/code-enigma/adding-files-to-nextcloud-using-the-command-line-9fe0074b843a
.. _`How to Install Nextcloud with Nginx and Lets Encrypt on Ubuntu 20.04 LTS`: https://www.howtoforge.com/tutorial/ubuntu-nginx-nextcloud/
.. _`Example installation on Ubuntu 22.04 LTS`: https://docs.nextcloud.com/server/30/admin_manual/installation/example_ubuntu.html