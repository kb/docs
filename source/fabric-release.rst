Release
*******

.. highlight:: bash

Checklist
=========

For a standard release, follow this checklist:

1. Commit your code.
2. Check your code formatting (using ``black`` for python).
3. ``git pull``
4. Check your branch.
5. (If you are on a branch, you might want to) ``merge master``.
6. ``pytest -x``
7. Release your code...

Procedure
=========

.. note::

  The release process supports GIT (and used to support Mercurial).

  The fabric release task will deploy your module to a package index of your
  choice (e.g. ``dev``).  For details on setting up your own package index,
  see the :doc:`devpi` documentation.

.. note::

  This task prefixes your application name with a name of your choosing.  This
  prevents name clashes with other packages on the python package index (PyPI).

  I recommend choosing a short version of your company name e.g. ``kb``:

.. tip:: Set-up your ``~/.pypirc`` file or ``release`` will fail.
         See :ref:`pypirc` for details.

Start by committing your code to the version control system.

Check you don't have any missing migrations
(from `Checking that Django has all migrations`_)::

  django-admin.py makemigrations --check --dry-run --settings=settings.dev_test > /dev/null || echo "Missing migrations."

Release the module and upload to your package index.  In this first example, we
switch to the ``fabric`` virtual environment which is using python
(``fabric`` is currently not compatible with python 3)::

  cd /your/project/folder/
  source ~/dev/module/fabric/venv-fabric/bin/activate && \
      fab -f ../../module/fabric/release.py dist:prefix=kb,pypirc=dev && \
      cd .

Or for the ``fish`` shell::

  source ~/dev/module/fabric/venv-fabric/bin/activate.fish; and fab -f ../../module/fabric/release.py dist:prefix=kb,pypirc=dev;

If this is the first time you have released this package then the upload will
fail.  You need to run the following before running the ``release`` command
again::

  devpi login kb --password "123"
  # devpi use --set-cfg kb/dev
  devpi upload

.. cd /your/app/folder/
.. python setup.py register -r dev

.. note:: see :doc:`devpi` for help with the above commands.

.. note:: Don't forget to commit your code to the version control system after
          running this task

Click here for :doc:`fabric-deploy` instructions...

To check the contents of the release::

  tar -ztvf dist/kb-app-name-0.0.16.tar.gz

.. note::

  The release process removes underscores from the package name, so if your
  package is called ``app_name`` it will be changed to ``app-name``.  I don't
  really understand why this is necessary, but the following links might be
  useful.

  http://python.6.x6.nabble.com/quot-Safe-quot-Project-Names-and-underscores-in-Project-Names-issue-td2011757.html

  https://bitbucket.org/tarek/distribute/src/611910892a04/pkg_resources.py#cl-1135

Data
====

If you want to include data files in your release e.g. ``name.json``, then put
them in a ``data`` folder inside an app folder in your project e.g::

  dash/data/
  project/data/

Issues
======

Running the fabric ``release.py`` command::

  File "/usr/lib/python2.7/distutils/command/upload.py", line 70, in upload_file
  raise AssertionError("unsupported schema " + schema)
  AssertionError: unsupported schema
  Fatal error: local() encountered an error (return code 1) while executing 'python setup.py clean sdist upload -r dev'

To solve this issue, create your ``~/.pypirc`` file. See :ref:`pypirc` for
details.


.. _`Checking that Django has all migrations`: https://medium.com/@kitsunde/checking-that-django-has-all-migrations-55a1c23c3a59#.d4eb5pr97
