KB Software Documentation
*************************

.. important:: Our aim is to create simple step-by-step documentation for
               implementing a feature or service.

               The notes do not usually try and explain the *reason why* (or
               alternative strategies).  They will assume competency in
               development, the linux command line and sys-admin.

               From `What nobody tells you about documentation`_.
               We are writing *How-To Guides*, so they are:

               1. is goal-oriented
               2. shows how to solve a specific problem
               3. is a series of steps

               Analogy: a recipe in a cookery book

               (A copy of this document can be found here:
               :download:`misc/what-nobody-tells-you-about-documentation.pdf`)

               The YouTube video from PyCON UK 2017, `Docs or it didn't happen`_
               is very useful.

.. toctree::
   :maxdepth: 1
   :caption: Standards

   glossary

   dev-security
   dev-standards
   dev-standards-code
   dev-standards-doc
   dev-standards-html
   dev-standards-js
   dev-standards-security
   dev-standards-sys
   dev-standards-tech
   dev-standards-ui

.. toctree::
   :maxdepth: 1
   :caption: Sales

   sales
   sales-hardware

.. toctree::
   :maxdepth: 1
   :caption: Process

   process
   dev-env
   checklist
   mattermost
   monitor
   dev-workflow

.. toctree::
   :maxdepth: 1
   :caption: Customer Support

   seo
   sys-android
   sys-backup
   sys-flowable
   sys-google-apps
   sys-podman
   sys-postfix
   sys-security
   sys-vpn

.. toctree::
   :maxdepth: 1
   :caption: Master

   salt-master
   devpi

.. toctree::
   :maxdepth: 1
   :caption: Minion

   salt-install
   salt-provision
   salt-commands

   fabric-env
   fabric-database
   fabric-deploy
   windows-deploy
   fabric-release
   fabric-search
   fabric-ssl

.. toctree::
   :maxdepth: 1
   :caption: System Administration

   diagnostics
   issues
   sys-azure
   sys-bitwarden
   sys-fail2ban
   sys-keycloak
   sys-manomano
   sys-mattermost
   sys-microsoft
   sys-mysql
   sys-nextcloud
   sys-nginx
   sys-nomachine
   sys-office-365
   sys-postgres
   sys-saleor
   sys-sparkpost
   sys-swap-space
   sys-systemd
   sys-ubuntu
   sys-uptime-kuma
   sys-uwsgi

.. toctree::
   :maxdepth: 1
   :caption: Site

   alfresco
   backup
   backup-dropbox
   cache
   site-config
   salt-pillar
   salt-droplet-rename
   sys-maintenance-mode
   auth
   celery
   firewall
   ftp
   google
   mailgun
   maintain
   restore
   solr
   ssl
   ssl-letsencrypt
   sys-remove-site

.. toctree::
   :maxdepth: 1
   :caption: Development - Django

   dev-django
   dev-django-apps
   dev-django-auth
   dev-django-field
   dev-django-filter
   dev-django-form
   dev-django-geo
   dev-django-media
   dev-django-migrations
   dev-django-rest-framework
   dev-django-reversion
   dev-django-static
   dev-django-test
   dev-django-thumbnails
   dev-django-view
   dev-django-windows

.. toctree::
   :maxdepth: 1
   :caption: Development

   dev-journal

   learning
   dev-apscheduler
   dev-env
   dev-captcha
   dev-chosen
   dev-ckeditor
   dev-codemirror
   dev-cookie-confirm
   dev-debug
   dev-debug-toolbar
   dev-dramatiq
   dev-elasticsearch
   dev-first-deployment
   dev-fish
   dev-flowable
   dev-git
   dev-gitlab
   dev-grunt
   dev-google
   dev-less
   dev-logging
   dev-magento
   dev-move-from-sqlite-to-postgres
   dev-pagination
   dev-pdf-object
   dev-pytest
   dev-requirements
   dev-restore
   dev-s3
   dev-sass
   dev-scripts
   dev-shiny
   dev-sitemap
   dev-social
   dev-tailwind
   dev-tailwind-ember
   dev-tailwind-ui
   dev-testing
   dev-timezone
   dev-wagtail
   dev-wordpress
   dev-webfonts
   domain

.. toctree::
   :maxdepth: 1
   :caption: Ember Development

   dev-ember
   dev-ember-addons
   dev-ember-addons-kb
   dev-ember-auth
   dev-ember-data
   dev-ember-patterns
   dev-ember-test

.. toctree::
   :maxdepth: 1
   :caption: Apps

   app-api
   app-apps
   app-base
   app-block
   app-blog
   app-booking
   app-chat
   app-checkout
   app-compose
   app-contact
   app-crm
   app-enquiry
   app-finance
   app-gallery
   app-gdpr
   app-invoice
   app-job
   app-login
   app-magento
   app-mail
   app-mailchimp
   app-mandrill
   app-monitor
   app-msgraph
   app-onbuy
   app-pipeline
   app-qanda
   app-record
   app-report
   app-sales-order
   app-sales-order-ticket
   app-search
   app-veeqo
   app-woocommerce
   app-work
   app-workflow
   app-xero

.. toctree::
   :maxdepth: 1
   :caption: Cloud

   sys-digital-ocean

.. toctree::
   :maxdepth: 1
   :caption: Kubernetes

   dev-kubernetes
   dev-kubernetes-install

.. toctree::
   :maxdepth: 1
   :caption: Project

   project-simple-site

.. toctree::
   :maxdepth: 1
   :caption: PHP

   sys-php
   dev-php
   dev-php-issues
   drupal

.. toctree::
   :maxdepth: 1
   :caption: WordPress

   sys-wordpress
   sys-wordpress-issues
   sys-wordpress-plugin
   sys-wordpress-security
   sys-wordpress-transfer
   sys-wordpress-update
   sys-wordpress-woocommerce

.. toctree::
   :maxdepth: 1
   :caption: Detailed Notes

   detail/devpi
   detail/restore
   detail/ssl-letsencrypt-dns
   detail/ssl-letsencrypt-manual
   detail/sys-vpn

.. toctree::
   :maxdepth: 1
   :caption: Old Notes

   old/app-checkout
   old/app-pay
   old/dev-activiti
   old/dev-angular2-with-django
   old/dev-backup
   old/dev-docker
   old/dev-kubernetes
   old/dev-restore
   old/fabric-backup
   old/monitor
   old/restore
   old/salt-cloud-amazon
   old/amazon-database
   old/amazon-s3
   old/salt-cloud-digitalocean
   old/salt-cloud-install
   old/salt-cloud-rackspace
   old/salt-masterless
   old/sys-backup
   old/sys-owncloud
   old/sys-wordpress

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _`Docs or it didn't happen`: https://www.youtube.com/watch?v=muhxjdxhIR0
.. _`What nobody tells you about documentation`: https://www.divio.com/blog/documentation/
