WordPress - Transfer / Migrate
******************************

Transfer / migrate from another hosting provider...

- :doc:`sys-wordpress`
- :doc:`sys-wordpress-plugin`
- :doc:`sys-wordpress-security`
- :doc:`sys-wordpress-update`
- :doc:`sys-wordpress-woocommerce`
- :doc:`sys-swap-space`
- :doc:`app-woocommerce`

Database
========

.. tip:: We have two options for creating a MySQL backup.
         Our preference (at the moment) is to use
         `Backup database using cPanel, Terminal`_
         rather than
         `Backup database using phpMyAdmin`_.
         For more information, see `Restore Issue`_ (below)...

Backup database using cPanel, Terminal
--------------------------------------

*cPanel*, *Advanced*, *Terminal*, identify the folder containing your site e.g.
``www_hatherleigh_info``.
Check in ``wp-config.php`` for the database name, user and password,
then backup::

  mysqldump --user=www_hatherleigh_info --password=MyPassword --no-tablespaces www_hatherleigh_info > 2021-11-01-www_hatherleigh_info.sql

Download the backup to your server e.g::

  ssh
  wget ftp://72.720.72.72/2021-11-01-www_hatherleigh_info.sql --ftp-user=user@name.co.uk --ftp-password=abcdefgh

Then `Restore database`_ ...

Backup database using phpMyAdmin
--------------------------------

From `Backing Up Your Database`_.  Log into cPanel, then *phpMyAdmin*:

- Select the database in the left hand panel.
- *Export*
  (make sure the correct database name is listed in
  *Exporting tables from "..." database*).
- *Quick*
- Format *SQL*
- *Go*

e.g. ``wpbackup.sql``

Copy the database backup to your server.

Restore database
----------------

Restore the database::

  mysql --user=www_hatherleigh_info --password=MyPassword www_hatherleigh_info < wpbackup.sql

Restore Issue
-------------

MySQL had an issue restoring the backup::

  ERROR 2013 (HY000) at line 308244: Lost connection to MySQL server during query

The MySQL log contains the following::

  2021-11-01T09:06:02.289281Z 0 [ERROR] [MY-013183] [InnoDB] Assertion failure: ddl0builder.cc:1495:n >= IO_BLOCK_SIZE thread 140515207010048
  InnoDB: We intentionally generate a memory trap.
  InnoDB: Submit a detailed bug report to http://bugs.mysql.com.
  InnoDB: If you get repeated assertion failures or crashes, even
  InnoDB: immediately after the mysqld startup, there may be
  InnoDB: corruption in the InnoDB tablespace. Please refer to
  InnoDB: http://dev.mysql.com/doc/refman/8.0/en/forcing-innodb-recovery.html
  InnoDB: about forcing recovery.
  09:06:02 UTC - mysqld got signal 6 ;

I managed to workaround this issue by backing up using ``mysqldump`` and the
``--no-tablespaces`` option (`Backup database using cPanel, Terminal`_).

Although I did manage a successful restore after using
`Backup database using phpMyAdmin`_ by setting ``net-read-timeout = 60`` in
``/etc/mysql/mysql.cnf``:

.. image:: ./misc/mysql/2021-10-30-net-read-timeout.png

Files
=====

.. tip:: Downloading files directly to the server will save time downloading
         to your workstation, then copying back to the cloud.

.. tip:: `Backing Up Your WordPress Files`_ says, *The important files to*
         *back up would be your* ``wp-config.php`` *file, which contains your*
         *settings and your* ``wp-content`` *directory (plus its contents)*
         *which contains all your theme and plugin files*.

.. tip:: Browse the folders in your project before downloaded.  Check for
         very large files (e.g. old backups) and remove (if you are sure they
         aren't used).

.. tip:: For our old notes on WordPress backup, see :doc:`old/sys-wordpress`

*cPanel*, *Advanced*, *Terminal*, identify the folder containing your site e.g.
``public_html`` and archive::

  tar cvzf 2021-10-26-public_html.tgz public_html/

Download the archive to your server e.g::

  ssh
  wget ftp://72.720.72.72/2021-10-26-public_html.tgz --ftp-user=user@name.co.uk --ftp-password=abcdefgh

Extract the archive (as ``root``)::

  sudo -i root
  cd /home/web/repo/project/www.hatherleigh.info/live
  tar --strip-components=1 -xzf /home/patrick/repo/temp/2021-10-26-public_html.tgz

Re-run the Salt states (or this command to set the initial permissions)::

  chown -R www-data:www-data /home/web/repo/project/www.hatherleigh.info/live/


.. _`Backing Up Your Database`: https://wordpress.org/support/article/backing-up-your-database/
.. _`Backing Up Your WordPress Files`: https://wordpress.org/support/article/backing-up-your-wordpress-files/
