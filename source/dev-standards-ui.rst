UI Standards
************

.. highlight:: python

- Click here for :doc:`dev-standards`
- Click here for :doc:`dev-standards-code`
- Click here for :doc:`dev-standards-doc`
- Click here for :doc:`dev-standards-html`
- Click here for :doc:`dev-standards-js`
- Click here for :doc:`dev-standards-security`
- Click here for :doc:`dev-standards-sys`
- Click here for :doc:`dev-standards-tech`

Accessibility
=============

We really want to make our sites accessible.  For a good introduction, see the
`How I do an accessibility audit`_ YouTube video.

Adam Wathan (Tailwind UI) referred to http://html5doctor.com/ ...

The `GOV.UK, Testing for accessibility`_ web pages are probably worth reading.

Dashboard
=========

.. note:: Public pages will normally be in the ``web`` app.

          The ``dash`` app is generally for pages where the user has to be
          logged in.

          The ``settings`` app is generally for pages where a member of staff
          can configure settings, add records to look-up tables etc.

          These are not a hard and fast rules.  We will often want other apps
          where the user needs to be logged in.

Projects will normally have a dashboard and a settings page.  The URL for the
dashboard must be named ``project.dash`` and the URL for the settings page must
be named ``project.settings`` e.g::

  # from 'dash/urls.py'
  url(regex=r'^$',
      view=DashView.as_view(),
      name='project.dash'
      ),
  url(regex=r'^settings/$',
      view=SettingsView.as_view(),
      name='project.settings'
      ),

An app can make it easy for a developer to add items to the dashboard or
settings template by making an ``<app-name>/_dash.html`` or
``<app-name>/_settings.html`` template e.g:

.. code-block:: html

  {% include 'cms/_dash.html' %}
  {% include 'cms/_settings.html' %}

For example code, see:

- https://github.com/pkimber/cms/blob/master/cms/templates/cms/_dash.html
- https://github.com/pkimber/cms/blob/master/cms/templates/cms/_settings.html

Forms
=====

To make forms look good on desktop *and* mobile, see :ref:`app_base_forms`.

Icons
=====

Align
-----

To vertically align icons (from `Fixed Width Icons`_), use the ``fa-fw`` class
e.g::

  <i class="fa fa-search fa-fw"></i>

Apps
----

:doc:`app-invoice`

Usage
-----

Audit / superuser::

  # http://fontawesome.io/icon/cogs/
  <i class="fa fa-cogs"></i>

Calendar / booking::

  # http://fontawesome.io/icon/calendar/
  <i class="fa fa-calendar"></i>

Tick / check::

  <KbSvg::CheckSolid />

  # http://fontawesome.io/icon/check/
  <i class="fa fa-check"></i>

Waiting / spinner / time::

  # 'KbSpinner' uses ''<KbSvg::SpinningCircle />' internally
  <KbSpinner @caption="Please wait..." />

  # http://fontawesome.io/icon/clock-o
  <i class="fa fa-clock-o"></i>
  # https://fontawesome.com/v4/icon/spinner
  <i class="fa fa-spinner" aria-hidden="true"></i>

Download::

  <KbSvg::ArrowDownTray />

  # http://fontawesome.io/icon/cloud-download/
  <i class="fa fa-cloud-download"></i>

Report::

  <i class="fa fa-table"></i>

Download CSV (prefer ``fa-cloud-download`` for a download::

  # caption - Download CSV
  <i class="fa fa-file-excel-o"></i>

Mail / email::

  # http://fontawesome.io/icon/envelope-o/
  <i class="fa fa-envelope-o"></i>

Warning::

  # http://fontawesome.io/icon/exclamation-triangle/
  <i class="fa fa-warning"></i>

Link (internal)::

  # https://fontawesome.com/icons/link
  <i class="fa fa-link"></i>

Link (external)::

  <KbSvg::Link />

  # http://fontawesome.io/icon/external-link/
  <i class="fa fa-external-link"></i>

Template - HTML::

  # http://fontawesome.io/icon/file-code-o/
  <i class="fa fa-file-code-o"></i>

Header / footer::

  # http://fontawesome.io/icon/header/
  <i class="fa fa-header"></i>

Home::

  # http://fontawesome.io/icon/home/
  <i class="fa fa-home"></i>

Money / payment::

  # http://fontawesome.io/icon/gbp/
  <i class="fa fa-gbp"></i>

Audit / history / log / logging::

  <KbSvg::Clock />

  # https://fontawesome.com/v4.7.0/icon/history
  <i class="fa fa-history"></i>

Workflow::

  # http://fontawesome.io/icon/magic/
  <i class="fa fa-magic"></i>

Docs / notes::

  # https://fontawesome.com/v4/icon/sticky-note-o
  <i class="fa fa-sticky-note-o"></i>

Upload (attach / upload)::

  <KbSvg::PaperClip />
  <KbSvg::ArrowUpTray />

  # http://fontawesome.io/icon/paperclip/
  <i class="fa fa-paperclip"></i>

Edit / update::

  # http://fontawesome.io/icon/pencil-square-o/
  <i class="fa fa-edit"></i>

Help, information, info::

  # http://fontawesome.io/icon/info/
  <i class="fa fa-info-circle"></i>

Next / previous::

  # https://fontawesome.com/v4.7.0/icon/arrow-circle-right::
  <i class="fa fa-arrow-circle-right"></i>

Phone::

  # http://fontawesome.io/icon/phone/
  <i class="fa fa-phone"></i>

Add / create::

  # http://fontawesome.io/icon/plus/
  <i class="fa fa-plus"></i>

Previous / next::

  # https://fontawesome.com/v4.7.0/icon/arrow-circle-left
  <i class="fa fa-arrow-circle-left"></i>

Retry / sync::

  # http://fontawesome.io/icon/refresh/
  <i class="fa fa-refresh"></i>

Dash (back)::

  # http://fontawesome.io/icon/reply/
  <i class="fa fa-reply"></i>

Star / highlight::

  # https://fontawesome.com/v4.7.0/icon/star::
  <i class="fa fa-star"></i>

Settings::

  # https://fontawesome.com/v4.7.0/icon/cog
  <i class="fa fa-cog" aria-hidden="true"></i>

Login / Sign in::

  # https://fontawesome.com/v4.7.0/icon/sign-in
  <i class="fa fa-sign-in" aria-hidden="true"></i>

GDPR::

  # https://fontawesome.com/icons/shield-check
  <i class="fa fa-shield"></i>

Money / payments::

  # http://fontawesome.io/icon/shopping-cart/
  <i class="fa fa-shopping-cart"></i>

Sort (up / down)::

  <KbSvg::ChevronRight />
  <KbSvg::ChevronDown />
  <KbSvg::ChevronUp />

  # https://fontawesome.com/icons/sort/
  <i class='fa fa-sort'></i>
  <i class='fa fa-sort-asc'></i>
  <i class='fa fa-sort-desc'></i>

Page::

  # http://fontawesome.io/icon/file-text-o/
  <i class="fa fa-file-text-o"></i>

Project::

  #  https://fontawesome.com/v4/icon/bullseye
  <i class="fa fa-bullseye"></i>

Search::

  # http://fontawesome.io/icon/search/
  <i class="fa fa-search"></i>

Dashboard::

  # http://fontawesome.io/icon/tachometer/
  <i class="fa fa-tachometer"></i>

Delete (or cross - opposite to a tick)::

  # http://fontawesome.io/icon/trash-o/

  <i class="fa fa-trash-o"></i>
  # or
  <i class="fa fa-times"></i>

Undo / undelete::

  # https://fontawesome.com/v4.7.0/icon/undo
  <i class="fa fa-undo"></i>

User::

  <KbSvg::User />

  # http://fontawesome.io/icon/user/
  <i class="fa fa-user"></i>

Pure
====

Grid
----

Two columns

.. code-block:: html

  <div class="pure-g">
    <div class="pure-u-1 pure-u-md-1-2">
      <!-- spacing -->
      <div class="l-box">
      <div class="r-box">

Menu
----

.. code-block:: html

  <div class="pure-g">
    <div class="pure-u-1">
      <div class="pure-menu pure-menu-horizontal">
        <ul class="pure-menu-list">
          <li class="pure-menu-item">
            <a href="{% url 'booking.list' %}" class="pure-menu-link">
              <i class="fa fa-calendar"></i>
              Bookings
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>

Table
-----

.. code-block:: html

  <table class="pure-table pure-table-bordered">
    <thead>
      <tr valign="top">
        <th>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr valign="top">
        <td>
        </td>
      </tr>
    </tbody>
  </table>

Template
========

Date
----

Short date e.g. ``05/09/2015 13:30``:

.. code-block:: html

  {{ item.checkout_date|date:'d/m/Y H:i' }}

In python::

  >>> x.strftime('%d/%m/%Y %H:%M')
  '20/05/2011 10:55'

Tags
----

From `Two Scoops of Django`_, *the convention we follow is*
``<app_name>_tags.py`` e.g. ``cms_tags.py``.


.. _`booking_form.html`: https://gitlab.com/kb/booking/blob/1242-community-centre/booking/templates/booking/booking_form.html#L21
.. _`Fixed Width Icons`: https://fontawesome.com/how-to-use/on-the-web/styling/fixed-width-icons
.. _`GOV.UK, Testing for accessibility`: https://www.gov.uk/service-manual/technology/testing-for-accessibility
.. _`How I do an accessibility audit`: https://www.youtube.com/watch?v=cOmehxAU_4s
.. _`Two Scoops of Django`: http://twoscoopspress.org/products/two-scoops-of-django-1-6
