Monitor
*******

.. highlight:: bash

https://gitlab.com/kb/monitor/

Icon::

  <i class="fa fa-chain-broken"></i>

This app has two roles:

1. `Monitor Services`_ e.g. domain name or SSL certificate expiry
2. `Monitor Costings`_ e.g. hosting / storage to make sure we invoice customers
   correctly.

Configuration
=============

- For Digital Ocean, create a *Personal Access Tokens* (*API*, *Tokens*).
- For Linode, *My Profile*, *API Tokens*, create a *Personal Access Tokens*,
  *Read Only*.

Add the following to ``settings/base.py``::

  DIGITAL_OCEAN_TOKEN = get_env_variable("DIGITAL_OCEAN_TOKEN")
  LINODE_TOKEN = get_env_variable("LINODE_TOKEN")
  MONITOR_COSTINGS_DEFAULT_PILLAR = get_env_variable(
      "MONITOR_COSTINGS_DEFAULT_PILLAR"
  )

Add the tokens to your environment e.g. ``.env.fish``::

  set -x MONITOR_COSTINGS_DEFAULT_PILLAR "yb"
  set -x DIGITAL_OCEAN_TOKEN "dop_v1_123"
  set -x LINODE_TOKEN "ab123"

.. note:: If the company default pillar is ``~/Private/deploy/pillar-yb/``,
          then ``MONITOR_COSTINGS_DEFAULT_PILLAR`` should be set to ``yb``.

Monitor Services
================

The ``monitor`` app is initialised with a ``Monitor`` header record:

- Each ``Monitor`` row is identified using a ``slug``
  e.g. ``Monitor.DOMAIN_NAME``.
- Each ``Monitor`` row links to a report class.
  The report class does the actual checking.
- The link from the ``Monitor`` to the report class is created using the
  ``app``, ``module`` and ``report_class`` fields.
- The ``Monitor`` will initiate a check on the report class by calling the
  ``initialise`` method.  This will be followed by a call to the ``monitor``
  method for each row on the report class.

Monitor Costings
================

Management Commands
-------------------

Parse the Salt pillar to get the domain configuration::

  django-admin costing-data-domain

.. tip:: The dump file is ``costing-data-domain.json``

Read the domain data (``costing-data-domain.json``) and combine with
hosting information from the Digital Ocean, Linode and CloudSpace APIs::

  django-admin costing-data-server

.. tip:: The dump file is ``costing-data-server.json``

Read the domain data (``costing-data-domain.json``) and combine with
hosting information from SparkPost API::

  django-admin costing-data-mail

.. tip:: The dump file is ``costing-data-mail.json``

Read the domain data (``costing-data-domain.json``) and combine with
hosting information from rsync.net::

  # WIP (not completed)
  django-admin costing-data-backup

.. tip:: The dump file is ``costing-data-backup.json``

Create the costing summary by contact::

  django-admin costing-contact-summary 20

.. tip:: The parameter is the monthly cost of the SparkPost service.

.. tip:: The output file is ``costing-contact-summary.csv``

Open ``costing-contact-summary.csv`` using LibreOffice.
