NoMachine
*********

.. highlight:: bash

Player (to access Windows)
==========================

From Linux (to access a Windows workstation running the NoMachine server)::

  /usr/NX/bin/nxplayer

Remove
======

::

  dpkg --purge nomachine && sudo rm -rf /usr/NX /etc/NX
