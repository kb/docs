crm
***

.. highlight:: python

https://gitlab.com/kb/crm

Icon
====

::

  <i class="fa fa-ticket"></i>

Initialise
==========

For the standard view, click *Settings*, *Display Notify List*

.. image:: ./misc/app-crm/2024-06-25-settings.png

Then tick *Display notify list*:

.. image:: ./misc/app-crm/2024-06-25-settings-display-notify-list.png

Management Commands
===================

Pandas - Ticket count by Month for a Contact
--------------------------------------------

Display the ticket count for a contact for the last 12 months::

  django-admin pandas-contact-ticket 12

Source code `pandas-contact-ticket`_ ...

Reports
=======

``ContactTicketCountReport``

    Contact List (with a count of tickets)

``TicketsByKanbanLaneReport``

    Tickets by Kanban Lane

Ticket
======

To re-open a complete ticket::

  from crm.models import Ticket
  ticket = Ticket.objects.get(pk=386)
  ticket.complete = None
  ticket.complete_user = None
  ticket.save()

User
----

To create a user on the CRM::

  from django.contrib.auth.models import User
  user = User.objects.create_user("b.zip")
  user.set_password("apples-or-oranges")
  user.first_name = "B"
  user.last_name = "Zip"
  user.is_staff = True
  user.save()

  from contact.models import Contact
  Contact.objects.create_contact(user=user)


.. _`pandas-contact-ticket`: https://gitlab.com/kb/crm/-/blob/master/crm/management/commands/pandas-contact-ticket.py
