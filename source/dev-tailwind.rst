Tailwind
********

.. highlight:: javascript

- :doc:`dev-tailwind-ember`
- :doc:`dev-tailwind-ui`

Learning
========

- `Adam Wathan, Tailwind UI Preview`_

Media Breakpoints
=================

Two options:

1. ``jonsugar/show-tailwind-breakpoint.html``
   https://gist.github.com/jonsugar/6bce22bd7d3673294caad36046c2b7cb

.. tip:: Make this into an ``ember-addon`` component?

2. To see the media breakpoints (very nice thank you Tim):

.. code-block:: html

  <div class="bg-black text-white bold fixed">
    <span class="hidden invisible sm:visible sm:inline">sm</span>
    <span class="hidden invisible md:visible md:inline">md</span>
    <span class="hidden invisible lg:visible lg:inline">lg</span>
    <span class="hidden invisible xl:visible xl:inline">xl</span>
  </div>

Requirements
============

Gulp:

.. code-block:: bash

  npm i -g gulp

Setup
=====

Create a ``theme`` folder alongside ``templates`` and ``tests`` the main app
folder:

.. code-block:: text

  <APPNAME>
  └───<APPNAME>
      └───src
          │   <APPNAME>.css
      │   gulpfile.js
      │   index.html
      │   package.json
      │   tailwind.config.js
  └───example_<APPNAME>

Add the boilerplate of the following (seek later versions from a recent app).

``<APPNAME>.css``
-----------------

.. code-block:: text

  @tailwind base;
  @tailwind components;

  .thing-to-style {
    @apply tailwind-class1 tailwind-class2
  }

  @tailwind utilities;

``gulpfile.js``
---------------

::

  const { src, dest, watch, series, parallel } = require("gulp");
  const autoprefixer = require("autoprefixer"),
    postcss = require("gulp-postcss"),
    purgecss = require('gulp-purgecss'),
    replace = require("gulp-replace"),
    tailwindcss = require("tailwindcss");
  var browserSync = require("browser-sync").create();
  var path = require("path");

  let parth = process.cwd().split(path.sep);
  const APPNAME = parth[parth.length - 2];

  const FILES = {
    watchPaths: ["./src/*.css", "./index.html", "./tailwind.config.js"],
    cssPath: `./src/${APPNAME}.css`
  };

  function cssTask() {
    return src(FILES.cssPath)
      .pipe(postcss([tailwindcss("./tailwind.config.js"), autoprefixer()]))
      // .pipe(
      //   purgecss({
      //     content: ['index.html']
      //   })
      // )
      .pipe(dest("css"));
  }

  function cacheBustTask() {
    let cbString = new Date().getTime();
    return src(["index.html"])
      .pipe(replace(/cb=\d+/g, "cb=" + cbString))
      .pipe(dest("."));
  }

  const build = cssTask;

  function watchTask() {
    browserSync.init({
      server: {
        baseDir: "./"
      }
    });
    watch(FILES.watchPaths, build).on("change", browserSync.stream);
  }

  exports.build = series(build, cacheBustTask);

  exports.default = series(build, watchTask);


``index.html``
---------------

Use CDN links:

.. code-block:: html

  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <title>
        Compose Theme Dashboard
      </title>
      <script src="https://use.fontawesome.com/d7718f5d3c.js"></script>
      <link
        href="https://fonts.googleapis.com/css?family=Ubuntu:400,700"
        rel="stylesheet"
        type="text/css"/>
      <link rel="stylesheet" href="css/<THEMENAME>.css?cb=0"/>
    </head>
    <body>

    </body>
  </html>

``package.json``
----------------

Use CDN links::

  {
    "author": "Your Name",
    "description": "Tailwind for <APPNAME>",
    "title": "<APPNAME>",
    "devDependencies": {
      "autoprefixer": "^9.7.1",
      "browser-sync": "^2.24.4",
      "gulp": "^4.0.2",
      "gulp-postcss": "^8.0.0",
      "gulp-purgecss": "^1.2.0",
      "gulp-replace": "^1.0.0",
      "prettier": "1.16.4",
      "tailwindcss": "^1.1.4"
    },
    "scripts": {
      "build": "gulp build",
      "prettier": "prettier --write \"**/*.{js,json,css,html}\" \"!package.json\"",
      "start": "gulp"
    }
  }

``tailwind.config.js``
----------------------

::

  const {
    colors
  } = require('tailwindcss/defaultTheme')

  module.exports = {
    theme: {
      fontFamily: {
        'body': 'Ubuntu',
      },
      colors: {
        primary: '#1f8dd6',
        secondary: '#265778',
        grayHeavy: colors.gray["800"],
        grayLight: colors.gray["400"],
        black: colors.black,
        white: colors.white,
      }
    },
    corePlugins: {
      container: false
    },
    variants: {
      backgroundColor: ['responsive', 'hover', 'focus'],
    }
  };

In the ``theme`` folder you created run ``npm i``.

Then run ``gulp``.

Your index page will load with browser-sync ready to start styling.


.. _`Adam Wathan, Tailwind UI Preview`: https://vimeo.com/393580241/82c6d7c5f6
