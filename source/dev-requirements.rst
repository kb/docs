Requirements
************

.. highlight:: bash

Before adding a new requirement to a project:

1. Our core technologies are Django and Ember.
2. Ask... is it really required?  We want to re-use good code, but keep
   requirements to a minimum.
3. Check the project is active i.e. has recent commits and is keeping up with
   recent releases e.g. Django 2
4. Discuss with the team before adding.

Common requirements::

  APScheduler==3.10.4
  attrs==25.1.0
  beautifulsoup4==4.12.3
  # bleach==6.x.x breaking changes!!
  # bleach==6.2.0
  bleach==4.1.0
  # To fix, object has no attribute 'is_ajax'
  # git+https://github.com/brack3t/django-braces.git@df847c9a517a09d81ccd27609af8b96d20887922#egg=django-braces
  django-braces==1.16.0
  # for linux...
  django-dramatiq==0.12.0
  # do not use - django-dramatiq for Windows (see :doc:`dev-dramatiq`)
  # kb-django-dramatiq==0.0.08
  django-extensions==3.2.3
  django-filter==24.3
  django-reversion==5.1.0
  django-taggit==6.1.0
  #
  # Django==4.1.8 no support for Postgres 10, https://www.kbsoftware.co.uk/crm/ticket/6389/
  # Psycopg3 Binary and Django 4.2 Installation Quick Tip
  # Trying
  # Django==5.1.5
  # https://learndjango.com/tutorials/psycopg3-binary-and-django-42-installation-quick-t
  Django==4.2.16
  # For Django 5, use
  # djangorestframework==3.15.2
  djangorestframework==3.14.0
  dramatiq[redis,watch]==1.17.1
  easy-thumbnails==2.10
  humanize==4.11.0
  #
  # For Windows...
  psycopg[binary]==3.2.1
  # For Linux and Windows...
  psycopg==3.2.4
  #
  # Django==4.1.8 no support for Postgres 10, https://www.kbsoftware.co.uk/crm/ticket/6389/
  psycopg2==2.9.3
  pycryptodome==3.18.0
  python-dateutil==2.9.0
  pytz==2024.2
  rich==13.9.2

Other requirements::

  anytree==2.12.1
  boto==2.36.0
  celery==4.3.0
  # for windows
  # celery==3.1.26.post2
  # try and remove this...
  # coreapi==2.3.3
  cssselect==1.1.0
  django-anymail==10.1
  django-axes==6.5.1
  django-bootstrap3==22.2
  django-colorful==1.3
  django-cookies-samesite==0.9.0
  django-cors-headers==3.2.0
  django-countries==7.4.2
  django-crispy-forms==1.7.2
  # 21/12/2022, django-formtools==2.4, formtools/wizard/views.py:222:
  # in get_form_list, RecursionError: maximum recursion depth exceeded
  django-formtools==2.3
  # The project is unmaintained!
  # For Windows
  # django-mptt==0.14.0
  django-mptt==0.16.0
  django-nvd3==0.9.7
  # no longer using  'django-pyodbc-azure' or 'django-azure-sql-backend'
  django-mssql-backend==2.8.1
  # See https://github.com/django-recaptcha/django-recaptcha/blob/main/CHANGELOG.md
  django-recaptcha==4.0.0
  # For Django 5, use
  # djangorestframework-jsonapi==7.1.0
  djangorestframework-jsonapi==6.1.0
  # https://www.kbsoftware.co.uk/docs/dev-django.html#django-4
  django-sendfile2==0.7.1
  django-statsd-mozilla==0.3.14
  django-storages-redux
  django-waffle==0.11.1
  django-webserver[waitress]==1.2.0
  djangoajax==3.3
  djrill==2.1.0
  dnspython==1.15.0
  docutils==0.14
  # elasticsearch==8.17.1
  elasticsearch==6.8.1
  Flask-Cors==3.0.6
  Flask==1.0.2
  fpdf2==2.7.8
  git+https://github.com/pyinstaller/pyinstaller.git@c7a24e5a023bdd7d2d6c571f6bab0513d8e809a1#egg=PyInstaller
  google-api-python-client==2.38.0
  google-auth==2.6.0
  hg+https://bitbucket.org/schinckel/django-jsonfield#egg=jsonfield
  hiredis==2.4.0
  html5lib==1.1
  iso8601==0.1.10
  Jinja2==3.1.4
  lxml==5.3.0
  mailchimp3==3.0.14
  mandrill==1.0.60
  mozilla-django-oidc==4.0.1 
  mysqlclient==2.1.0
  pandas==2.1.3
  paramiko==3.4.1
  Pillow==11.0.0
  psd-tools==1.4
  # to do an offline install on windows, 'pip install psutil==5.9.5'
  psutil==6.0.0
  pydantic==2.8.2
  PyInstaller==3.4
  pypiwin32==223
  # Replace pypyodbc==1.3.5 with a maintained package
  # https://www.kbsoftware.co.uk/crm/ticket/4957/
  # On Linux, you may need to install:
  # apt install unixodbc-dev
  pyodbc==5.1.0
  python-docx==1.1.2
  python-slugify==6.1.2
  python-whois==0.9.5
  pyxero==0.9.2
  # CVE-2017-18342 - https://github.com/advisories/GHSA-rprw-h62v-c2w7
  PyYAML==6.0.1
  test_dev_test_project_navigatorraygun4py==4.3.0
  # install 'redis' with 'hiredis' (see above)
  redis==5.0.8
  # for windows
  # redis==2.10.6
  reportlab==4.2.5
  # see urllib3 (below)
  requests==2.32.3
  scp==0.15.0
  social-auth-app-django==1.1.0
  sparkpost==1.3.10
  spyne==2.11.0
  statsd==3.0.1
  # stripe==2.76.0
  # stripe==11.1.1 (not tested yet)
  stripe==2.71.0
  tasklib==1.1.0
  transitions==0.9.2
  # for compatibility with requests
  # urllib3==1.26.13
  urllib3==2.0.4
  # for celery - not version 5.0.0 is not compatible
  vine==1.3.0
  wagtail==6.3.2
  # was waitress==2.1.2
  waitress==3.0.0
  whitenoise==6.7.0
  WooCommerce==3.0.0
  workalendar==16.4.0
  xmltodict==0.14.2
  Yapsy==1.10.423

For local requirements (``requirements/local.txt``)::

  django-debug-toolbar

To update the version of a dependency in ``base.txt``::

  find . -name "base.txt" | xargs sed -i 's/Django==1.6.1/Django==1.6.2/g'
