Fish Shell
**********

.. highlight:: bash

nvm
===

.. warning:: I am not longer using ``nvm``.
             I am hoping to use https://volta.sh/ instead!

::

  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

  # https://github.com/jorgebucaran/fisher
  curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish

  fisher add FabioAntunes/fish-nvm
  fisher add edc/bass

  nvm install 10.19
  nvm use 10.19
  npm -v
