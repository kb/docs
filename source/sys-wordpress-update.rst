WordPress - Update
******************

- :doc:`sys-wordpress`
- :doc:`sys-wordpress-issues`
- :doc:`sys-wordpress-plugin`
- :doc:`sys-wordpress-security`
- :doc:`sys-wordpress-transfer`
- :doc:`sys-wordpress-woocommerce`
- :doc:`sys-swap-space`
- :doc:`app-woocommerce`

Learning!
=========

`How to Update WordPress – Manually or Automatically`_

.. warning:: I am not sure which files to copy / update, but the 
             `WordPress Documentation - Manual Update`_
             should make it clear?!

Maintenance Mode
================

If your WordPress site is stuck in maintenance mode, delete the
``.maintenance`` file in the root of the site e.g::

  rm /home/web/repo/project/www.hatherleigh.info/live/.maintenance


.. _`How to Update WordPress – Manually or Automatically`: https://www.youtube.com/watch?v=hLo_p6kk44k
.. _`WordPress Documentation - Manual Update`: https://wordpress.org/support/article/updating-wordpress/#manual-update
