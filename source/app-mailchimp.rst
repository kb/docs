MailChimp
*********

This app is designed to synchronise GDPR consent information from the MailChimp
campaigns.

.. note:: MailChimp *Campaigns* have an associated mailing *List*.
          It is worth noting that the *List* can be deleted, but the *Campaign*
          still exists.
          I ignore a deleted list on a campaign if the campaign was sent more
          than 90 days ago.

Settings
========

Add the following to ``base.py``::

  MAILCHIMP_API_KEY = get_env_variable("MAILCHIMP_API_KEY")
  MAILCHIMP_USER_NAME = get_env_variable("MAILCHIMP_USER_NAME")

Environment
-----------

Add the following to ``.gitlab-ci.yml``:

.. code-block:: yaml

  test:
    script:
    - export MAILCHIMP_API_KEY="11111e111ee1e1e1111111111eeee11e-us12"
    - export MAILCHIMP_USER_NAME="my-mailchimp-user-name"

For testing, add the following to ``.env.fish``::

  set -x MAILCHIMP_API_KEY "11111e111ee1e1e1111111111eeee11e-us12"
  set -x MAILCHIMP_USER_NAME "my-mailchimp-user-name"

.. tip:: Override in the ``.private`` file to use *live* keys...
