Work
****

The ``work`` app is our generic extension to the ``workflow`` app.
The ``Flow`` database model is a link to the *Flowable* process.

Apps
====

The workflow app (:doc:`app-apps`) is **not** used to control access to the
REST / Ember API (`Ticket 5555`_).

For now, the ``App`` model is just used to help people find their way to the
Ember app.

API
===

Add the ``apiUrl`` variable to the start process of the workflow.  This will add
the URL of the API to the variables for the process.

Create Workflow
---------------

Use a script task to set the following parameters::

  var ObjectMapper = com.fasterxml.jackson.databind.ObjectMapper;

  var data = {};
  data.processKey = "navigatorTrainingTwo";
  data.processId = execution.getProcessInstanceId();
  # comma separated list of user IDs e.g. '1,2,3'
  data.userPks = execution.getVariable("userPk");

  var httpTaskData = JSON.stringify(data);
  var json = new ObjectMapper().readTree(httpTaskData);
  execution.setVariable("httpTaskData", json);]]></script>

.. note:: Additional variables will be added to the new workflow (if they are
          included in the start event).

``POST`` to this URL in an HTTP task::

  ${apiUrl}/workflow/

Refresh Workflow
----------------

Update the workflow variables.  Useful for long running process where
information may change over time e.g. the manager for an employee.

``PUT`` to this URL in an HTTP task::

  ${apiUrl}/refresh-variables/${execution.getProcessInstanceId()}/

Django
======

To create a workflow:
http://localhost:8000/work/create/

Ember vs Django
===============

The BPMN (XML) file includes a ``formKey`` e.g::

  flowable:formKey="work"

The value of the ``formKey`` is processed by the ``build_django_url`` function
in ``workflow/activiti.py`` to determine if the task form should be displayed
using Django or Ember.

Django
------

The ``build_django_url`` function in ``workflow/activiti.py`` decides whether
to use Django or Ember for the task form.

The ``work`` app has a ``work`` URL name which can be used to display a task
using a Django form.

Ember
-----

To use an Ember form to process a task, set the ``formKey`` to an empty string.

.. warning:: The information returned from ``build_django_url`` (see above)
             is passed to Ember via the API, so the following may no longer be
             useful (or may need to be updated) (PJK 17/05/2023).

The ``Task::List::TaskUrl`` component decides whether to use Django or Ember
for a task::

  front/addon/components/task/list/task-url.hbs

.. tip:: If the task should be handled by Ember, ``is_work`` will be ``true``.

When the task is completed, we redirect to the next task.
The code for handling this is ``taskRedirectTask`` in::

  front/addon/controllers/task/form.js

To handle all tasks using the Ember UI:

1. Remove ``else`` from ``task-url.hbs``
2. Update ``taskRedirectTask`` so all tasks ``transitionToRoute``.
3. Remove ``{{#if task.is_work}}`` from ``addon/templates/task/form.hbs``.

.. tip:: 13/10/2023, Our ``task/form/str`` component has support for Markdown
         in the help text, but has not been added elsewhere (yet)
         For more information, see :ref:`dev-ember-addons-kb-markdown`.


.. _`Ticket 5555`: https://www.kbsoftware.co.uk/crm/ticket/5555/
