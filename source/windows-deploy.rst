Deploy on Windows
*****************

.. highlight:: bash

python
======

Download and deploy a 64bit version of python 3.5.
I used ``python-3.5.3-amd64.exe``.

To try and solve the issues running python services
(see `Celery`_ and :ref:`issues_celery_windows_service`), I asked the installer
to do the following:

1. Installed python for all users
2. Added python to the ``PATH``.

In the end, I think the issue was solved by installing
``pywin32-221.win-amd64-py3.5.exe`` as a global package, so perhaps we don't
need to add python to the ``PATH`` or for all users?

Activiti
========

.. note:: The steps taken to intall Activiti on our 1&1 Windows server

pgAdmin::

  create role       activiti
  password          yourDbPassword

  create database
  owner             activiti
  template          template0

Download the JDK from
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
(selected 8u92)

  Click install *Next > ;  Next > ; Next >; Close*

  Note the installation directory for use in the next step

Download Tomcat from https://tomcat.apache.org/download-80.cgi

  Choose 32-bit/64-bit Windows Service installer and run Click *Next ; I Agree
  ; Next >*;

  Then on Configuration Options dialog change ``Http/1.1 Connector port: 8080``
  (default but may have to change)

::

 Tomcat Administrator Username  admin
 Password                       yourTomcatPassword

On Java Virtual Machine path Select, ensure the following is selected::

  C:\Program Files\Java\jre1.8.0_92

Accept the default installation directory and click install and then close
(leaving *Run Tomcat* selected)

  In *Control Panel* | *Administrative tools* | *Services* click on *Apache
  Tomcat 8.0* *Tomcat8* and choose startup type *Automatic* and press *OK*.

Download the Postgresql JDBC jar from
https://jdbc.postgresql.org/download/postgresql-9.4.1208.jar and place it in::

  C:\Program Files\Apache Software Foundation\Tomcat 8.0\lib

Download ``activiti-6.0.0.Beta2.zip`` from http://activiti.org/download.html

Using file explorer browse to::

  C:\Users\Administrator\Downloads\activiti-6.0.0.Beta2.zip\activiti-6.0.0.Beta2\wars

and copy the ``activiti-rest.war`` file to::

  C:\Program Files\Apache Software Foundation\Tomcat 8.0\webapps

Tomcat will automatically extract the webapp.

  After about a minute you should be able to type:
  http://localhost:8080/activiti-rest/service/repository/deployments/ and see a
  json dataset containing the currently deployments.

Configure activiti rest to use postgresql as follows:

Alter the::

  C:\Program Files\Apache Software Foundation\Tomcat 8.0\webapps\activiti-rest\WEB-INF\classes\db.properties

as follows::

  db=activiti
  jdbc.driver=org.postgresql.Driver
  jdbc.url=jdbc:postgresql://localhost:5432/activiti
  jdbc.username=activiti
  jdbc.password=yourDbPassword

Restart Tomcat using the *services* menu.

.. _windows_celery:

Celery
======

To install the Celery services, start by installing python (see `python`_):

.. code-block:: winbatch

  cd C:\inetpub\wwwroot\your-app-folder\
  venv\Scripts\activate.bat
  python celery_service_worker.py install
  python celery_service_beat.py install

.. note:: For troubleshooting see, :ref:`issues_celery_windows_service`...

Deploy
======

Use our ``offline`` module at https://gitlab.com/kb/offline

FastCGI
=======

Upload File Size and Timeout Issues
-----------------------------------

In IIS if there are problems with Timeouts, etc, Open IIS then:

FASTCGI:

1. Locate the root node for the server (parent of Sites).
2. Find FastCGI settings
3. Locate the setting for the website with the issue. There should be a FastCGI
   settings for each of your Django sites on Initialise
4. Click to Edit:(secs)::

    Activity Timeout: 3600
    Idle Timeout: 3600
    Request Timeout: 3600

SITE:

1. Locate the root node for the Site
2. Advanced Settings::

    Connection Timeout (secs): 12000
    Failed Requests Tracing -->
      Directory: %SystemDrive%\inetpub\logs\FailedReqLogFiles
      Enabled: True

3. Locate the Request Filtering icon::

    Maximum Allowed Content length: 3147483648

4. Add to web.config::

    <system.webServer>
        <security>
          <requestFiltering>
            <requestLimits maxAllowedContentLength="3147483648">
                      <headerLimits>
                      </headerLimits>
                  </requestLimits>
          </requestFiltering>
        </security>

APP POOL:

1. Locate the App pool for your Site
2. Advanced Settings::

    Ping Maximum Response Time: 360

Release
=======

Use our ``offline`` module at https://gitlab.com/kb/offline


.. _`Run applications in the background using Task Scheduler`: https://www.calazan.com/windows-tip-run-applications-in-the-background-using-task-scheduler/
