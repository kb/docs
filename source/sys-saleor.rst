Saleor
******

Deployment
==========

For our first attempt, we deployed Saleor onto two separate servers:

1. https://ecommerce.hatherleigh.info/ for the ``saleor-dashboard``
2. https://saleor.hatherleigh.info/ for the Saleor API

This was to workaround issues with *the "back" URL*
(see `more recent install notes`_ for details).

For future deployments, we can deploy two sites to the same server.
The two domain names will keep the Nginx configuration separate.

Install
=======

.. tip:: The `more recent install notes`_ are here...

Linux install uses Docker, so follow `Manual Installation on Windows`_

::

  cd ~/dev/src/
  wget https://github.com/saleor/saleor/archive/refs/tags/3.1.14.zip
  unzip 3.1.14.zip
  cd saleor-3.1.14/
  python3 -m venv venv
  source venv/bin/activate.fish                                                                                                                                                                                             
  pip install -r requirements.txt 
  createuser --username=postgres --superuser --pwprompt saleor  # Enter password for new role: saleor
  # if you need to start with a clean database
  # dropdb --username=postgres saleor
  createdb --username=postgres --owner=saleor saleor
  python manage.py migrate --no-input

::

  python manage.py createsuperuser
  # Email: patrick@kbsoftware.co.uk
  # Password: letmein1  (minimum-length 8 characters)

::

  # In ~/dev/src/saleor-3.1.14/
  # create a .env.fish file with these contents
  source venv/bin/activate.fish
  set -x ALLOWED_HOSTS "localhost,127.0.0.1"
  # may only be needed for the node app ('saleor-dashboard')
  set -x API_URI "http://localhost:8000/graphql/"
  set -x DJANGO_SETTINGS_MODULE saleor.settings
  echo "ALLOWED_HOSTS:" $ALLOWED_HOSTS
  echo "API_URI:" $API_URI
  echo "DJANGO_SETTINGS_MODULE:" $DJANGO_SETTINGS_MODULE

::

  source .env.fish
  python manage.py runserver 0.0.0.0:8000

Saleor Dashboard Install
========================

::

  cd ~/dev/src/
  wget https://github.com/saleor/saleor-dashboard/archive/refs/tags/3.3.2.zip
  unzip 3.3.2.zip
  cd saleor-dashboard-3.3.2/

Install Volta if required::

  curl https://get.volta.sh ¦ bash

and once installed, open a new terminal window to continue::

  volta install node@14
  npm install

::

  # create a .env.fish file with these contents
  set -x API_URI "http://localhost:8000/graphql/"

::

  source .env.fish
  npm start

Browse to http://localhost:9000/

Configuration
=============

- Activate the Channel
- Give the product variant a price.
- Before using a warehouse it needs to be made *Public*
  (or use the ``WarehouseUpdate`` API)

Issues
======

The search does not appear to activate until you have entered 6 characters.


.. _`Manual Installation on Windows`: https://docs.saleor.io/docs/3.0/developer/installation/windows
.. _`more recent install notes`: https://docs.google.com/document/d/1avlknFNqcbf3wp_OUvDbuufLjb39SOVXf_W3JcyKk9s/edit#bookmark=id.eykiod8sr72k
