Google Development
******************

- :doc:`dev-captcha`
- :doc:`sys-google-apps`

We are using a service account to retrieve emails and user account details.
For more information, see `Using OAuth 2.0 for Server to Server Applications`_

.. image:: ./misc/google-service-account.png

We are using the following scopes::

  SCOPES = [
      "https://www.googleapis.com/auth/admin.directory.user.readonly",
      "https://www.googleapis.com/auth/gmail.readonly",
  ]

The permissions are given in the GSuite *Admin console*:

- Go to your Google Apps domain’s Admin console.
- Select *Security* from the list of controls.
- Select *Advanced settings* from the list of options.
- Select *Manage API client access* in the Authentication section.
- In the *Client name* field enter the service account's ``client_id``.
- In the *One or More API Scopes* field enter the list of scopes that your
  application should be granted access to.
- Click *Authorise*.

.. image:: ./misc/google-service-account-client-access.png


.. _`Using OAuth 2.0 for Server to Server Applications`: https://developers.google.com/api-client-library/python/auth/service-accounts
