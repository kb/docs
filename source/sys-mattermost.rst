MatterMost - Install and Update
*******************************

.. highlight:: bash

- :doc:`mattermost`

Upgrade
=======

For more information, see `Upgrading Mattermost Server`_

::

  ssh chat.yourbiz.co.uk

As ``root``::

  supervisorctl
  stop mattermost

As the ``web`` user::

  # backup the mattermost site
  backup.chat.yourbiz.co.uk.sh

  cd ~/repo/temp/
  # create a folder
  mkdir mattermost-$(date +'%F-%H-%M')
  cd mattermost-20xx-xx-xx-xx-xx/

.. tip:: I think the downloads are here:
         `mattermost-team-edition-server-archive`_

::

  # cd into the folder and get the 'tar' file for the latest release
  wget https://releases.mattermost.com/x.xx.x/mattermost-team-x.xx.x-linux-amd64.tar.gz
  tar -xf mattermost*.gz --transform='s,^[^/]\+,\0-upgrade,'

  # make a copy of the existing mattermost folder
  cd ~/repo/project/chat.yourbiz.co.uk/
  cp -ra live/ mattermost-back-$(date +'%F-%H-%M')/

  # list the files which will be removed
  find live/ live/client/ -mindepth 1 -maxdepth 1 \! \( -type d \( -path live/client -o -path live/client/plugins -o -path live/config -o -path live/logs -o -path live/plugins -o -path live/data \) -prune \) | sort | xargs echo rm -r
  # remove the files
  find live/ live/client/ -mindepth 1 -maxdepth 1 \! \( -type d \( -path live/client -o -path live/client/plugins -o -path live/config -o -path live/logs -o -path live/plugins -o -path live/data \) -prune \) | sort | xargs rm -r

  mv live/plugins/ live/plugins~
  mv live/client/plugins/ live/client/plugins~

  cp -an ~/repo/temp/mattermost-20xx-xx-xx-xx-xx/mattermost-upgrade/. live/
  rm -r ~/repo/temp/mattermost-20xx-xx-xx-xx-xx/mattermost-upgrade/
  # remove the *copy of the existing mattermost folder
  mv mattermost-back-0000-00-00-00-00 ~/repo/temp/backup-mattermost/

As ``root``::

  supervisorctl
  start mattermost

Upgrade your ``config.json`` schema:

- Browse to your MatterMost instance, https://chat.yourbiz.co.uk/
- Open the *System Console* and change a setting, then revert it.
  e.g. *Site Configuration*, *Users and Teams*, *Max Users Per Team*...
  This should enable the *Save* button for that page.
- Click *Save*.
- Refresh the page.
- Revert the change...

As the ``web`` user, reinstate the ``plugins`` directories::

  cd ~/repo/project/chat.yourbiz.co.uk/live/
  rsync -au plugins~/ plugins
  rm -rf plugins~
  rsync -au client/plugins~/ client/plugins
  rm -rf client/plugins~

As ``root``, restart the mattermost service::

  supervisorctl
  restart mattermost

Check MatterMost is displaying posts and images correctly...


.. _`mattermost-team-edition-server-archive`: https://docs.mattermost.com/administration/version-archive.html#mattermost-team-edition-server-archive
.. _`Upgrading Mattermost Server`: https://docs.mattermost.com/administration/upgrade.html#upgrading-to-the-latest-version
