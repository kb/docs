Add swap space to a server
**************************

- :doc:`sys-wordpress`

Introduction
============

This is a healthy server with 8G of RAM and 3GB of available RAM (``avail Mem``):

.. image:: ./misc/2022-03-17-swap-avail-mem.png

.. tip:: ``buff/cache`` is memory utilised by Linux to optimise performance.
         It is still *available RAM* and can be used if necessary.

This is an unhealthy server with 4GB of RAM.
The available RAM (``avail Mem``) is very low
and the ``buff/cache`` is also very low:

.. image:: ./misc/2022-03-17-swap-avail-mem-not-enough.png

This is the unhealthy server after adding swap (using the instructions below):

.. image:: ./misc/2022-03-17-swap-avail-mem-not-enough-added-swap.png

- You can see the 4GB of swap (``MiB Swap``).
- The server still has ``601.7 avail Mem``
- 3.5GB of swap is ``free`` and available if required.

Steps
=====

Adapted from https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-20-04

.. tip:: I asked *How do you decide how much swap to create?*,
         Malcolm says, *Same size as memory*...

.. tip:: For a WordPress server, Malcolm suggests *that you install a largeish*
         *swap, then if it uses too much memory it will just run slow rather*
         *than falling over... need to set the server params to make sure it*
         *doesn't use swap unless absolutely necessary*...

Examine existing swap config::

  sudo swapon --show

Check available space::

  df -h

Allocate file for swap and configure::

  sudo fallocate -l 4G /swapfile
  sudo chmod 600 /swapfile
  sudo mkswap /swapfile

Edit fstab and apply the setting::

  sudo cp /etc/fstab /etc/fstab.bak
  echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
  sudo swapon --all

View the swapiness and cache pressure settings::

  cat /proc/sys/vm/swappiness
  #  probably displays 60 - reduce this to 10 (see below)
  # so that swap is only used if absolutely necessary

  cat /proc/sys/vm/vfs_cache_pressure
  #  probably displays 100 - reduce this to 50 (see below)
  # to ease the pressure on RAM resources (unless RAM is plentiful).

Edit ``/etc/sysctl.conf`` to set new values for these
- add these 2 lines to the end of the file::

  vm.swappiness=10
  vm.vfs_cache_pressure=50

Apply these settings::

  sudo sysctl -p

View the settings using cat commands above or::

  sudo sysctl -a

Remove
======

::

  sudo swapoff -v /swapfile

Remove entry from ``/etc/fstab``::

  /swapfile none swap sw 0 0

Remove ``swapfile``::

  sudo rm /swapfile
