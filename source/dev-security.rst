Development Security
********************

HTML
====

`Link with target="_blank" and rel="noopener noreferrer" still vulnerable`_

.. code-block:: html

  <a href="mailto:{{ object.user.email }}" target="_blank" rel="noopener noreferrer">
    {{ object.user.email }}
  </a>


.. _`Link with target="_blank" and rel="noopener noreferrer" still vulnerable`: https://stackoverflow.com/questions/50709625/link-with-target-blank-and-rel-noopener-noreferrer-still-vulnerable
