Firewall
********

We have three firewall configurations:

- ``salt``
  Our new VPN based on Wireguard

- ``web``,
  To restrict access to web sites via IP (port 80 and 443).

- ``monitor``,
  To allow ElasticSearch, APM client to post to the APM Server
  (which is running on our Kibana server),
  To configure the firewall for monitoring, see :ref:`monitor_firewall_config`.

.. _firewall_wireguard:

Wireguard - Configuration
=========================

.. tip:: Start by adding the server to the :doc:`sys-vpn`...

The firewall details are stored in the site file (``server_meta``).
Add the following to the ``sites`` pillar e.g. ``sites/kb101.sls``::

  server_meta:
    configure_default_site: True
    sshd_interface_ips:
      - 10.10.2.15
    firewall:
      ports:
        - 80
        - 443
        - 51820
  
- ``configure_default_site`` - (PJK 13/11/2021 TODO) what does this do?
- ``sshd_interface_ips`` is the IP address of your server on the **VPN**
  (see :doc:`sys-vpn`)...
- ``firewall`` - list the ``ports`` which you want open.  This example has our
  standard configuration (80 and 443 for the web and 51820 for Wireguard).

ssh
---

To open the ``ssh`` port, add port 22 to the ``firewall`` **and** remove the
``sshd_interface_ips`` e.g::

  server_meta:
    configure_default_site: True
    # sshd_interface_ips:
    #  - 10.10.2.15
    firewall:
      ports:
        - 22
        - 80
        - 443
        - 51820

.. tip:: After applying the Salt state, restart the ssh service i.e.
         ``systemctl restart sshd.service``

Diagnostics
-----------

To check your public keys:

1. Generate your public key::

    sudo grep Private /etc/wireguard/wg1.conf | cut -d' ' -f3 | sudo wg pubkey

2. Compare the generated public key with the one on the VPN server.

Testing
-------

Use ``nmap`` to check for open ports::

  sudo nmap -O www.hatherleigh.info

.. note:: ``nmap`` only detects ports that are actively listening - you can use
          ``netstat -nlp`` on the server to get a fuller list of ports.

.. warning:: The ``nmap`` check should only be done on servers you have
             permission to scan.

.. tip:: For more information, see `How To Use Nmap to Scan for Open Ports`_

.. _firewall_config:

Web - Configuration
===================

.. note:: This example was written for restricting access to a :doc:`devpi`
          server by IP address but it will work just as well for restricting
          access to a website running on port 80 and 443.

Find the external IP address of your workstation::

  dig +short myip.opendns.com @resolver1.opendns.com

Add it to ``config/firewall/devpi.sls`` in your pillar e.g:

.. code-block:: yaml

  firewall:
    web:
      # web server
      - 13.16.24.14
      # yourbiz
      - 91.85.167.37

.. tip:: Please add a comment to show which server / workstation has the IP
         address.

Copy the pillar to your Salt master and run a ``state.apply`` to update the
firewall on your monitor server e.g:

.. code-block:: bash

  salt 'my-server' state.apply --state-verbose=False


.. _`How To Use Nmap to Scan for Open Ports`: https://www.digitalocean.com/community/tutorials/how-to-use-nmap-to-scan-for-open-ports
