Technology Standards
********************

- Click here for :doc:`dev-standards`
- Click here for :doc:`dev-standards-code`
- Click here for :doc:`dev-standards-doc`
- Click here for :doc:`dev-standards-html`
- Click here for :doc:`dev-standards-js`
- Click here for :doc:`dev-standards-sys`
- Click here for :doc:`dev-standards-tech`
- Click here for :doc:`dev-standards-ui`

ElasticSearch
=============

As a general principle, we should write code which uses ElasticSearch rather
than writing code to avoid using it.

We consider our ElasticSearch experience to be a sales opportunity (it has
already helped us get sales orders).

We still have lots to learn, but consider the time spent to be a good
investment.
