enquiry
*******

.. highlight:: python

https://gitlab.com/kb/enquiry

.. note:: The ``enquiry`` app will send emails to the list of users set-up in
          the notify list in admin.

Install
=======

Install :doc:`dev-captcha`

Add the :doc:`app-mail` app to your project.

Add the enquiry app to ``requirements/local.txt``::

  -e ../../app/enquiry

Add the enquiry app to ``requirements/production.txt``.

Add the enquiry app to ``settings/base.py``::

  LOCAL_APPS = (
      'project',
      # ...
      'enquiry',

Add the enquiry app to ``project/urls.py``::

  urlpatterns = [
      # ...
      url(regex=r"^enquiry/", view=include("enquiry.urls")),

Issues
======

Initialise
----------

If you get the following error::

  "GdprError, Consent 'enquiry-app-contact' does not exist"

Then initialise the ``enquiry`` app e.g::

  django-admin init-app-enquiry

Usage
=====

.. note:: The following instructions are for adding an enquiry form to a custom
          page (using our :doc:`app-block` app).

Create a custom page for the enquiry form (probably best to do this in the
``init_project`` management command for your project):
`project/management/commands/init_project.py`_

.. note:: This example is for use in a management command.
          (it uses page sections from the ``compose`` app)

Create a URL for your custom page::

  from web.views import EnquiryCreateView

  url(
      regex=r"^contact/$",
      view=EnquiryCreateView.as_view(),
      kwargs=dict(page=Page.CUSTOM, menu="contact"),
      name="web.contact",
  ),

In your view, inherit from ``EnquiryCreateMixin`` e.g: `web/views.py`_

.. note:: To add a notice to your form (which can include HTML), see
          :doc:`app-gdpr`

Add the enquiry form to your template e.g:
`compose/templates/compose/contact.html`_

In the base template for your project, add a link to your enquiry view e.g:
`web/templates/web/base.html`

.. code-block:: html

  {% block menu_extra %}
    {{ block.super }}
    <li class="pure-menu-item{% if 'contact' in path %} pure-menu-selected{% endif %}">
      <a href="{% url 'web.contact' %}" class="pure-menu-link">
        Contact Us
      </a>
    </li>
    {# use an exact match on the path if you want to add a design menu #}
    {% if path == '/contact/' or path == '/custom/contact/design/' %}
      {% include 'block/_design_menu.html' %}
    {% endif %}
  {% endblock menu_extra %}

.. tip:: If you don't add the design menu, you can edit the content of the
         enquiry page, using *Settings*, *Pages*, *Config*...

.. note:: The enquiry page will not be included in the
          :ref:`block_menu_cms_pages` or the
          :ref:`block_menu_navigation_menu`
          because it is a custom page.

Set-up
======

Log into the admin area of the site and set-up notify users.


.. _`compose/templates/compose/contact.html`: https://gitlab.com/kb/compose/blob/master/compose/templates/compose/contact.html
.. _`project/management/commands/init_project.py`: https://gitlab.com/kb/hatherleighcommunitymarket_org/blob/master/project/management/commands/init_project.py
.. _`web/templates/web/base.html`: https://gitlab.com/kb/hatherleighcommunitymarket_org/blob/master/web/templates/web/base.html
.. _`web/views.py`: https://gitlab.com/kb/hatherleighcommunitymarket_org/blob/master/web/views.py
