Keycloak
********

.. highlight:: bash

Download ``keycloak-8.0.1.tar.gz`` from https://www.keycloak.org/downloads.html

.. note:: The UI on the download table is very confusing.
          You need to download the *Server*, *Standalone server distribution*
          (which looks like the header row).

::

  sudo -i -u web
  cd ~/repo/temp/
  wget https://downloads.jboss.org/keycloak/8.0.1/keycloak-8.0.1.tar.gz
  cd ~/repo/project/login.kbsoftware.co.uk/live/
  tar --strip-components=1 --show-transformed-names -xvzf ~/repo/temp/keycloak-8.0.1.tar.gz
