WordPress - WooCommerce
***********************

- :doc:`app-woocommerce`
- :doc:`sys-wordpress`
- :doc:`sys-wordpress-issues`
- :doc:`sys-wordpress-plugin`
- :doc:`sys-wordpress-security`
- :doc:`sys-wordpress-update`

Plugins
=======

CedCommerce, ManoMano Integration For WooCommerce
-------------------------------------------------

- Plugin Home Page
  https://cedcommerce.com/woocommerce-extensions/manomano-integration-woocommerce
- Docs
  https://woocommerce.com/document/manomano-integration-for-woocommerce/
- Demo
  https://www.youtube.com/watch?v=ot5wYT_jhzQ
- ManoMano - Automatic Import page (Dynamic Sellers)
  :download:`./misc/wordpress/Automatic Import page _Dynamic Sellers_ EN version.pdf`

Request API credentials from ManoMano (by email to
support.technique@manomano.com).
Enter these credentials in two places:

1. *CedCommerce*, *Manomano*, *Add Account*:

.. image:: ./misc/wordpress/2022-07-06-woocommerce-mano-mano-add-account.png

2. *CedCommerce*, *Manomano*, *Configure*, *SETTINGS*, *ORDER CONFIGURATION*:

.. image:: ./misc/wordpress/2022-06-29-manomano-api-credentials.png

.. tip:: 06/07/2022, WhatsApp conversation with CED tech support,
         *Do we enter the API credentials into both?*:
         *Please use API credentials*,
         *As api credentials needed to fetch order from manomano*.
         *Yes as due to password get changed*.

veeqo
-----

`Setting up your WooCommerce store via FTP`_

- Only an administrator can disable a channel.

Theme
=====

We are using the *Flatsome* theme...

To edit the theme, browse to the WordPress *Dashboard*.

WooCommerce includes several Widgets_ ...

The filtering for *WooCommerce* products is configured in two places:

- *Appearance*, *Widgets*.

- *Flatsome*, *Theme Options*, *Widgets*, *Shop Sidebar*

.. warning:: If the *Shop Sidebar* isn't included in the list (e.g. you only
         see *Footer 1* and *Footer 2*), then browse to a product category
         page and it should appear!!

.. tip:: For details on adding *Sidebar Widgets*, see
         `WooCommerce Lesson #14. How to Use WooCommerce Sidebar Widgets`_

Products
========

.. tip:: Also see `Products - CSV`_ (below)...

Attributes
----------

`WooCommerce documentation, Product attributes`_

*Products* , *Attributes*, *Add new attribute*

After creating the attribute, *Configure terms*

.. note:: *Attributes* have fixed selection of terms, so are not useful for
          product specific data e.g. rack location in a warehouse.

To set the attribute, edit a product, select *Product data*, *Attributes*,
select your new *Custom product attribute* and select a term e.g.

.. image:: ./misc/wordpress/2022-06-22-woocommerce-product-attributes.png

Catalog
-------

To remove pagination, *Dashboard*, *Appearance*, *Customize*, *WooCommerce*,
*Product Catalog*, set *Rows per page* to ``0``:

.. image:: ./misc/wordpress/2021-11-04-theme-woocommerce-product-catalog-rows-per-page.png

For more information, see `Ticket 5897`_

Tags
----

Are created in a similar way to Attributes_, and are linked to a product.
Tags do not have a value.

Products - CSV
==============

.. tip:: Also see `Products`_ (above)...

.. warning:: From `WooCommerce, Product CSV Importer and Exporter`,
             If possible, avoid Microsoft Excel due to formatting and
             character encoding issues.

Import / export options are at the top of the *Products* page:

.. image:: ./misc/wordpress/2022-06-27-woocommerce-product-csv-import-export.png

Import Products
---------------

Columns required in CSV file:

- Type (e.g. ``Simple``)
- Categories (e.g. ``THE 80s``)
- SKU (e.g. ``ABC2024V``)
- Description (e.g. ``Green T Shirt``)
- Regular price (e.g. £87.99 – this is the RRP including VAT)
- Sale price (e.g. £79.99 – this is the PROMO including VAT)
- EAN (e.g. ``123450000000`` – this is the barcode)

Import process summary:

- Login as admin.
- Sidebar => Products.
- ‘Import’ button.
- Choose file (CSV).
- Either:
-   Tick the checkbox to update existing products and not create new products.
-   Untick the checkbox to create new products and not update existing products.
- ‘Continue’ button.
- As long as column headings match the list above, the mapping will be
  automatically set.
- ‘Run the importer’ button.

Import Cost Price Updates
-------------------------

.. warning:: From `WooCommerce, Product CSV Importer and Exporter`,
             If possible, avoid Microsoft Excel due to formatting and
             character encoding issues.

.. tip:: This process creates custom fields and is not restricted to cost
         prices. A custom field is created by adding a prefix of 'meta:'
         before your column heading, e.g. ``meta:cost_price``.

Columns required in CSV file:

- ``SKU`` (e.g. ``AB1234HIJK``)
- ``meta:cost_price`` (e.g. 1579.94)

Import process is as above but ensure that the checkbox to update existing
products is ticked.

Questions
=========

1. Would you update this?  Might the theme need updating as well?

  You have version 5.0.3 installed. Update to 6.3.1.
  View version 6.3.1 details.
  Compatibility with WordPress 5.9.2: 100% (according to its author)

.. image:: ./misc/wordpress/2022-04-04-woocommerce-update-major.jpg

Widgets
=======

Widgets included with WooCommerce
https://docs.woocommerce.com/document/woocommerce-widgets/

One of these is, *Filter Products by Attribute* which will *Display a list of
attributes to filter products in your store*.


.. _`Setting up your WooCommerce store via FTP`: https://help.veeqo.com/en/articles/4256002-setting-up-your-woocommerce-store-via-ftp
.. _`Ticket 5897`: https://www.kbsoftware.co.uk/crm/ticket/5897/
.. _`WooCommerce documentation, Product attributes`: https://woocommerce.com/document/managing-product-taxonomies/#section-6
.. _`WooCommerce Lesson #14. How to Use WooCommerce Sidebar Widgets`: https://www.youtube.com/watch?v=06SQHx2FVps
.. _`WooCommerce, Product CSV Importer and Exporter`: https://woocommerce.com/document/product-csv-importer-exporter/
