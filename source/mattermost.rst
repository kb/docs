MatterMost
**********

- :doc:`sys-mattermost`

Edit (and preview) a message
============================

.. raw:: html

    <video controls height="350" width="100%">
      <source src="_static/2020-01-07-mattermost-edit.mp4" type="video/mp4">
      Sorry, your browser doesn't support embedded videos.
    </video>

Click *Preview* to see a preview of your message.

.. tip:: If you don't see the *Preview* option, then see Settings_ below...

Formatting a message
====================

.. raw:: html

    <video controls height="350" width="100%">
      <source src="_static/2020-01-07-mattermost-help.mp4" type="video/mp4">
      Sorry, your browser doesn't support embedded videos.
    </video>

Click *Help* to browse to the online manual.

Link to a message
=================

.. raw:: html

    <video controls height="350" width="100%">
      <source src="_static/2020-01-07-mattermost-permalink.mp4" type="video/mp4">
      Sorry, your browser doesn't support embedded videos.
    </video>

Settings
========

To switch on the *Preview* option:

- Main Menu
- Account Settings
- Advanced
- Preview pre-release features
- Show markdown preview option in message input box

.. image:: ./misc/2020-01-07-mattermost-account-settings.png
