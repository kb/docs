Sales Order Ticket
******************

- https://gitlab.com/kb/sales_order_ticket
- :doc:`app-sales-order`

.. _sales-order-ticket-reports:

Reports
=======

To initialise the reports::

  django-admin init-app-sales-order-ticket

``SalesOrderSummaryReport`` (``sales_order_ticket/reports.py``)
  *Summary of work done on a sales order* as a PDF created using WeasyPrint.
  Run the report by browsing to a sales order and clicking *Summary*.

``SalesOrderTicketHoursReport`` (``sales_order_ticket/reports.py``)
  *Sales Order Ticket - Hours*

``TicketChargeableTimeNoSalesOrder`` (``sales_order_ticket/reports.py``)
  *Tickets with Chargeable Time but No Sales Order*
