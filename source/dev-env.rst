Development Environment
***********************

.. highlight:: bash

.. note:: For Ubuntu 20.04 LTS

Packages
========

Install the following::

  sudo locale-gen en_GB.utf8
  sudo update-locale en_GB.utf8

Development tools (install ``vim`` or an editor of your choosing)::

  sudo apt install build-essential curl mercurial git vim wget

.. note:: Do we need to install ``wget``?

Configure git with your email and name::

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

.. tip:: To remove the ``a/`` and ``b/`` prefixes from the ``diff`` output:
         ``git config --global diff.noprefix true``
         (for more information, see `diff.noprefix`_)

If you wish to use the scripts from `dev-scripts`_ install ``gawk``::

  sudo apt install gawk

.. tip:: If installing on versions earlier than 14.04 use ``apt-get``.

python development::

  sudo apt install python3-dev
  # pillow
  # for ubuntu 14.04
  sudo apt install libtiff4-dev

  # for ubuntu 15.10 and later including 16.04
  sudo apt install libtiff5-dev
  sudo apt install libjpeg-dev zlib1g-dev libfreetype6-dev
  sudo apt install liblcms2-dev libwebp-dev tcl8.5-dev tk8.5-dev python-tk

  # for readline
  sudo apt install libncurses5-dev

  # if you have issues with setuptools e.g. Requirement.parse('setuptools>=0.8'))
  wget https://bootstrap.pypa.io/ez_setup.py -O - | sudo python

Postgres::

  sudo apt install postgresql libpq-dev

MySQL (if you plan to develop using MySQL)::

  sudo apt install mysql-server mysql-client default-libmysqlclient-dev

ODBC (if you plan to develop using odbc)::

  sudo apt install unixodbc-dev

.. tip:: The error message will be
         ``ImportError: libodbc.so.2: cannot open shared object file: No such file or directory``

Redis::

  sudo apt install redis-server

python
======

::

  apt install python3.9-venv

To create a ``virtualenv``::

  python3 -m venv venv-for-app
  source venv-for-app/bin/activate.fish

bash and zsh
============

A simple script for directory based environments (if a directory contains a
``.env`` file it will automatically be executed when you ``cd`` into it)::

  git clone git://github.com/kennethreitz/autoenv.git ~/.autoenv

::

  # bash
  echo 'source ~/.autoenv/activate.sh' >> ~/.bashrc

  # for zsh use the 'autoenv' plugin
  # vim ~/.zshrc
  # plugins=(autoenv autojump git history-substring-search mercurial python tmux virtualenv)

Database
========

Postgresql

.. important:: For ubuntu 14.10, replace ``9.3`` with ``9.4``.
               For 16.04, replace ``9.3`` with ``9.5``.
               For 18.04, replace ``10.x`` with the correct version.

Replace ``/etc/postgresql/9.x/main/pg_hba.conf``
with :download:`misc/9.x/pg_hba.conf`:

Replace ``/etc/postgresql/10.x/main/pg_hba.conf``
with :download:`misc/10.x/pg_hba.conf`::

  sudo chown postgres:postgres /etc/postgresql/9.x/main/pg_hba.conf
  sudo chmod 640 /etc/postgresql/9.x/main/pg_hba.conf

Replace ``/etc/postgresql/9.3/main/postgresql.conf`` with
:download:`misc/9.3/postgresql.conf` for ``9.3``
and
:download:`misc/9.4/postgresql.conf` for ``9.4``::
and
:download:`misc/9.5/postgresql.conf` for ``9.5``::
and
:download:`misc/9.6/postgresql.conf` for ``9.6``::
and
:download:`misc/10.x/postgresql.conf` for ``10.x``::

  sudo chown postgres:postgres /etc/postgresql/9.x/main/postgresql.conf
  sudo chmod 644 /etc/postgresql/9.x/main/postgresql.conf

Re-start Postgres::

  sudo service postgresql restart

Create a role for your user name
(replace ``patrick`` with your linux user name)::

  psql -X -U postgres -c "CREATE ROLE patrick WITH NOSUPERUSER CREATEDB NOCREATEROLE LOGIN;"

  # if you get 'ERROR:  must be owner of extension plpgsql', then this will solve it
  psql -X -U postgres -c "ALTER ROLE patrick SUPERUSER;"

python
======

pip
---

.. note:: Refer to your company :doc:`checklist` and replace
          ``devpi.yourbiz.co.uk`` with the name of your ``devpi`` server.
          Do the same for the username and password.

.. note:: (to myself) Check out the ``--set-cfg`` parameter in
          http://doc.devpi.net/latest/userman/devpi_commands.html
          It might do the following automatically.

.. warning:: According to the latest ``pip`` documentation
          (https://pip.pypa.io/en/latest/user_guide.html#configuration), there
          is a config file at ``~/.config/pip/pip.conf``.  (``~/.pip/pip.conf``
          is now a **legacy** per-user configuration file).

Add the following to the ``~/.config/pip/pip.conf`` file::

  [install]
  index-url = https://devpi.yourbiz.co.uk/kb/dev/+simple/
  trusted-host = devpi.yourbiz.co.uk
  [global]
  require-virtualenv = True

.. tip:: ``trusted-host`` is copied from my current file.

.. tip:: ``require-virtualenv`` requires a virtual environment when using
         ``pip``.

Add the following to the ``~/.pydistutils.cfg`` file::

  [easy_install]
  index_url = https://devpi.yourbiz.co.uk/kb/dev/+simple/

.. _pypirc:

.pypirc
-------

.. tip:: You won't be able to :doc:`fabric-release` unless you do this!

Add the following to the ``~/.pypirc`` file::

  [distutils]
  index-servers =
      dev

  [dev]
  repository: https://devpi.yourbiz.co.uk/kb/dev/
  username: bz
  password: 789

.. tip:: To check the configuration of your package index, you can browse to
         https://pypi.yourbiz.co.uk/kb/dev/+simple/kb-base/

Tools
=====

These are tools that I like (they are not required to build these projects):

- https://www.pkimber.net/howto/linux/apps/alacritty.html
- https://www.pkimber.net/howto/linux/apps/ack.html
- https://www.pkimber.net/howto/linux/fish.html
- https://www.pkimber.net/howto/linux/apps/tmux.html

And legacy notes on some old tools:

- https://www.pkimber.net/howto/linux/apps/atom.html

Microsoft
---------

To install the Microsoft core fonts (useful for sharing documents)::

  sudo apt install msttcorefonts

.. tip:: You'll need to accept the Microsoft EULA.

Source Code
===========

Check out your source code into this folder structure::

  ├── dev
  │   ├── app
  │   │   ├── base
  │   │   ├── block
  │   │   ├── booking
  │   │   ├── cms
  │   │   ├── crm
  │   │   ├── enquiry
  │   │   ├── invoice
  │   │   ├── login
  │   │   ├── mail
  │   │   ├── pay
  │   │   ├── search
  │   │   └── stock
  │   ├── module
  │   │   ├── deploy
  │   │   │   ├── pillar
  │   │   │   ├── post-deploy
  │   │   │   └── ssl-cert
  │   │   ├── docs
  │   │   ├── fabric
  │   │   └── salt
  │   └── project
  │       ├── hatherleigh_info
  │       └── pkimber_net

app
---

The source code for the reusable apps go into the ``app`` folder.  The github
URL and documentation for our open source apps are here:

- :doc:`app-base`
- :doc:`app-block`
- :doc:`app-booking`
- :doc:`app-checkout`
- :doc:`app-compose`
- :doc:`app-crm`
- :doc:`app-enquiry`
- :doc:`app-finance`
- :doc:`app-invoice`
- :doc:`app-job`
- :doc:`app-login`
- :doc:`app-mail`
- :doc:`app-search`

deploy
------

``pillar``, :doc:`salt-pillar`

``ssl-cert``, :doc:`ssl`

docs
----

(This documentation),
https://github.com/pkimber/docs

fabric
------

:doc:`fabric-env`

project
-------

Put the source code for your customer into the ``project`` folder e.g:
https://github.com/pkimber/pkimber_net

Follow the instructions in the ``README.rst`` file in the app or project
folder.


.. _`dev-scripts`: https://github.com/mdinsmore/dev-scripts
.. _`diff.noprefix`: https://twitter.com/brandur/status/1182066723337293828?s=09
