Alfresco Community Edition
**************************

To re-start the service::

  service alfresco restart

Log files::

  /opt/alfresco-community/share.log
  /opt/alfresco-community/alfresco.log
  /opt/alfresco-community/tomcat/logs/catalina.out

Configuration (see ``salt`` states for more information)::

  /opt/alfresco-community/tomcat/shared/classes/alfresco-global.properties

For backup we are using
https://github.com/toniblyx/alfresco-backup-and-recovery-tool

Install
=======

Create the server...

Set the correct ``hostname``: :ref:`rename_server`

.. tip:: Don't forget to change the minion ID: :ref:`salt_change_minion_id`.

Update the server (``apt update``, ``apt upgrade``).

Install Salt Minion (I followed the *old instructions* in
:doc:`old/salt-cloud-install`).

Connect the minion to the master: :ref:`salt_provision_minion`.

Run an initial ``state.apply`` on the server::

  salt 'java-b' state.apply --state-verbose=False

Download the Community Edition to the server (an upload from your laptop will
probably take a very long time)::

  wget http://eu.dl.alfresco.com.s3.amazonaws.com/release/community/201702-build-00016/alfresco-community-installer-201702-linux-x64.bin

Install the Community Edition::

  sudo -i
  chmod +x /home/patrick/repo/temp/alfresco-community-installer-201702-linux-x64.bin
  /home/patrick/repo/temp/alfresco-community-installer-201702-linux-x64.bin

.. note:: It took me a long time to find all the packages required for the
          LibreOffice install.  There are probably more than we need now:
          https://github.com/pkimber/salt/commit/fc07bb55a5a1ddc6c7be6fd01209fc7615fe5fdc

Update your ``/etc/hosts`` file (or update the DNS).

Configuration
=============

Maximum file size is controlled by Nginx.  I don't think Alfresco has a limit:
https://github.com/pkimber/salt/commit/53e45955a18203614cc077e512bab6196a11c4fc

LibreOffice
===========

LibreOffice is not converting files.  I tried adding logging (see below).  This
did not help.

I just found ``alfresco5_office_preview_fix.md``:
https://gist.github.com/yohang88/2d5fce9c522ffc6eb1e7689bb09fe7b4

This command says LibreOffice is not running::

  /opt/alfresco-community/libreoffice/scripts/libreoffice_ctl.sh status

I updated the ``LIBREOFFICE_SCRIPT`` path in
``/opt/alfresco-community/alfresco.sh``::

  # LIBREOFFICE_SCRIPT=$INSTALLDIR/libreoffice/scripts/ctl.sh
  LIBREOFFICE_SCRIPT=$INSTALLDIR/libreoffice/scripts/libreoffice_ctl.sh

Run this command to check if all dependencies are installed::

  /opt/alfresco-community/libreoffice/program/soffice.bin --nofirststartwizard --nologo --headless --accept=socket,host=localhost,port=8100

Start LibreOffice using this command::

  /opt/alfresco-community/libreoffice/scripts/libreoffice_ctl.sh start

Logging
=======

Copy the sample file::

  cp /opt/alfresco-community/tomcat/shared/classes/alfresco/extension/custom-log4j.properties.sample /opt/alfresco-community/tomcat/shared/classes/alfresco/extension/audit-log4j.properties

The contents of::

  /opt/alfresco-community/tomcat/shared/classes/alfresco/extension/audit-log4j.properties

Are::

  log4j.logger.org.alfresco.enterprise.repo.content=DEBUG
  log4j.logger.org.alfresco.repo.content.transform.TransformerDebug=DEBUG
  log4j.logger.org.artofsolving.jodconverter=DEBUG

  log4j.logger.org.alfresco.repo.audit.inbound=DEBUG

  #log4j.logger.org.alfresco.util.exec.RuntimeExecBootstrapBean=debug
  #log4j.logger.org.alfresco.util.exec.RuntimeExec=debug

.. note:: Adding ``log4j.logger.org.alfresco.repo.audit.inbound=DEBUG`` gave me
          lots of output, but nothing useful (for LibreOffice diagnostics).

Restore
=======

Run the following command to restore the data::

  /home/web/opt/alfresco-bart.sh restore

.. note:: When the script asks you to *Type a destination path with enough
          space available*, then choose a temporary folder e.g.
          ``/home/web/repo/temp/2017-04-20/``

To view the database::

  psql -U postgres -h localhost

To restore::

  service alfresco stop
  /opt/alfresco-community/postgresql/scripts/ctl.sh start

  # note: to stop postgres
  # /opt/alfresco-community/postgresql/scripts/ctl.sh stop

  DROP DATABASE alfresco;
  CREATE DATABASE alfresco OWNER alfresco;

::

  gunzip -c ~/repo/temp/postgresql/alfresco.dump.gz | psql -h localhost -d alfresco -U postgres

  mv /home/web/repo/temp/cs/contentstore /opt/alfresco-community/alf_data/contentstore
  mv /home/web/repo/temp/solr/config/index /opt/alfresco-community/alf_data/solr4/index
  service alfresco start
