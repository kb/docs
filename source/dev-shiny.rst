Shiny
*****

.. highlight:: bash

- Click here for my notes on the `R programming language`_

Logging
=======

Shiny logs are in this folder::

  /var/log/shiny-server/

Salt
====

To add Shiny to your server, add ``config.r`` to your ``pillar`` e.g:

.. code-block:: yaml

  base:
    'kb-g':
      - config.r
      - sites.kb-g

Script
======

To add a script to your Shiny server:

Copy the script to the ``shiny`` folder in your project folder e.g::

  shiny/app.R

Convert the script to Linux format::

  fromdos shiny/app.R

Add the source file to GIT and commit with a suitable comment.

Copy the file from ``shiny/app.R`` to the server::

  /srv/shiny-server/app.R

Issues
======

We had an issue where the page view was drawing, but had no charts or data.
There were no errors in the log.

To solve the issue, we added ``server = TRUE`` to ``updateSelectizeInput`` e.g::

  updateSelectizeInput(
    session,
    inputId = "platform",
    choices = as.vector(df$platform),
    selected = df$platform,
    server = TRUE
  )


.. _`R programming language`: https://www.pkimber.net/howto/r/index.html
