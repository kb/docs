SiteMap
*******

.. highlight:: python

Block App::

  # project/urls.py
  from django.contrib.sitemaps import GenericSitemap

  from block.models import Page

  info_dict = {
    'queryset': Page.objects.pages(),
    'date_field': 'modified',
  }

  sitemaps = {
      'block': GenericSitemap(info_dict, priority=0.5, changefreq='monthly'),
  }

  urlpatterns = patterns(
      '',
      url(regex=r'^sitemap\.xml$',
          view='django.contrib.sitemaps.views.sitemap',
          kwargs={'sitemaps': sitemaps},
          ),

  # settings/base.py
  DJANGO_APPS = (
      # ...
      'django.contrib.sitemaps',
  )

To add static URLs to the sitemap::

  # project/sitemaps.py
  # -*- encoding: utf-8 -*-
  from django.contrib import sitemaps
  from django.core.urlresolvers import reverse

  class StaticViewSitemap(sitemaps.Sitemap):

      priority = 0.5
      changefreq = 'weekly'

      def items(self):
          return [
              'latest.info',
          ]

      def location(self, item):
          return reverse(item)

  # project/urls.py
  from .sitemaps import StaticViewSitemap

  sitemaps = {
      'static': StaticViewSitemap,
  }

To look at the site map using ``httpie``::

  http GET http://localhost:8000/sitemap.xml
