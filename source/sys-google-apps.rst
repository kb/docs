Google Apps
***********

2FA - 2-Step Verification
=========================

`Avoid account lockouts when 2-Step Verification is enforced by your organization`_

Admin
=====

To make a user an admin:

1. Users, select the user.
2. Scroll down, "Show more"
3. Admin roles and privileges, Manage Roles
4. Super Admin

Data Migration
==============

So annoying... trying to move email from one Google Workspace account to
another.

The *Data Migration* option only seems to allow a
*Source* of ``imap.gmail.com``.

I get the following error message::

  Error (Retrying)
  Authentication failure. (18017)

This article, `G Suite Migration Data error 18017`_, seems to give a clue
i.e. turn this setting on, https://myaccount.google.com/lesssecureapps
but it still doesn't work!

Email Footer
============

To add a global footer to all emails go to:
Admin, Apps, G Suite, Settings for Gmail, Advanced settings, Compliance, Append Footer

To add an image get the image ID by right clicking the image and selecting
"get shareable link" you will get a url similar to that below

https://drive.google.com/open?id=0B8KETRyB-KBgX096SmNHWmI3a1k

Remove the https://drive.google.com/open?id=
and replace it with https://docs.google.com/uc?id=
Thus:
https://docs.google.com/uc?id=0B8KETRyB-KBgX096SmNHWmI3a1k

Use the Add Image Icon in the Footer dialogue to paste the url into the footer

Group Alias
===========

.. tip:: `Fix common issues with group settings`_ has solved various issues.
         In particular, *People outside my organization can’t email my group*
         fixed everthing.

Start by adding *Groups for Business* to your *Google Apps*.  This will allow
you to add the *Public* Access Level to a group.

.. tip:: If you want emails to go to a group of people e.g. to send ``info@``
         emails to everyone in the company, then create a group alias.

To create an ``info`` or ``admin`` email address, go to *Google Groups*, click
the *Create group* button:

  Home, More Controls, Groups

.. image:: ./misc/google-apps-group-create.png

We must give the ``public`` permission to send email addresses to the group:

  Groups, select your group, Access Settings, Posting permissions, Post,
  Public, Save

.. image:: ./misc/google-apps-group-post-permission.png

.. note:: Creating a group does not add another user to the account, so will
          not cost any more.

.. warning:: Malcolm followed these notes,
             `Create a group & choose group settings`_ and they worked.
             The notes above also worked for me on the 19/04/2023.


.. _`Avoid account lockouts when 2-Step Verification is enforced by your organization`: `https://apps.google.com/supportwidget/articlehome?hl=en&article_url=https%3A%2F%2Fsupport.google.com%2Fa%2Fanswer%2F9176805%3Fhl%3Den&assistant_id=generic-unu&product_context=9176805&product_name=UnuFlow&trigger_context=a`
.. _`Create a group & choose group settings`: https://support.google.com/groups/answer/2464926?hl=en
.. _`Fix common issues with group settings`: https://support.google.com/a/answer/9325317?hl=en&ref_topic=1685690&fl=1&sjid=5804852540769033978-NA
.. _`G Suite Migration Data error 18017`: https://support.google.com/a/thread/24460425/g-suite-migration-data-error-18017
