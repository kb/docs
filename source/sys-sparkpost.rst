SparkPost
*********

API Key (for API and SMTP access)
=================================

When creating the API key, I selected:

- *Transmissions: Read/Write*

.. image:: ./misc/sparkpost/2024-06-10-sparkpost-api-key.png

When creating the SMTP key, I selected:

- *Send via SMTP*
- *Transmissions: Read/Write*

.. image:: ./misc/sparkpost/2024-06-10-sparkpost-api-key-smtp.png

.. warning:: *Transmissions: Read-only* is not enough.

For :doc:`app-monitor`, I selected:

- *Metrics: Read-only*.

From `Getting 403 Error when sending an email with Sparkpost`_:

  One likely cause for that 403 error is your API key does not include the
  permission for "Transmission".

.. _mail-sparkpost-domains:

Domains
=======

Before SparkPost will send emails, you need to configure a domain...

.. warning:: The SparkPost domain name **must** match the domain on the
             ``from`` address
             e.g. If you configure ``mail.hatherleigh.info``, then your
             ``from`` address must be your.name@mail.hatherleigh.info.
             (``your.name@hatherleigh.info`` will fail).

Log into the SparkPost *Configuration*, *Domains* section:

- Configure *Sending Domains*

  The page says, *"Using a subdomain is recommended e.g. sub.domain.com"*, but
  a subdomain will not work if you want the recipient to reply (see above for
  *"The SparkPost domain name must match the domain on the from address"*).

.. tip:: For our most recent customer we setup a *Sending* domain on the
         organisational domain (e.g. ``hatherleigh.info``) with a
         ``TXT``/``DKIM`` record, but no ``CNAME`` record.
         We configured a separate *Bounce Domain* as
         ``bounce.hatherleigh.info``.

- *Create a separate bounce subdomain* using ``bounce`` as the sub-domain.
  The SparkPost site will walk you through the steps
  e.g. create a ``CNAME`` for ``bounce.hatherleigh.info`` and set the value to
  ``sparkpostmail.com``.

.. _mail-sparkpost-smtp:

SMTP
====

::

  server        smtp.sparkpostmail.com
  user          SMTP_Injection
  password      <sparkpost-api-key>
  port          587
  security      STARTTLS

Subaccounts
===========

Subaccounts look like a nice idea, but they require the Premier plan.

I received this email from SparkPost support in October 2022:

  You are currently on the 50K starter plan, which does not include
  subaccounts.
  Subaccounts are only available on the 100K Premier and up plans.

  Please see the following page for a comparison between the Starter and
  Premier plans:
  https://support.sparkpost.com/docs/faq/difference-between-starter-and-premier

  You will need to upgrade to a Premier plan if you wish to use subaccounts.


.. _`Getting 403 Error when sending an email with Sparkpost`: https://stackoverflow.com/questions/60128409/getting-403-error-when-sending-an-email-with-laravel-and-sparkpost
