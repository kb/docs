GitLab
******

Deploy Token
============

We use deploy tokens in ``requirements/ci.txt`` to allow the CI system to
access private apps e.g::

  -e git+https://work-deploy-token:abCd1fGhIjKlm45-T3qr@gitlab.com/kb/work.git#egg=kb-work

To create a deploy token follow the `GitLab Deploy tokens` documentation.

.. tip:: Copy the *Name* to *"Username (optional)"*.
         The *Username* is the first part of the URL
         e.g. ``work-deploy-token`` in the example above.

For *Scopes*, I selected ``read_repository``.

.. _gitlab_branch:

GitLab Branches
===============

.. note:: Before using *Delete Merged Branches*, check the list of merged
          branches to see if there are any you think you might want.

1. To *delete untracked remotes* on your local machine, see :ref:`git_branch`
2. We have a script to delete :ref:`scripts_untracked_branches`

New Repository
==============

To create a new repository:

- Log into https://gitlab.com/ using our company login details.
- Click on *New Project*.
- In the *Project path* section select the ``kb`` group.  Select the correct
  *Visibility Level* and click *Create Project*.
- You should now be able to clone the repository, add a ``README.rst`` file and
  then push.
- Go to the project *Settings*, *Repository*, and scroll down to
  *Protected branches* and unprotect the master branch so developers can push
  to it.
- Go to the project *Settings*, *CI/CD*, and scroll down to *Runners* and
  in the *Specific Runners* column enable the company ci runner in the
  ``Runners activated for this project`` section. In the *Shared Runners*
  column click on the ``Disable shared runners`` button.

To move a repository from GitHub to GitLab: `Migrate from GitHub`_

Continuous Integration
----------------------

.. tip:: To set-up a runner see :ref:`continuous_integration`.

.. tip:: To set-up GitLab Multi-Runner and Docker, see the following section...

GitLab Multi-Runner and Docker
==============================

From `Install using official GitLab repositories`_

Install Docker:
https://www.pkimber.net/howto/linux/apps/docker/getting-started.html

Create a Docker image, by following the instructions in the ``README.rst`` file
in https://gitlab.com/kb/gitlab-pytest

Install ``gitlab-runner``::

  curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
  sudo apt-get install gitlab-runner

.. tip:: Register the runner using ``sudo`` because you cannot enable the
         service unless you are ``root``.

From `Registering Runners`_

Get a registration token, by browsing to your group and selecting *Settings*,
*CI / CD*, *Runners* e.g:

.. image:: ./misc/gitlab-project-settings-ci-cd.png

Set-up::

  sudo -i
  gitlab-runner register

Sample output::

  Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/ci):
  https://gitlab.com/
  Please enter the gitlab-ci token for this runner:
  eaeae123ababa
  Please enter the gitlab-ci description for this runner:
  [master-b]: ci.kbsoftware.co.uk
  Please enter the gitlab-ci tags for this runner (comma separated):
  INFO[0204] eabd2c42 Registering runner... succeeded
  Please enter the executor: shell, parallels, docker, docker-ssh, ssh:
  docker
  Please enter the Docker image (eg. ruby:2.1):
  pkimber/gitlab-pytest
  Runner registered successfully.

Run in debug mode::

  gitlab-runner --debug run

From `Install GitLab Runner manually`_, to install the service::

  gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

To start the service::

  gitlab-runner start

To view logging::

  tail -f /var/log/syslog

To set-up Continuous Integration for the project, see
:ref:`continuous_integration`.

.. tip:: To cleanup Docker containers, see :ref:`docker-system-prune`.

.. _gitlab_npm_package:

npm package
===========

- `Publishing private npm packages using GitHub Packages`_
- `GitLab, NPM packages in the Package Registry`_

.. _npm-patch-install-publish:

Patch, install and publish
--------------------------

.. warning:: Make sure to stop ``ember serve``!

.. warning:: Make sure to update ``package.json`` with the latest versions
             and remove any ``link`` packages.

.. tip:: We only publish *apps* to our package repository
         (see gitlab_npm_package_ above).
         We do **not** publish projects.
         To release one of our *projects*, follow the
         *Release and Deploy* notes in the ``README.rst`` file.

::

  cd ~/dev/app/app-or-project/front/

  # check there are no link dependencies
  cat package.json | grep file
  # check there is no uncommitted code
  git status

  pnpm env use --global 18
  rm -rf dist/ node_modules/ /tmp/embroider/
  pnpm version patch

  # if releasing a project (for 'ember-cli-new-version')
  python ../kb.py --version-config

  pnpm install
  git add config/environment.js package.json package-lock.json
  git commit -m "prep for release - ember"

  pnpm publish

.. tip:: To use the ``@kb:registry`` (see gitlab_npm_package_ above), I think
         the package name needs to be prefixed with ``@kb``
         e.g. ``"name": "@kb/ember-work"``

Personal Access Token
---------------------

.. tip:: We are using a single GitLab repository (``package``) for all of our
         *Packages*.

Find the *Project ID* in GitLab:

- Find the project.
- Click *Settings*, *General*

e.g.

.. image:: ./misc/gitlab/2024-05-16-package-project-id.png

Create a personal access token:

- *Edit Profile* , *Access Tokens*, *Personal Access Tokens*,
  https://gitlab.com/-/profile/personal_access_tokens
- I set most of the *Scopes*::

   api, read_api, read_user, create_runner, k8s_proxy, read_repository, write_repository, read_registry, write_registry

.. tip:: I am sure some of the *Scopes* can be removed...

Then set the registry and auth token for ``pnpm`` (replacing the *Project ID*
and access token) e.g::

  # set the auth token
  pnpm config set '//gitlab.com/api/v4/projects/29981347/packages/npm/:_authToken' "my-gitlab-auth-token"
  # set the registry (update the project ID)
  pnpm config set @kb:registry https://gitlab.com/api/v4/projects/29981347/packages/npm/

.. tip:: I guess the ``@kb`` part of the command is from the URL
         e.g. https://gitlab.com/kb/msgraph

.. tip:: I think we can only have one package repository.
         To check, ``npm config list`` or ``cat ~/.npmrc``.


.. _`GitLab Deploy tokens`: https://docs.gitlab.com/ee/user/project/deploy_tokens/
.. _`GitLab, NPM packages in the Package Registry`: https://docs.gitlab.com/ee/user/packages/npm_registry/
.. _`Install GitLab Runner manually`: https://docs.gitlab.com/runner/install/linux-manually.html
.. _`Install using official GitLab repositories`: https://docs.gitlab.com/runner/install/linux-repository.html
.. _`Migrate from GitHub`: https://www.pkimber.net/howto/git/gitlab.html#migrate-from-github
.. _`Publishing private npm packages using GitHub Packages`: https://medium.com/javascript-in-plain-english/publishing-private-npm-packages-using-github-packages-415993cd2da8
.. _`Registering Runners`: https://docs.gitlab.com/runner/register/index.html
