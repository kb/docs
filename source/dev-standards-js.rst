Ember / JavaScript Code Standards
*********************************

.. highlight:: javascript

- Click here for :doc:`dev-standards`
- Click here for :doc:`dev-standards-code`
- Click here for :doc:`dev-standards-doc`
- Click here for :doc:`dev-standards-html`
- Click here for :doc:`dev-standards-security`
- Click here for :doc:`dev-standards-sys`
- Click here for :doc:`dev-standards-tech`
- Click here for :doc:`dev-standards-ui`

Classes
=======

Order of attributes in a class:

1. services
2. model
3. # alphabetical

Handlebars
==========

Tim is using the following *Atom* plugin:
https://github.com/kandhavivekraj/beautify-handlebars

.. tip:: You have to manually remove ``/`` from ``<hr/>``.

Prettier
========

Using ``front/prettier.config.js``:

.. code-block:: javascript

  module.exports = {
    trailingComma: "none",
    tabWidth: 2,
    semi: false,
    singleQuote: false,
    bracketSpacing: true,
    arrowParens: "avoid",
    printWidth: 80
  }

.. code-block:: bash

  npm run prettier
  npm run prettier:hbs

``var``, ``let`` or ``const``
=============================

.. tip:: Prefer ``const`` wherever possible only using ``let`` when a value
         must change.  Avoid ``var`` unless absolutely necessary.

Chris Thoburn, @not_runspired

  I've been on team prefer-const since the beginning and it’s been a joy
  watching that go from niche to widely accepted/linted norm.
  I suspect (and this is imo wild) that it was typescript that finally got
  everyone to realize how much value there is in the mental model of it.

Tomasz Ducin, @tomasz_ducin

  Shall we use `var`, `let` or `const` in #javascript/#typescript? My take has
  been the same for years (since ES2015 released). And it turns out I have the
  same coding standards as #angular 🙃

.. image:: ./misc/js/2012-12-10-var-let-or-const.jpg

https://twitter.com/not_runspired/status/1728933270522634298
