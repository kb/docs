Microsoft Azure
***************

Links
=====

- :doc:`app-msgraph`
- :doc:`sys-microsoft`
- :doc:`sys-office-365`
- :ref:`sys-postgres-microsoft-azure`

email
=====

To create an email address for a user, create the user in the Azure portal
(https://portal.azure.com/) then browse to :ref:`sys-office-365-email` in the
Office 365 admin.

OpenID Connect
==============

Is used by the :doc:`app-login` app.  See :ref:`app_login_openid_connect` for
more information.

Create Directory
----------------

Create your directory at https://portal.azure.com/

.. image:: ./misc/azure/azure-create-directory.png

Register application
--------------------

Register your application:

.. image:: ./misc/azure/azure-app-registrations.png

Add a *Redirect URI* for the Django (and Ember apps)...

Start by finding the Django URL::

  # www.hatherleigh.info.sh shell
  import urllib.parse
  from django.conf import settings
  from django.urls import reverse
  urllib.parse.urljoin(settings.HOST_NAME, reverse('oidc_authentication_callback'))

Then add the Django URL e.g::

  https://www.hatherleigh.info/back/oidc/callback/

.. tip:: Don't forget to append a ``/``.

.. image:: ./misc/azure/register-application.png

.. tip:: If you forget to add a *Redirect URI* or set it incorrectly, the error
         message from Azure *Sign in* will be, *The reply URL specified in the
         request does not match the reply URLs configured for the application*.

Ember
-----

Add a *Redirect URI* e.g::

  https://www.hatherleigh.info/login

Or for development::

  http://localhost:4200/login

.. warning:: Do *NOT* append a backslash for the Ember URLs.

Update ``front/config/environment.js`` with the ::

  if (environment === "development") {
    ENV["oidcAuth"]["clientId"] = "0cc7e...",
    ENV["oidcAuth"]["host"] = "https://login.microsoftonline.com/bcb19.../oauth2/v2.0"
  }

So for a Django and Ember app on a development machine, the *Redirect URIs*
will look like this:

.. image:: ./misc/azure/2024-08-26-redirect-urls.png

Update
------

You can update the redirect URIs by selecting *Redirect URIs* in the application
overview:

.. image:: ./misc/azure/application-callback.png

.. warning:: You will need to append a ``/`` to the *Redirect URI*
             (e.g. http://localhost:8000/oidc/callback/).

.. tip:: For more information, see *examples of callback urls* in
         `mozilla-django-oidc`_,

.. _azure_api_permissions:

API permissions
---------------

Check the API permissions and set the *Status* to *Granted...* for
``User.Read``:

.. image:: ./misc/azure/2024-08-26-api-permission-user-read.png

Settings
--------

The :doc:`app-login` app requires a ``.private`` file containing the following::

  set -x OIDC_OP_AUTHORIZATION_ENDPOINT "https://login.microsoftonline.com/fcee251/oauth2/v2.0/authorize"
  set -x OIDC_OP_TOKEN_ENDPOINT "https://login.microsoftonline.com/fcee251/oauth2/v2.0/token"

  set -x OIDC_RP_CLIENT_ID "36ad9"
  set -x OIDC_RP_CLIENT_SECRET "aead6"

  set -x OIDC_OP_JWKS_ENDPOINT "https://login.microsoftonline.com/common/discovery/v2.0/keys"
  set -x OIDC_RP_SIGN_ALGO "RS256"
  set -x OIDC_USE_NONCE False

  # used by 'login/management/commands/demo_data_login_oidc.py'
  set -x KB_TEST_EMAIL_FOR_OIDC "patrick@kbsoftware.co.uk"
  set -x KB_TEST_EMAIL_USERNAME "pkimber"

The *Application ID* (``OIDC_RP_CLIENT_ID``) is on the *Overview* page:

.. image:: ./misc/azure/2024-08-26-application-id.png

The ``OIDC_OP_AUTHORIZATION_ENDPOINT`` and ``OIDC_OP_TOKEN_ENDPOINT`` endpoints
are found by clicking on *Endpoints*

.. image:: ./misc/azure/application-endpoints.png

Create a secret in *Certificates and secrets* (``OIDC_RP_CLIENT_SECRET``):

.. image:: ./misc/azure/application-secrets.png

.. tip:: Copy the secret (not the id of the secret).

To get the ``OIDC_OP_JWKS_ENDPOINT`` and ``OIDC_RP_SIGN_ALGO``
browse to:
https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration

.. image:: ./misc/azure/openid-configuration.png

.. tip:: I found this information in the
         `Fetch the OpenID Connect metadata document`_...

ssh
===

If your ``ssh`` connections are timing out, then add the following to your
``~/.ssh/config`` file::

  Host www.hatherleigh.info
      ServerAliveInterval 120
      ServerAliveCountMax 4

.. tip:: Replace ``Host`` as appropriate.


.. _`Configure an OpenID/OAuth application from the Azure AD app gallery`: https://docs.microsoft.com/en-us/azure/active-directory/saas-apps/openidoauth-tutorial
.. _`Fetch the OpenID Connect metadata document`: https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-protocols-oidc#fetch-the-openid-connect-metadata-document
.. _`mozilla-django-oidc`: https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html#acquire-a-client-id-and-client-secret
