Pagination
**********

.. tip:: To handle search parameters (using ``_paginate_with_parameters.html``)
         see :ref:`pagination-with-parameters`

Django Pagination
=================

::

  # views.py
  class PaymentListView(BaseMixin, ListView):
      paginate_by = 20

::

  <!-- html -->
  <div class="pure-g">
    <div class="pure-u-1">
      {% include 'base/_paginate.html' %}

.. note:: The ``_paginate.html`` template will leave some blank space if there
          are not enough rows to switch it on.  To remove the space, enclose
          the section in ``{% if is_paginated %}``.

.. tip:: To handle search parameters (using ``_paginate_with_parameters.html``),
         see :ref:`pagination-with-parameters`
