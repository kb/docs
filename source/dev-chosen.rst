Chosen - Select Box
*******************

.. highlight:: html

Usage
=====

Add the following to your form template (probably below the
``{% endblock content %}`` block)::

  {% block script_extra %}
    {{ block.super }}
    {% include 'base/_chosen.html' %}
  {% endblock script_extra %}

Any element with a class of ``chosen-select`` will become a chosen drop-down
(dropdown) box.  You can add this in the ``__init__`` method of a form e.g::

  class MailChimpSettingsForm(forms.ModelForm):
      def __init__(self, *args, **kwargs):
          super().__init__(*args, **kwargs)
          f = self.fields["default_consent"]
          f.widget.attrs.update({"class": "chosen-select"})

Validation / Errors
===================

If your Chosen select box is set to ``required`` and the user has not selected
an item, the browser will attempt to pop-up a message.  This message does not
appear (for some reason on Firefox), so you might like to make the field
``required=False`` and add a ``clean`` method to the form e.g::

  def clean_product_category(self):
      data = self.cleaned_data.get("product_category")
      if not data:
          raise forms.ValidationError(
              "You must select a product category",
              code="product_category__required",
          )
      return data
