Security - Sys-Admin
********************

- Click here for :doc:`dev-standards-security`

Stay Safe Online
================

Very interesting chart...

.. image:: ./misc/stay-safe-online.png

From `Arvind Narayanan`_ `tweet on the 18th August 2019`_ ...

Anti-Virus
==========

Looks like Windows Defender is finally an option:

.. image:: ./misc/pc-gamer-anti-virus-2019-08.png

For details, see
`AV-TEST Product Review and Certification Report - May-Jun/2019`_


.. _`Arvind Narayanan`: https://twitter.com/random_walker
.. _`AV-TEST Product Review and Certification Report - May-Jun/2019`: https://www.av-test.org/en/antivirus/home-windows/windows-10/june-2019/microsoft-windows-defender-4.18-192315/
.. _`tweet on the 18th August 2019`: https://twitter.com/random_walker/status/1163121422711046144
