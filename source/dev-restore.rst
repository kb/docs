Restore
*******

.. highlight:: bash

List::

  fab domain:www.hatherleigh.info list_current:backup
  fab domain:www.hatherleigh.info list_current:files

Restore::

  fab restore www.hatherleigh.info backup
  fab restore www.hatherleigh.info files

.. tip:: If you get a ``permission denied`` error on restore, then
         ``ALTER ROLE patrick SUPERUSER``.
         For more information, see :doc:`dev-env`
