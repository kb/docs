Mandrill (do not use)
*********************

.. warning:: Do **NOT** use this app.
             The only thing it does is handle ``RejectedEmail`` and this code
             was moved to the ``mail`` app.

For other mail related apps, see:

- :doc:`app-mailchimp` 
- :doc:`app-mandrill` 
