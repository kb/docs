Documentation
*************

.. highlight:: python

- Click here for :doc:`dev-standards`
- Click here for :doc:`dev-standards-code`
- Click here for :doc:`dev-standards-js`
- Click here for :doc:`dev-standards-security`
- Click here for :doc:`dev-standards-sys`
- Click here for :doc:`dev-standards-tech`
- Click here for :doc:`dev-standards-ui`

Syntax
======

Click here for `reStructuredText syntax`_

From `PEP 257 - Docstring Conventions - Multi-line Docstrings`_::

  def complex(real=0.0, imag=0.0):
      """Form a complex number.

      Keyword arguments:
      real -- the real part (default 0.0)
      imag -- the imaginary part (default 0.0)
      """
      if imag == 0.0 and real == 0.0:
          return complex_zero

I think I agree with most of the examples in this document:
:download:`misc/pep8_cheat.pdf`.

I also like some of the ideas in `Elements of Python Style`_.  We can watch and
see if it becomes accepted.

Initialise
==========

Folders
-------

We have two standard folders for Sphinx documentation:

1. ``docs-kb``, for internal (or *Technical Documentation*).
2. ``docs-user``, for documentation written for the client

.. note:: This documentation should **never** include *secret* information.

Requirements
------------

Add the following to your ``requirements/local.txt`` file::

  sphinx
  sphinx_rtd_theme

Quickstart
----------

Run ``sphinx-quickstart``::

  sphinx-quickstart

Extensions
----------

You can accept most of the defaults.  These are the exceptions::

  Separate source and build directories (y/n) [n]: y
  autodoc: automatically insert docstrings from modules (y/n) [n]: y
  todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: y
  coverage: checks for documentation coverage (y/n) [n]: y
  viewcode: include links to the source code of documented Python objects (y/n) [n]: y

We use the following ``extensions`` in ``source/conf.py``::

  extensions = [
      "sphinx.ext.autodoc",
      "sphinx.ext.doctest",
      "sphinx.ext.todo",
      "sphinx.ext.coverage",
      "sphinx.ext.viewcode",
  ]

Here are links to the documentation for the ``extensions``:

- `sphinx.ext.autodoc`_ Include documentation from docstrings
- `sphinx.ext.doctest`_ Test snippets in the documentation
- `sphinx.ext.todo`_ Support for todo items
- `sphinx.ext.coverage`_ Collect doc coverage stats
- `sphinx.ext.viewcode`_ Add links to highlighted source code

Theme
-----

We use the ``sphinx_rtd_theme`` in ``source/conf.py``::

  html_theme = "sphinx_rtd_theme"

Django
------

If the project uses Django::

  # -- Extension configuration -------------------------------------------------

  import django
  import os
  import sys

  sys.path.insert(0, os.path.abspath(".."))
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "example_contact.dev_test")
  django.setup()

.. note:: Update ``example_contact.dev_test`` to the settings file for your
          project.

API Documentation
-----------------

From `One-Off Creation of RST Files`_

  There is a script that you can run to create a directive file per Python
  module. **You should only run this command once** to set up the ``*.rst``
  files.

In the docs directory, run this command to create ``rst`` files that document
your python modules.

.. warning:: The ``-f`` option tells it to overwrite existing files.

::

  sphinx-apidoc -f -o source/ ../

You should see ``rst`` files created in the ``source`` folder

Extensions
==========

Module (``autodoc``)
--------------------

From `sphinx.ext.autodoc`_

To include python modules in your documentation - i.e. automembered with
properties, method names, etc - use the following syntax in your
reStructuredText files, placing it where you want the module documented::

  .. automodule:: app.modulefile
     :members:

This supports docstrings which are comments wrapped in three speechmarks that
should be written after the member you want to autodoc::

  class Models():
    """Stores and manage the models signed up to this agency."""

    model_height = model.IntField()
    """The height of the model in metres."""

    def update(self, model):
      """Update this model's info."""

    def expel_model(self, model_pk):
      """
      Expel this model from the agency.

      :param model_pk: The id of the model being expelled.
      :returns: Success message.
      :raises keyError: raises an exception if the model cannot be expelled.
      """


.. _`Elements of Python Style`: https://github.com/amontalenti/elements-of-python-style
.. _`One-Off Creation of RST Files`: https://docs-python2readthedocs.readthedocs.io/en/develop/userguide/code-doc.html#one-off-creation-of-rst-files
.. _`PEP 257 - Docstring Conventions - Multi-line Docstrings`: https://www.python.org/dev/peps/pep-0257/#multi-line-docstrings
.. _`reStructuredText syntax`: https://www.pkimber.net/howto/markup/restructuredtext/syntax.html
.. _`sphinx.ext.autodoc`: http://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
.. _`sphinx.ext.coverage`: http://www.sphinx-doc.org/en/master/usage/extensions/coverage.html
.. _`sphinx.ext.doctest`: http://www.sphinx-doc.org/en/master/usage/extensions/doctest.html
.. _`sphinx.ext.todo`: http://www.sphinx-doc.org/en/master/usage/extensions/todo.html
.. _`sphinx.ext.viewcode`: http://www.sphinx-doc.org/en/master/usage/extensions/viewcode.html
