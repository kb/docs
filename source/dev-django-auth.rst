Django Auth
***********

We are successfully using Active Directory in a project.

Development
===========

To login when using the development server (i.e. no IIS server to do the
authentication, set the ``REMOTE_USER`` environment variable e.g. (for the
fish shell):

.. code-block:: bash

  set -x REMOTE_USER "patrick.kimber"

For more information see
`StackOverflow, using REMOTE_USER - How to test in dev server`_

Backend
=======

.. warning:: This ended up being far more complicated than is shown below!

We created our own authentication_backend::

  # -*- encoding: utf-8 -*-
  from django.contrib.auth.backends import RemoteUserBackend


  class ExistingRemoteUserBackend(RemoteUserBackend):

      create_unknown_user = False

      def clean_username(self, username):
          """Remove the domain name from the 'username'."""
          pos = username.find("\\")
          if pos == -1:
              result = username
          else:
              result = username[pos + 1 :]
          return result

Our tests are copies of the Django tests at test_remote_user_.


.. _`StackOverflow, using REMOTE_USER - How to test in dev server`: https://stackoverflow.com/questions/15554062/django-authentication-using-remote-user-how-to-test-in-dev-server
.. _test_remote_user: https://github.com/django/django/blob/master/tests/auth_tests/test_remote_user.py
