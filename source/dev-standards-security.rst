Security Standards - Development
********************************

- Click here for :doc:`dev-standards`
- Click here for :doc:`dev-standards-code`
- Click here for :doc:`dev-standards-doc`
- Click here for :doc:`dev-standards-html`
- Click here for :doc:`dev-standards-js`
- Click here for :doc:`dev-standards-sys`
- Click here for :doc:`dev-standards-tech`
- Click here for :doc:`dev-standards-ui`
- Click here for :doc:`sys-security`

We should create an *AppSec Program*.  Start by working through these resources:

- This is an excellent video, by https://twitter.com/edgeroute, setting out a
  strategy for improving security of web apps:
  `Building an AppSec Program with a Budget of $0 (Beyond the OWASP Top 10)`_
- The PowerPoint slides are here:
  :download:`misc/DevOps_Buiding-an-AppSec-Program_Chris-Romeo_AppSecEU2018.pdf`

Django
======

Remove the ``admin`` app.  To replace::

  from django.contrib.admin.views.decorators import staff_member_required

Do the following::

  from django.contrib.auth.decorators import user_passes_test

  @user_passes_test(lambda u: u.is_staff)
  def report_consent_user_download(request, pk):


.. _`Building an AppSec Program with a Budget of $0 (Beyond the OWASP Top 10)`: https://www.youtube.com/watch?v=JxZr5-biOY0
