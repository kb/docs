WordPress - Development
***********************

REST API
========

From, `How to use the WordPress REST API with Python`_

Create an *Application Password*:

- *Users*, *Profile*, scroll down to *Application Passwords*.
- Enter a name / description, then click the *Add New Application Password* to
  generate the password.

Update the ``WOOCOMMERCE_SITE_1_WP_APP_PASS`` in the :doc:`app-woocommerce`.


.. _`How to use the WordPress REST API with Python`: https://robingeuens.com/blog/python-wordpress-api
