Simple Site (CMS)
*****************

- https://gitlab.com/kb/simple-site
- :doc:`dev-wagtail`

A simple site for community organisations using Wagtail and Tailwind.

Configuration
=============

Wagtail :ref:`wagtail-configuration`

Initialise the ``enquiry`` app (so the contact form works)::

  django-admin init-app-enquiry

Google Search Console and Verification
======================================

If your site uses Google Analytics or needs to be verified:

.. image:: ./misc/simple-site-settings.png

Old Documentation
=================

After installing the Simple CMS:

1. Create a ``Tracking ID`` for your site in
   https://analytics.google.com/

2. Copy the ``Tracking ID`` to the ``google_site_tag`` in the block settings
   (using this URL ``/admin/block/blocksettings/``)

3. Add your site to https://search.google.com/search-console
   If you set-up Google Analytics in step 1, then you can validate site
   ownership using Google Analytics.

Content
-------

For the slideshow, create an image with these dimensions:
3681 pixels x 664 pixels e.g.

.. image:: ./misc/simple-cms-slideshow.jpg
