Django Applications
*******************

.. highlight:: python

django-mptt
===========

::

  THIRD_PARTY_APPS = (
      'mptt',

django-recaptcha
================

See :doc:`dev-captcha`

