S3
**

Our S3 Bucket is ``kbsoftware``

Our tree structure is::

  kbsoftware/contacts/<client site name e.g. hatherleigh_info>/<client folders>

e.g::

  kbsoftware/contacts/hatherleighcommunitycentre_couk/calendar/

Add a new Client to S3
======================

Create a new folder in S3 e.g::

  kbsoftware/contacts/hatherleigh_info

Create (or find) an IAM user, *Services*, *IAM*, *Users*, *Summary*,
*Security credentials*.

.. tip:: The user name should match the name of the web site for the client...

Un check the *Generate an access key for each user* box and click *Create*
(10/01/2020, I cannot see this on the control panel).

Open the newly created user and click the *Groups* tab.
Add the user to the ``contacts`` group.

Under *Manage Password* give them a defined password and record it along with
their user name
(10/01/2020, I cannot see this on the control panel).

Click *Create Access Key* and download the ``credentials.csv`` file to the
relevant folder in the ``Private`` folder.

.. tip:: We have used Cyberduck CLI to upload files to S3.
         For detailed information, see our ``Private`` client notes
         (search for ``calendar.rst``).

Apply a Bucket Policy
=====================

These policies appear to apply to all users.

Click Add or Edit Bucket Policy from::

  https://console.aws.amazon.com/s3/home?region=eu-west-1

To allow Public Read Only access to all folders::

  {
      "Version": "2012-10-17",
      "Statement": [
          {
              "Sid": "AllowPublicRead",
              "Effect": "Allow",
              "Principal": {
                  "AWS": "*"
              },
              "Action": "s3:GetObject",
              "Resource": "arn:aws:s3:::kbsoftware/*"
          }
      ]
  }

Apply a User, Group or Role Policy
==================================

Click Create Policy in::

  https://console.aws.amazon.com/iam/home?region=eu-west-1#policies

Select *Create Your Own Policy* and enter a Policy Name and the code

To allow access to User folders by only specific User::

  {
      "Version": "2012-10-17",
      "Statement": [
          {
              "Sid": "AllowGroupToSeeBucketListInTheConsole",
              "Action": [
                  "s3:ListAllMyBuckets",
                  "s3:GetBucketLocation"
              ],
              "Effect": "Allow",
              "Resource": [
                  "arn:aws:s3:::*"
              ]
          },
          {
              "Sid": "AllowRootAndHomeListingOfCompanyBucket",
              "Action": [
                  "s3:ListBucket"
              ],
              "Effect": "Allow",
              "Resource": [
                  "arn:aws:s3:::kbsoftware"
              ],
              "Condition": {
                  "StringEquals": {
                      "s3:prefix": [
                          "",
                          "contacts/"
                      ],
                      "s3:delimiter": [
                          "/"
                      ]
                  }
              }
          },
          {
              "Sid": "AllowListingOfUserFolder",
              "Action": [
                  "s3:ListBucket"
              ],
              "Effect": "Allow",
              "Resource": [
                  "arn:aws:s3:::kbsoftware"
              ],
              "Condition": {
                  "StringLike": {
                      "s3:prefix": [
                          "contacts/${aws:username}/*"
                      ]
                  }
              }
          },
          {
              "Sid": "AllowAllS3ActionsInUserFolder",
              "Action": [
                  "s3:*"
              ],
              "Effect": "Allow",
              "Resource": [
                  "arn:aws:s3:::kbsoftware/contacts/${aws:username}/*"
              ]
          }
      ]
  }
