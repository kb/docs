Blog
****

Migrate
=======

.. tip:: To migrate from ``block.Image`` to ``gallery.Image``...

Edit ``blog/migrations/0002_auto_20190204_1624.py`` and change
``migrations.AddField`` to::

  migrations.AlterField
