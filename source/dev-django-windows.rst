Django on Windows
*****************

.. highlight:: python

- https://github.com/lincolnloop/django-webserver
- `Arguments to waitress.serve`_
- `Using WhiteNoise with Django`_

Add the following to ``requirements/base.txt``::

  django-webserver[waitress]
  whitenoise

.. tip:: See :doc:`dev-requirements` for the current version...

In ``settings/base.py`` for the project::

  MIDDLEWARE = [
      "django.middleware.security.SecurityMiddleware",
      # Always place WhiteNoiseMiddleware above other middleware
      # http://whitenoise.evans.io/en/stable/django.html#enable-whitenoise
      "whitenoise.middleware.WhiteNoiseMiddleware",

.. note:: Always place ``WhiteNoiseMiddleware`` below ``SecurityMiddleware`` and
          above other middleware (for details, see `Enable WhiteNoise`_).

::

  THIRD_PARTY_APPS = (
      "django_webserver",

In ``settings/dev_patrick.py`` (from `Using WhiteNoise in development`_)::

  # Add 'whitenoise.runserver_nostatic' to the top of your 'INSTALLED_APPS' list
  # http://whitenoise.evans.io/en/stable/django.html#using-whitenoise-in-development
  INSTALLED_APPS = (
      "whitenoise.runserver_nostatic",
      "django_extensions",
      "debug_toolbar",
  ) + INSTALLED_APPS

We are not using the ``static`` app, so we need to run ``collectstatic``::

  django-admin.py collectstatic

Run your project::

  django-admin.py waitress --port=8000


.. _`Arguments to waitress.serve`: https://docs.pylonsproject.org/projects/waitress/en/latest/arguments.html
.. _`Enable WhiteNoise`: http://whitenoise.evans.io/en/stable/django.html#enable-whitenoise
.. _`Using WhiteNoise in development`: http://whitenoise.evans.io/en/stable/django.html#using-whitenoise-in-development
.. _`Using WhiteNoise with Django`: http://whitenoise.evans.io/en/stable/django.html
