Sys-Admin and Configuration Management
**************************************

- Click here for :doc:`dev-standards`
- Click here for :doc:`dev-standards-code`
- Click here for :doc:`dev-standards-doc`
- Click here for :doc:`dev-standards-html`
- Click here for :doc:`dev-standards-js`
- Click here for :doc:`dev-standards-security`
- Click here for :doc:`dev-standards-tech`
- Click here for :doc:`dev-standards-ui`

We use SaltStack for Configuration Management.  This article explains the
benefits of using configuration management tools:
`What are configuration management tools?`_

Here is a copy of the list of important features:

Enforcement:

  Configuration enforcement may be the single most important feature of a
  configuration management tool. By running regularly and ensuring the machine
  is configured to the desired state, configuration management tools prevent
  configuration drift. Configuration drift can happen in a variety of ways:
  Package updates, live debugging, "helpful" coworkers, etc. Whatever the
  cause, being able to say with confidence, "This is how this machine is
  configured," is a great way to shorten incident resolution time and reduce
  surprises.

Enables cooperation:

  Configuration management tools make it easier for team members to cooperate.
  With one change, configuration can be updated across the entire
  infrastructure. Hand-editing configuration on machines can only lead to
  unexpected differences. By putting all of the configuration in one place, you
  can keep from stepping on someone else's (including future you) toes.

Version control friendly:

  Of course, the best way to enable cooperation is to have everything in a
  version control system. All of the tools listed below use some form of text
  for configuration. This means you can take advantage of the benefits of your
  favorite version control system. The benefits of version control are beyond
  the scope of this article, but let me assure you from experience that you
  really, really want your configuration in a version control system.

Enables change control processes:

  Because configuration management tools are textual and VCS-friendly, you can
  treat infrastructure changes like code. Changes can be submitted as diffs or
  merge requests and be subject to code review before approval. Having
  explicitly enumerated changes with timestamps can make reconstructing
  incidents much easier. With atomic changes, you can release them at a rate
  you're comfortable with instead of throwing a bucket of changes at your
  infrastructure all at once.

Abstraction:

  Few sysadmins maintain completely homogeneous environments. Even if you're an
  all-Linux shop, you probably have multiple distros that you support, or at
  least multiple versions of a distro. With configuration management tools,
  many of the operating-system-specific implementations of a configuration are
  abstracted away for you. The same configuration file can be used to manage,
  for example, the installation of Apache HTTPD on both Red Hat and Ubuntu
  systems.


.. _`What are configuration management tools?`: https://opensource.com/business/16/9/what-are-configuration-management-tools
