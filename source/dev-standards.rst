Standards
*********

- Click here for :doc:`dev-standards-code`
- Click here for :doc:`dev-standards-doc`
- Click here for :doc:`dev-standards-html`
- Click here for :doc:`dev-standards-js`
- Click here for :doc:`dev-standards-security`
- Click here for :doc:`dev-standards-sys`
- Click here for :doc:`dev-standards-tech`
- Click here for :doc:`dev-standards-ui`

.. highlight:: bash

.. important:: We want to continually improve the quality and reliability of
               our software.

               We love coding... and want to have fun!

               If you have any thoughts or ideas for improvement, then please
               create a pull request here: https://gitlab.com/kb/docs

Our current method of working is as follows:

Daily Meeting
=============

Before starting work each day, please send a *Daily Meeting* email.  The email
should have three headings:

1. *Yesterday*.  What did you do yesterday (or what was the last piece of work
   you did for us)?
2. *Today*.  What do you expect to do today?
3. *Concerns*.  Are there any impediments in your way?  We will review your
   email and see if we can do anything to help.

emails
======

If a customer sends an email to one or more members of your team:

1. Do not discuss the email with the other person.
2. Just get on - do the work and reply to the customer.

.. note:: Do not copy a colleagues emails into a ticket.
          If it needs to be copied, your colleague will copy it.

Create a ticket
===============

Create a ticket for every support issue and every development task.

1. Tickets for development tasks should be created for each piece of work which
   is estimated to take between 4 and 7 hours (maximum 2 days).
2. Add tickets to the backlog or development (or support) board as required.
3. Update tickets on the board so they reflect their current state.

.. important:: Only add the ticket number to the subject line of a **new** email
               e.g. ``Fix invoice description #1234``
               Do **not** amend the subject line of an email from a customer.

.. tip:: For internal emails, include a link to the ticket in the body of
         the email e.g. ``https://www.kbsoftware.co.uk/crm/ticket/1234/``

.. tip:: For external emails, you can include the ticket number in the **body**
         of the email e.g. #1234.

Record Time
===========

Time record everything against a ticket (fixed price, hourly rate or free of
charge).

We will pay your agreed hourly rate or fixed price amount based on your
time-sheet.

.. important:: Time records should be created on the day the work is done.

Pull Request
============

Please make changes to the code by creating a branch and then a merge request.

Create a branch as follows::

  git checkout -b 488-time-management

.. note:: In this example, 488 is the ticket number and ``time-management`` is
          a short description of the feature.

Add the URL of the merge request to the description of the ticket.

Ask another member of the team to review the work before merging the code back
into the ``master`` branch.  Check the following:

1. Merge request
2. Unit testing
3. Avoid new requirements (if possible).
4. Time recorded
5. ``white`` and ``black``

.. note:: We aim to create short-lived branches and merge them quickly back into
          the ``master`` branch.

Commit early, commit often
==========================

Please code in small units of work and commit after each change.  For more
information, see :doc:`dev-workflow`.

After each section of work is complete, it should be committed to the code
repository.

At a minimum, the code will be committed at the end of each morning, afternoon
or evening irrespective of whether it is complete or not.

Unit Testing
============

We practice test-driven development using http://pytest.org/.  We aim for a
minimum level of coverage.

See :doc:`dev-standards-code` for the percentage.

Preventing regressions
----------------------

Tests must be written when a bug is found and used to prove it has been fixed.

Private
=======

Code snippets for a fix or update to be installed in the ``Private`` folder for
the client in a folder or ``rst`` file e.g. for ticket 706, *Shop*, make a file
``706-stop.rst``.

.. note:: As of 20/10/2016, we don't have a simple way to sync changes to our
          ``Private`` folders.

Standards
=========

- Please use the modules listed in :doc:`dev-requirements`.  If you need to use
  other modules, then please discuss with a member of the team first.
