Django Field Types
******************

``BooleanField``
================

If you need to accept null values then use ``NullBooleanField`` instead.

.. note:: ``NullBooleanField`` is deprecated in version 3.1

``CharField``
=============

To create a ``CharField`` which can be ``blank``, specify **only** ``blank``.

If your ``CharField`` includes ``choices``, then you can get the
human-readable name using ``get_FOO_display()``::

    COLOUR_CHOICES = [
        (GREEN, "Green"),
        (RED, "Red"),
    ]
    colour = models.CharField(max_length=30, blank=True, choices=COLOUR_CHOICES)

    # In the template
    {{ object.get_colour_display }}

``DateTimeField``
=================

To create a ``DateTimeField`` which can be ``blank``, specify ``blank``
**AND** ``null``.

``DecimalField``
================

For now (because I can't find it in the docs), I am creating a ``DecimalField``
which can be ``blank`` with ``blank`` **AND** ``null`` (the same as an
``IntegerField``).

::

  rate = models.DecimalField(max_digits=8, decimal_places=2)

``EmailField``
==============

The ``EmailField`` is a ``CharField`` so for ``blank``, we will specify
**only** ``blank``.

``FileField`` and ``ImageField``
================================

To create a ``FileField`` or ``ImageField`` which can be ``blank``:

  I am not sure what to do here, but we will try copying a ``CharField``,
  so specify **only** ``blank``.

Read
----

To read from a ``FileField``::

  data = schedule.output.readlines()

File System
-----------

From `Loading Django FileField and ImageFields from the file system`_::

  from django.db import models

  class Company(models.Model):
      name = models.CharField(max_length=100)
      logo = models.ImageField()


  import requests
  from django.core.files import File
  from pathlib import Path

  from .models import Company

  r = requests.get("http://media.revsys.com/img/revsys-logo.png")

  with open("/tmp/revsys-logo.png", "wb") as f:
      f.write(r.content)

  reopen = open("/tmp/revsys-logo.png", "rb")
  django_file = File(reopen)

  revsys = Company()
  revsys.name = "Revolution Systems"
  revsys.logo.save("revsys-logo.png", django_file, save=True)

.. _`Loading Django FileField and ImageFields from the file system`: http://www.revsys.com/blog/2014/dec/03/loading-django-files-from-code/

``ForeignKey``
==============

To create an optional ``ForeignKey``, specify ``blank`` **AND** ``null``
e.g: ``mentor = models.ForeignKey(MentorModel, blank=True, null=True)``

``IntegerField``
================

To create an ``IntegerField`` which can be ``blank``, specify
``blank`` **AND** ``null``.

``JSONField``
=============

::

  from django.core.serializers.json import DjangoJSONEncoder
  parameters = JSONField(blank=True, null=True, encoder=DjangoJSONEncoder)

Date and Time
-------------

To ``dump`` json data using the ``DjangoJSONEncoder``::

  import json
  from django.core.serializers.json import DjangoJSONEncoder
  print(json.dumps(result, indent=4, cls=DjangoJSONEncoder))

If you add a date / time to the ``JSONField`` e.g::

    my_model.parameters = {
        "contact_pk": contact.pk,
        "from_date": from_date,
        "to_date": to_date,
    }

You need to parse them e.g::

  from dateutil.parser import parse
  from_date = parse(parameters.get("from_date"))

``ManyToManyField``
===================

  app_group = models.ManyToManyField(AppGroup, blank=True)

.. note:: I tried setting ``null=True``, but I got the following warning from
          migrations:: ``(fields.W340) null has no effect on ManyToManyField``.

``TextField``
=============

To create a ``TextField`` which can be ``blank``, specify **only** ``blank``.

``URLField``
=============

The ``URLField`` is a ``CharField`` so for ``blank``, we will specify
**only** ``blank``.

``UUIDField``
=============

I am *adding* a ``uuid`` field
(I couldn't find a way to convert an existing ``id`` field)::

  uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

To allow ``null``::

  uuid = models.UUIDField(null=True)

For more details, see the `Django UUIDField documentation`_ and
`Will Django UUIDFIeld behave correctly?`_


.. .. warning:: This method of replacing an ``id`` field loses the data in the
.. ``id`` field, so existing foreign keys will **break**.
.. Note: The following didn't work for me, so I just *added* a ``uuid`` field::
.. uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
.. .. warning:: This method of replacing an ``id`` field loses the data in the
.. ``id`` field, so existing foreign keys will **break**.
.. .. tip:: We use this 2 step method because the Django migrations system cannot
.. automatically convert from an integer to a UUID.
.. To replace the ``id`` field with a *universally unique identifier*...
.. 1. Create a ``UUIDField`` with a default value::
.. import uuid
.. uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
.. 2. Create a migration.
.. 3. Rename the ``uuid`` field to ``id``::
.. id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
.. 4. Create a final migration.


.. _`Django UUIDField documentation`: https://docs.djangoproject.com/en/2.2/ref/models/fields/#uuidfield
.. _`Will Django UUIDFIeld behave correctly?`: https://stackoverflow.com/questions/62093556/will-django-uuidfield-unique-true-blank-true-and-null-true-behave-correctly
