Wagtail
*******

.. highlight:: python

- :doc:`project-simple-site`

.. tip:: To log into the admin interface for our Wagtail sites,
         append ``/cms/`` e.g. https://www.hatherleigh.info/cms/

.. _wagtail-configuration:

Configuration
=============

1. Start by creating a home page.
2. Browse to *Settings*, *Sites* and delete the default site.
3. Create a new site and set the *Root page* to the home page you created in
   step 1.

Issues
======

Child Pages (e.g. *Contact Us*)
-------------------------------

If you create a child page and it doesn't appear in the menu:

- Edit the page and click on the *Promote* tab.
- Tick *Show in menus* - *For site menus*.

DisallowedHost
--------------

::

  django.core.exceptions.DisallowedHost: Invalid HTTP_HOST header:
  'https'. You may need to add 'https' to ALLOWED_HOSTS.

To solve this issue, I removed ``https`` from the *Hostname* in the *Site*
i.e. ``hatherleigh.info`` rather than ``https://hatherleigh.info``.

Sites - Home Page
-----------------

.. tip:: When running the development server, the home page may not display.
         To fix this, *Settings*, *Sites* and tick *Is default site*.

.. image:: ./misc/wagtail/2024-03-25-is-default-site.png

For more information,
`Django Wagtail page not found when i don't use "127.0.0.1:8000"`_:

  Check the site records under Settings -> Sites in the Wagtail admin. When a
  request comes in, Wagtail will only serve a page if the hostname matches one
  named in a site record, or the 'is default site' box is checked.

404
---

.. tip:: If the page fails to display (probably a 404 Page Not Found error),
         then try publishing the page again.

Blocks
======

``RichTextBlock`` and ``prose``
-------------------------------

.. code-block:: python

  description = blocks.RichTextBlock()

To use ``prose``:

- Add the ``prose`` keyword to the HTML element.
- Use ``self.description`` on it's own i.e. do *not* use
  ``self.description|linebreaksbr``.

::

  <div class="max-w-4xl mx-auto mt-4 prose text-xl leading-relaxed text-center text-purple-800 sm:mt-5">
    {{ self.description }}
  </div>

.. warning:: If you add ``prose`` to ``<p>`` tag, then the paragraphs will be
             appended using new ``<p>`` tags.  These ``<p>`` tags won't
             include ``prose``.
             To solve this issue, make sure to add ``prose`` to a ``div``!

Documents
=========

We use ``SENDFILE`` for documents...

``settings/production.py``::

  # https://docs.wagtail.io/en/stable/reference/settings.html#documents
  WAGTAILDOCS_SERVE_METHOD = "direct"

.. note:: Really not sure what to do with ``WAGTAILDOCS_SERVE_METHOD``.
          I tried to use ``serve_view`` with ``django-sendfile`` and couldn't
          get it to work.

Domain
======

This is the base URL used by the Wagtail admin site::

  # settings/local.py
  WAGTAILADMIN_BASE_URL = "http://localhost:8000/"
  # settings/production.py
  WAGTAILADMIN_BASE_URL = DOMAIN

Home
====

For the Wagtail home page URL, see Menu_

Django
------

If your Django project is looking for ``project.home``, then try this::

  from django.views.generic import RedirectView

  url(
      r"^project/home/redirect/$",
      view=RedirectView.as_view(url="/", permanent=False),
      name="project.home",
  ),

Editor Guide
============

A guide to using the Editor is here https://guide.wagtail.org/en-latest/

.. tip:: You can also access the guide through the CMS navigation bar.
         Go to Help > Editor Guide

Footer
======

See Menu_

Icons
=====

Click `Available icons`_ to view the list...

Menu
====

To get the home page and site menu, see
`Set up site menu for linking to the homepage and other pages`_

For the footer, see `Create a footer for all pages`_

Page
====

The *Promote* tab includes the *Meta description*:

.. image:: ./misc/wagtail/2024-06-08-promote.png

To display the *Meta description* in your template::

  <meta name="description" content="{{ self.search_description }}"/>


.. _`Available icons`: https://docs.wagtail.org/en/latest/advanced_topics/icons.html#available-icons
.. _`Create a footer for all pages`: https://docs.wagtail.org/en/stable/tutorial/create_footer_for_all_pages.html
.. _`Django Wagtail page not found when i don't use "127.0.0.1:8000"`: https://stackoverflow.com/questions/44358695/django-wagtail-page-not-found-when-i-dont-use-127-0-0-18000
.. _`Set up site menu for linking to the homepage and other pages`: https://docs.wagtail.org/en/stable/tutorial/set_up_site_menu.html
