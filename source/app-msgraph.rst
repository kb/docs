Microsoft Graph
***************

.. highlight:: python

Links
=====

- :doc:`sys-azure`
- :doc:`sys-microsoft`
- :doc:`sys-office-365`

Useful information:

- `Get access without a user`_
- `Register an application with the Microsoft identity platform`_
- `python-sample-console-app`_

App
===

Configuration
-------------

1. Configure Settings_ (register your application in Azure)
2. Add ``MSGRAPH_GROUP_NAME_TO_SYNC`` to ``settings/base.py``::

    MSGRAPH_GROUP_NAME_TO_SYNC = get_env_variable("MSGRAPH_GROUP_NAME_TO_SYNC")

  This is the name of the group e.g. ``kbuk`` in this screenshot:

.. image:: ./misc/azure/2022-05-25-azure-ad-group-name.png

Diagostics
----------

Click here for `Microsoft Graph error responses and resource types`_

Here is an example error::

  format 500: The operation has timed out. ('generalException')

The `Microsoft Graph error responses and resource types`_ says
``generalException``, *An unspecified error has occurred.*...

Management Commands
-------------------

To see if the Graph API is working:

.. code-block:: bash

  django-admin microsoft-graph-user code@pkimber.net

To run ``update_microsoft_graph_users`` (from ``msgraph.service``):

.. code-block:: bash

  django-admin update-microsoft-graph-users

The ``update-microsoft-graph-users`` management command will:

1. Retrieve all users from the Graph API
2. Select the list of *users to synchronise* by finding the members
   of the Active Directory group (``settings.MSGRAPH_GROUP_NAME_TO_SYNC``).
3. Add the users to the ``MicrosoftGraphUser`` model.
4. If a user has been removed from Active Directory, then the
   ``MicrosoftGraphUser`` record will be soft-deleted.
5. Retrieve all user *managers* from the Graph API
6. Add the managers to the ``MicrosoftGraphUserSupervisor`` model.

To download the Microsoft Graph users to a CSV file:

.. code-block:: bash

  django-admin microsoft-graph-users-as-csv

To download the Microsoft Graph groups to a CSV file:

.. code-block:: bash

  django-admin microsoft-graph-groups-as-csv

Pagination
----------

Paging Microsoft Graph data in your app
https://docs.microsoft.com/en-us/graph/paging

Example diff
https://gitlab.com/kb/msgraph/-/commit/b12a03bb1d174b94cd9a3a28d3303dc88de89c25

URLs
----

::

  urlpatterns = [
      url(regex=r"^microsoft/graph/", view=include("msgraph.urls")),
  ]

Template (Settings)
-------------------

.. code-block:: html

  {% block content %}
    <div class="pure-g">
      {% include 'msgraph/_settings.html' %}
    </div>
  {% endblock content %}

Settings
========

1. `Register an application with the Microsoft identity platform`_

   e.g. for an app called ``ticket-3597-v1``:

.. image:: ./misc/msgraph-overview.png

.. image:: ./misc/msgraph-redirect-uris.png

2. Under the applications *API permissions* page, choose *Add a permission*,
   select *Microsoft Graph*, and then choose the permissions your app requires
   under *Application permissions*:

.. image:: ./misc/azure/2021-08-19-api-permissions-with-group.png

.. note:: The ``User.Read`` permission does not need to be selected.
          It is automatically *Delegated* when you select ``User.ReadWrite``.

.. note:: If you change permissions, users and/or admins will have to consent
          even if they have done so previously.

.. tip:: 19/08/2021, The group permissions were added to allow us to
         *Sync user permissions from Active Directory*.

3. Under the applications *Certificates & secrets* page in the *Client secrets*
   section, create a *New client secret*:

.. image:: ./misc/msgraph-client-secret.png

.. warning:: I think you only get a single chance to copy this secret!

4. Copy the *Application (client) ID* to and *client secret* to your
   environment e.g::

     # .private
     set -x MSGRAPH_APPLICATION_ID "6731de76-14a6-4931de76-14a6-49ae"
     set -x MSGRAPH_CLIENT_SECRET "the-client-secret"

5. Browse to *Settings*, *Microsoft Graph*, *Get Consent*

.. image:: ./misc/msgraph-settings-get-consent.png

6. Make a note of the *Redirect URI* for the next step:

.. image:: ./misc/msgraph-get-consent.png

7. Under the applications *Authentication* page in the *Redirect URIs* section,
   set the redirect URI for your web site.

.. image:: ./misc/msgraph-redirect-uri.png

.. tip:: The *Redirect URI* is displayed on your web site under
         *Settings*, *Microsoft Graph*, *Get Consent*.


.. _`Get access without a user`: https://docs.microsoft.com/en-us/graph/auth-v2-service
.. _`Microsoft Graph error responses and resource types`: https://docs.microsoft.com/en-us/graph/errors#code-property
.. _`python-sample-console-app`: https://github.com/microsoftgraph/python-sample-console-app/blob/master/helpers.py
.. _`Register an application with the Microsoft identity platform`: https://docs.microsoft.com/en-us/graph/auth-register-app-v2
