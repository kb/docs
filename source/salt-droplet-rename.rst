Rename a Digital Ocean droplet
******************************

.. highlight:: bash

Rename on digital ocean control panel
=====================================

Login to the Digital Ocean Control Panel

Click the droplet you want to rename

Then, on the droplet detail window, click on the name of your droplet

Change the name in the entry field and click the tick mark beside the field

.. _rename_server:

Rename on the server
====================

Log in to the server and edit the hosts file::

  sudo vim /etc/hosts

Find the host entry that reads::

  127.0.1.1  <old droplet name>

n.b. this line may say::

  127.0.1.1  <old droplet name> <old droplet name>

Alter this host entry to read::

  127.0.1.1  <old droplet name> <new droplet name>

This is transitional meaning that the host will respond to both names. After
the host has been renamed and a reboot has been done you can remove
<old droplet name> from the entry

rename the server using the hostname command::

  sudo hostname <new droplet name>

.. tip:: To update the Salt minion, move onto the next section.

.. _salt_change_minion_id:

Update Salt configuration
=========================

As well as changing the host name you also need to alter the minion id and
update the master salt server.

Amend the minion id
-------------------

Stop the salt-minion service::

  sudo service salt-minion stop

Change minion id in /etc/salt/minion_id::

  sudo vim /etc/salt/minion_id

After you edit the file should contain the new droplet name.

Start the salt-minion service::

  sudo service salt-minion start

Update master server
---------------------

On the master server run check for the new minion name

  sudo salt-key -L

Accept the new minion name using::

  sudo salt-key -a <new droplet name>

Type Y<enter> to accept

Delete the old minion name::

  sudo salt-key -d <old droplet name>

.. note:: If you are changing the first part of the droplet name
          e.g from test-a to xy-a then ensure that the 'test-*' section
          has the same content as the 'xy-*' section

Rename the configuration file as follows::

  cd /srv/pillar/sites
  mv <old droplet name>.sls <new droplet name>.sls

Edit top.sls to use the new name::

  cd <pillar directory>
  vim top.sls

Find the section for the old droplet name::

  <old droplet name>:
    sites.<old droplet name>

Change this to::

  <new droplet name>:
    sites.<new droplet name>

