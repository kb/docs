ownCloud
********

To Do
=====

- Backup the ``/etc/ocis/ocis.yaml`` file (see below for details)

Install
=======

From `Bare Metal Deployment with systemd`_

Install the ownCloud software::

  sudo wget -P /usr/local/bin https://download.owncloud.com/ocis/ocis/stable/7.0.0/ocis-7.0.0-linux-amd64
  sudo chmod a+x /usr/local/bin/ocis-7.0.0-linux-amd64
  sudo ln -s -f /usr/local/bin/ocis-7.0.0-linux-amd64 /usr/local/bin/ocis
  # check the version
  ocis version --skip-services

Configuration
=============

Create the configuration file::

  sudo -u ocis ocis init --config-path /etc/ocis

.. tip:: You will be asked if you want to configure *Ininite Scale* with
         certificate checking disabled. You can safely answer yes.

.. warning:: Do **NOT** delete the ocis.yaml file without creating a backup
             of it first. When you regenerate the ocis.yaml file, it will be
             out of sync with the stored user data in the ocis data directory
             ``/var/lib/ocis``.
             If for some reason you still want to regenerate the config file,
             you first need to empty the data directory but beware if you
             already have user files stored there.
             For more information, see `Infinite Scale Data Directory`_

To create the certificate::

  init-letsencrypt owncloud.hatherleigh.info

.. tip:: No need to add the path to the folder (it didn't seem to work).

.. _`Bare Metal Deployment with systemd`: https://doc.owncloud.com/ocis/7.0/depl-examples/bare-metal.html#install-and-configure-the-infinite-scale-binary
.. _`Infinite Scale Data Directory`: https://doc.owncloud.com/ocis/7.0/depl-examples/bare-metal.html#infinite-scale-configuration-file
