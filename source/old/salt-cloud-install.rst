Salt Cloud - Install (Legacy Notes)
***********************************

04/04/2017, I followed the instructions in `Salt-Cloud to Spin Up DigitalOcean
Resources`_ which uses ``salt-cloud`` on the ``master``.  This worked very
nicely.

See :doc:`salt-cloud-digitalocean` for instructions.

_______________________________________________________________________________

.. warning:: The following notes are the old instructions.

.. important:: These instructions should install version 2015.8.3 (Beryllium)

Install dependencies::

  sudo apt-get install build-essential python-m2crypto sshpass python-software-properties

If the instructions above do not install version 2015.8.3, then try this::

  sudo -i
  # 14.04
  wget -O - https://repo.saltstack.com/apt/ubuntu/14.04/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -
  # 16.04
  wget -O - https://repo.saltstack.com/apt/ubuntu/16.04/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -

Add the following line to ``/etc/apt/sources.list``::

  vim /etc/apt/sources.list
  # 14.04
  deb http://repo.saltstack.com/apt/ubuntu/14.04/amd64/latest trusty main
  # 16.04
  deb http://repo.saltstack.com/apt/ubuntu/16.04/amd64/latest xenial main

  sudo apt update
  sudo apt install salt-cloud

Configuration files
-------------------

.. warning:: The configuration files are kept securely and should **never be
             copied to a public repository e.g. DropBox, GitHub or BitBucket**

.. note:: Replace ``patrick`` with your *User Name* (:doc:`../checklist`)

.. note:: Replace ``yb`` with your *Company Abbreviation* (:doc:`../checklist`)

::

  sudo -i
  cd /etc/salt/cloud.profiles.d/
  sudo ln -s /home/patrick/repo/dev/module/deploy/salt-cloud/yb.profiles.conf .
  cd /etc/salt/cloud.providers.d/
  sudo ln -s /home/patrick/repo/dev/module/deploy/salt-cloud/yb.providers.conf .

Create a Server
===============

You can now follow the appropriate instructions for creating your own cloud
server:

- :doc:`salt-cloud-amazon`
- :doc:`salt-cloud-digitalocean`
- :doc:`salt-cloud-rackspace`


.. _`Salt-Cloud to Spin Up DigitalOcean Resources`: https://www.digitalocean.com/community/tutorials/saltstack-infrastructure-configuring-salt-cloud-to-spin-up-digitalocean-resources
