Development with Docker
***********************

.. highlight:: bash

Install
=======

::

  apt install docker.io
  usermod -aG docker patrick

.. tip:: Replace ``patrick`` with your own Linux user name.

Docker Compose
--------------

::

  sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  # test
  docker-compose --version

Flowable
========

Docker Compose script for the Flowable Modeler, `docker-compose.yml`_:

.. tip:: This script run  Postgres in a container, so you will need to stop
         Postgres and Tomcat services running locally on your workstation.

Follow these steps to run `docker-compose.yml`_::

  wget https://raw.githubusercontent.com/flowable/flowable-engine/master/docker/modeler-idm-postgres.sh
  wget https://raw.githubusercontent.com/flowable/flowable-engine/master/modules/flowable-ui-modeler/flowable-ui-modeler-app/src/main/docker/docker-compose.yml

Edit the first line of ``modeler-idm-postgres.sh`` to read::

  DOCKER_COMPOSE_FILE="./docker-compose.yml"

Back at your command line::

  sudo service postgresql stop
  sudo service tomcat8 stop

  chmod +x modeler-idm-postgres.sh
  ./modeler-idm-postgres.sh start
  # to check progress
  ./modeler-idm-postgres.sh info

Browse to

  http://localhost:8080/flowable-modeler

Login as ``admin``, ``test``

Postgres
========

From `Don't install Postgres. Docker pull Postgres`_

Configure::

  sudo -i
  service docker start

  mkdir ~/repo/docker/postgres/volumes

Run::

  docker run --rm --name pg-docker -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v $HOME/repo/docker/postgres/volumes:/var/lib/postgresql/data postgres

Test::

  # the password is 'postgres'
  psql -h localhost -U postgres -d postgres

.. tip:: See Commands_ below...

Django
------

.. code-block:: python

  # dev_local.py
  DATABASES = {
      "default": {
          "ENGINE": "django.db.backends.postgresql_psycopg2",
          "NAME": "dev_test_invoice",
          "USER": get_env_variable("DATABASE_USER"),
          "PASSWORD": get_env_variable("DATABASE_PASS"),
          "HOST": get_env_variable("DATABASE_HOST"),
          "PORT": "",
      }
  }

  # .env.fish
  set -x DATABASE_HOST "localhost"
  set -x DATABASE_PASS "postgres"
  set -x DATABASE_USER "postgres"

Commands
--------

Create a database (called ``temp_test_create_on_docker``)::

  psql -X -U postgres -h localhost -c "CREATE DATABASE temp_test_create_on_docker TEMPLATE=template0 ENCODING='utf-8';"


.. _`docker-compose.yml`: https://raw.githubusercontent.com/flowable/flowable-engine/master/modules/flowable-ui-modeler/flowable-ui-modeler-app/src/main/docker/docker-compose.yml
.. _`Don't install Postgres. Docker pull Postgres`: https://hackernoon.com/dont-install-postgres-docker-pull-postgres-bee20e200198
