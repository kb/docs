Salt Masterless
***************

- `Salt Masterless Quickstart`_

Install Salt Masterless::

  sudo -i
  apt install --no-install-recommends salt-minion

Edit the ``/etc/salt/minion`` configuration file::

  # vim /etc/salt/minion

  file_client: local

  file_roots:
    base:
      - /srv/salt

  pillar_roots:
    base:
      - /srv/pillar

Disable the ``salt-minion`` service::

  service salt-minion stop
  systemctl disable salt-minion

Apply the states::

  salt-call --local state.apply -l debug


.. _`Salt Masterless Quickstart`: https://docs.saltstack.com/en/latest/topics/tutorials/quickstart.html
