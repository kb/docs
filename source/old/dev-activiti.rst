Old Activiti Notes
******************

.. highlight:: bash

- Here are the *new* :doc:`../dev-flowable` notes...

Deploy
======

Settings
--------

In ``settings/local.py``:

.. code-block:: python

  ACTIVITI_URL = 'activiti-rest'

In ``settings/production.py`` (after ``DATABASE``):

.. code-block:: python

  ACTIVITI_URL = 'activiti-rest-{}'.format(DOMAIN.replace('.', '-')

Pillar
------

To deploy, add ``workflow`` to your pillar e.g:

.. code-block:: yaml

  # sites/my.sls
  sites:
    www.hatherleigh.info:
      package: hatherleigh_info
      profile: django
      workflow: True

Create a ``workflow`` ``sls``:

.. code-block:: yaml

  # config/workflow.sls
  workflow:
    True

And add it to the config for the server e.g:

.. code-block:: yaml

  # top.sls
  'test-a':
    - config.workflow
    - sites.my

Database
--------

:doc:`../fabric-database`

.. warning:: The following notes are for manual install and testing of Activiti
             (perhaps on your local workstation).

Testing
=======

To test Activiti (and create initial tables), find the URL by starting with
``activiti-rest-`` and appending your domain name, replacing ``.`` with ``-``
e.g. for ``www.hatherleigh.info``::

  curl -u kermit:kermit -X GET http://localhost:8080/activiti-rest-www-hatherleigh-info/service/repository/deployments/

To check the logs::

  tail -f /var/lib/tomcat7/logs/catalina.out

Install
-------

Install tomcat and dependancies::

  sudo apt install tomcat8

The above is all that is required but you can also install::

  sudo apt install tomcat8-admin tomcat8-common tomcat8-docs tomcat8-examples tomcat8-user

Download Activiti-6.0.0 from https://www.activiti.org/download-links

Extract the zip and copy the war files ``activiti-app.war`` and
``activiti-rest.war`` from the ``wars`` directory to
``/var/lib/tomcat8/webapps``::

  cd <directory expanded activiti to>/wars/
  sudo cp activiti-rest.war activiti-app.war /var/lib/tomcat8/webapps/

Start tomcat using the command::

  sudo service tomcat8 start

The ``activiti-app`` war will be extracted and can be accessed (N.B. default db
config assumes mysql database called ``activitiui`` with username ``alfresco``,
password ``alfresco``::

  http://localhost:8080/activiti-app

By default the activiti app uses a mysql database if you want to configure an
h2 in memory database::

  sudo vim /var/lib/tomcat8/webapps/activiti-app/WEB-INF/classes/META-INF/activiti-app/activiti-app.properties

Comment out the lines::

  # datasource.driver=com.mysql.jdbc.Driver
  # datasource.url=jdbc:mysql://127.0.0.1:3306/activiti6ui?characterEncoding=UTF-8
  # datasource.username=alfresco
  # datasource.password=alfresco

  # hibernate.dialect=org.hibernate.dialect.MySQLDialect

Add the lines::

  # CONFIG FOR THE H2 IN MEMORY DATABASE
  datasource.driver=org.h2.Driver
  datasource.url=jdbc:h2:mem:activiti;DB_CLOSE_DELAY=-1
  datasource.username=sa
  datasource.password=

To test access, login in as ``admin@app.activiti.com`` with password ``test``.

The ``activiti-rest`` war will also be extracted (default db config is h2 in
memory) you can test access using::

  curl -u kermit:kermit -XGET http://localhost:8080/activiti-rest/service/repository/deployments/

Install Tomcat on Windows (incomplete!!)
----------------------------------------

Download and install java from java.com/download

Download the tomcat installer from tomcat.apache.org

Double click on the installer to run it.  Click "Next >" on the welcome page,
then click "I Agree".  On the Choose components page click next On the
configuration page Enter a username and password for the adminstrator (don't
think this has to be a windows user)

On the next screen browse to the address of the which you installed the jre

Download Activiti from https://activiti.org and expand the zip.  From the wars
directory copy the activiti-rest.war to the webapps directory of the tomcat
installation (e.g. C:\Program Files\Apache Software Foundation\Tomcat8\webapps).
Start tomcat if it is not already running, it will unpack the war file and make
the rest api available.  You should now be able to use the sample app.

To use Postgresql follow the instructions below

Database
--------

Activiti by default uses uses the h2 in memory database.  To use a database
that saves data we need to use a different database:

Configure Postgresql (requires jdbc driver, available from
https://jdbc.postgresql.org/download.html).  I downloaded::

  postgresql-9.4.1212.jar

If using tomcat8, copy ``postgresql-9.4.1212.jar`` to the ``lib`` folder::

  sudo cp postgresql-9.4.1208.jar /usr/share/tomcat8/lib/

Assuming your database is called ``activiti`` edit::

  /var/lib/tomcat8/webapps/activiti-app/WEB-INF/classes/META-INF/activiti-app/activiti-app.properties

.. code-block:: yaml

  datasource.driver=org.postgresql.Driver
  datasource.url=jdbc:postgresql://localhost:5432/activiti
  datasource.username=<activiti user>
  datasource.password=<activiti user password>

  # PJK, 25/11/2016, I un-commented this line as well
  hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect

In Activiti REST::

  /var/lib/tomcat8/webapps/activiti-rest/WEB-INF/classes/db.properties

.. code-block:: yaml

  db=activiti
  jdbc.driver=org.postgresql.Driver
  jdbc.url=jdbc:postgresql://localhost:5432/activiti
  jdbc.username=<activiti user>
  jdbc.password=<activiti user password>

Then create a database::

  psql -X -U postgres -c "CREATE DATABASE activiti TEMPLATE=template0 ENCODING='utf-8';"

Log into the database and create ``<activiti user>`` for the database::

  psql -X -U postgres activiti
  CREATE ROLE activiti WITH PASSWORD '<activiti user password>' NOSUPERUSER CREATEDB NOCREATEROLE LOGIN;

If the user already exists, you can update the password::

  ALTER USER "<activiti user>" WITH PASSWORD '<activiti user password>';

The PostgreSQL database needs to be configured to accept TCP connections.  Our
standard ``pg_hba.conf`` configuration is as follows::

  # /etc/postgresql/9.5/main/pg_hba.conf
  local all all trust

When you connect to the database via ``psql``, you are by default using a unix
domain socket connection - so the line starting with ``local`` applies.  If you
try to connect via a JDBC driver, the connection will be done via TCP, which
means lines starting with ``host`` are applicable.

To allow access via TCP, add the following ``host`` line to the file::

  # /etc/postgresql/9.5/main/pg_hba.conf
  host    all         all         127.0.0.1/32          md5

Re-start postgres::

  service postgresql restart

Re-start tomcat::

  sudo service tomcat8 restart

Check for issues in the log files::

  /var/lib/tomcat8/logs/catalina.out

Activiti Rest API
=================

The activiti rest api follows the convention of the 'Services' in the Java API

Repository Service::

  /service/repository/

Corresponds to the ``RepositoryService``.  This service manipulates the
deployments and process definitions

Runtime Service::

  /service/runtime/

Corresponds to the ``RuntimeService``.  This service is responsible for
manipulating processes e.g. starting a process.

Task Service::

  /service/tasks/

Corresponds to the ``TaskService``.  This service is responsible for tasks e.g.
completing a task

Form Service::

  /service/form

Corresponds to the ``FormService``.  This service is responsible for providing
the information that is required for start forms (to start a process) and task
forms (to complete a task).

Script Task
===========

We can use a ``scriptTask`` to run JavaScript code e.g:

.. code-block:: xml

  <scriptTask id="sid-32609229" scriptFormat="javascript" activiti:autoStoreVariables="false">
    <script><![CDATA[var secondLevel = parseFloat(secondLevelValue);
      var value = parseFloat(invoiceTotal);
      var firstLevelOnly = value < secondLevel;
      execution.setVariable("firstLevelOnly", firstLevelOnly);]]>
    </script>
  </scriptTask>

In this example:

1. We use JavaScript to convert ``Decimal`` variables (which are stored as a
   ``string``) to a ``float`` for a value comparison.
2. ``secondLevelValue`` and ``invoiceTotal`` are variables in the workflow
   process.  As you can see, it is very easy to access them.
3. ``execution.setVariable`` stores a variable into the workflow.  The variable
   can be used by another task later in the process.

The Workflow App
================

This consists of a module that encapsulates the activiti rest api
(``workflow/activiti.py``) and several views that use the api.

Use the module as follows (e.g. to list deployments)::

  from activiti import Activiti
  import json

  activiti = Activiti()
  response = activiti.deployments()
  # response is a dict that contains status information and an element called
  # data which contains the
  print (json.dumps(response))

The workflow app has an example app called ``example_workflow``.

This currently allows deployment of a workflow and listing of deployments and
process definitions.  To use the app run ``./init_dev.sh``

Log in as ``staff``

Click on the workflow tab.  The current process definitions are displayed.  To
see available deployments click on "Deployments"

To deploy a new workflow click "New Deployment" browse to a ``bpmn2.0.xml``
file and press "submit"

Appendix: Alternative Manual Install Tomcat
===========================================

.. note:: You'll need to install the dependancies (Java runtime etc)

Download version 8 of tomcat zip from https://tomcat.apache.org/download-80.cgi
and extract to ``/opt`` (requires ``sudo``) create a symbolic link to it as
follows::

  sudo ln -s /opt/apache-tomcat-8.0.33 /opt/tomcat

Install the script tomcat in ``~/bin``

.. code-block:: bash

  PATH=$PATH:/opt/tomcat/bin; export PATH
  TRUE=0

  if [ "$1" == "start" ]; then
      processes=`ps -ef | grep catalina | grep "Bootstrap start" | grep -v grep`
      if [ "${processes}" == "" ];then
          if [ `id -u` != 0 ]; then
              echo "Start Tomcat - this script uses sudo - you may need to enter your password"
          fi
          sudo /opt/tomcat/bin/catalina.sh $1
      else
          echo -e "\n\t*** Tomcat already running ***\n"
          echo ${processes}
      fi
  elif [ "$1" == "stop" ]; then
      processes=`ps -ef | grep catalina | grep "Bootstrap start" | grep -v grep`
      if [ "${processes}" != "" ];then
          if [ `id -u` != 0 ]; then
              echo "Stop Tomcat - this script uses sudo - you may need to enter your password"
              echo "This script uses sudo - you may need to enter your password"
          fi
          sudo /opt/tomcat/bin/catalina.sh $1
      else
          echo -e "\n\t*** Tomcat is not running ***\n"
          echo ${processes}
      fi
  elif  [ "$1" == "status" ]; then
      ps -ef | grep catalina | grep -v grep
  elif  [ "$1" == "restart" ]; then
      $0 stop
      sleep 4
      $0 start
  else
      echo -e "\nUsage\n\t`basename $0` start | stop | status\n"
      exit 2
  fi

Starting activiti from manual install
-------------------------------------

start tomcat using the command::

  tomcat start

If you've not created the above script you can start using::

  sudo /opt/tomcat/bin/catalina.sh start

Flowable
========

1. Download and extract the contents of ``flowable-6.4.2.zip``.
2. Copy ``flowable-rest.war`` to ``/var/lib/tomcat8/webapps/``.
3. Re-start Tomcat
4. I found ``flowable-default.properties`` in::

     /var/lib/tomcat8/webapps/flowable-rest/WEB-INF/classes/

     # and
     ./webapps/flowable-rest/WEB-INF/classes/META-INF
     ./webapps/flowable-rest/META-INF

   From
   https://github.com/flowable/flowable-engine/releases
   All Flowable apps (IDM, Modeler, Task, Admin and REST) now read their
   configuration from the ``flowable-app.properties`` file, which is included in
   each WAR file in the ``META-INF/flowable-app`` folder.
   Consequently, a single, shared ``flowable-app.properties`` can be placed on
   the classpath for all apps.
   All property files (e.g. ``db.properties`` for the REST app) are still read
   and work in a backwards-compatible way.

   So... perhaps I need to look in the ``flowable-modeler.war`` file?

   Here is a list of the Flowable apps:
   https://www.flowable.org/docs/userguide/index.html#flowableApps

5. Update ``flowable-default.properties``::

     /var/lib/tomcat8/webapps/flowable-rest/WEB-INF/classes/flowable-default.properties

   .. note:: I couldn't get Flowable to *find*
             ``flowable-app.properties``
             or
             ``flowable.properties``.
             This link
             https://github.com/flowable/flowable-engine/blob/2adba2d026f58acedfc17281b79acda7b8ab9956/docs/userguide/src/zh_CN/bpmn/ch14-Applications.adoc
             talks about using the
             ``application.properties`` file in the ``WEB-INF/classes/`` directory
             of each app, so perhaps that is the way forward.

   These settings seem to work::

     spring.datasource.driver-class-name=org.postgresql.Driver
     spring.datasource.url=jdbc:postgresql://localhost:5432/activiti
     spring.datasource.username=activiti
     spring.datasource.password=activiti

     flowable.rest.app.authentication-mode=any-user
     flowable.rest.app.admin.user-id=kermit
     flowable.rest.app.admin.password=kermit

6. For Flowable REST deployed with our default Salt states, then the test URL is:
   http://kermit:kermit@localhost:8080/flowable-rest/service/repository/deployments/
