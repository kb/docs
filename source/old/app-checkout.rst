Checkout (before Stripe SCA)
****************************

.. highlight:: python

- https://gitlab.com/kb/checkout
- Source code documentation for :doc:`app-checkout`

.. note:: This app replaces the old :doc:`app-pay`

``forms.py``::

  from checkout.forms import CheckoutForm

  class JobCheckoutForm(CheckoutForm):

      class Meta:
          model = Job
          fields = (
              'action',
          )

``views.py``::

  from checkout.views import CheckoutMixin

  class JobCheckoutView(
      PageFormMixin, CheckoutMixin, BaseMixin, UpdateView):

      model = Job
      form_class = JobCheckoutForm

In the ``html`` template:

.. code-block:: html

  {% include '_form.html' with submit='Checkout' form_id='id_checkout_form' %}

  {% block script %}
    {{ block.super }}
    {% include 'checkout/_stripe.js.html' %}
  {% endblock script %}

In your ``.private`` file, add your test keys for Stripe:

.. code-block:: bash

  export STRIPE_PUBLISH_KEY="pk_test_123"
  export STRIPE_SECRET_KEY="sk_test_456"

Testing::

  from checkout.tests.helper import check_checkout

  @pytest.mark.django_db
  def test_checkout():
      obj = JobFactory()
      check_checkout(obj)

Payment Plan Instalment
=======================

If a user has marked a payment plan instalment as paid (using *Mark Paid*) and
you want to make it due again.

Find the primary key of the ``ObjectPaymentPlanInstalment`` which is due::

  from checkout.models import ObjectPaymentPlanInstalment
  content_object = ObjectPaymentPlanInstalment.objects.get(pk=3164)

Check the payment plan is for the correct person::

  content_object.object_payment_plan.content_object
  # <Enrol: Mr P Kimber>

Create a ``Checkout`` object and ``fail`` the payment::

  from checkout.models import Checkout, CheckoutAction
  from django.contrib.auth.models import User

  user = User.objects.get(username="patrick.kimber")
  checkout = Checkout.objects.create_checkout(CheckoutAction.objects.manual, content_object, user)
  checkout.save()
  checkout.fail()

Stripe
======

To set-up Stripe, tick *Payment reviews* in *Emails* (*Settings*).  Stripe will
send email notifications for new payments placed in the review queue.

Testing
=======

Mock
----

To mock ``stripe.Charge.create`` and ``stripe.Customer.create``::

  import attr
  from unittest.mock import patch

  @attr.s
  class StripeCustomer:
      id = attr.ib()

  @patch("stripe.Charge.create")
  @patch("stripe.Customer.create", return_value=StripeCustomer(id="xyz"))
  @pytest.mark.django_db
  def test_payment(mock_charge, mock_customer, client):
      _check_success(client)
      assert mock_charge.called is True
      assert mock_customer.called is True

Customers
---------

If you are using a **copy** of the live data set and you want to run test
payments, then you might get a ``No such customer`` error.  This is because the
customer numbers on the live system will not match the customer numbers on your
test system.

To remove the customer records on the live system::

  from checkout.models import Customer
  Customer.objects.all().delete()

Payment Plan
------------

To test the payment plans...

Find a valid customer ID on the Stripe dashboard e.g:: ``cus_DsK5YbWqKHP9jj``.

Imports::

  from datetime import date
  from checkout.models import Customer, ObjectPaymentPlanInstalment

Update all customers on your data set to point to this customer e.g::

  Customer.objects.update(customer_id='cus_DsK5YbWqKHP9jj')

Find an instalment which is *due*::

  next_due = date(2018, 11, 1)
  instalment = ObjectPaymentPlanInstalment.objects.filter(due=next_due).first()

Set the date on the instalment to ``today``::

  instalment.due = date.today()
  instalment.save()

Check the instalment is due (and meets all other conditions)::

  ObjectPaymentPlanInstalment.objects.due

Run the payment run management command::

  django-admin.py process_payments

