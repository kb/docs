#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u
helm install flowable/flowable --name=kb-flowable --set postgres.enabled=false --set rest.enabled=true --set database.username=postgres --set database.password=postgres --set database.datasourceDriverClassName=org.postgresql.Driver --set database.datasourceUrl=jdbc:postgresql://kb-dev-db-postgresql.default.svc.cluster.local:5432/flowable --set flowable.host.external=localhost
