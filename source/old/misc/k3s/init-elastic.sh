#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u
helm install --name kb-elastic stable/elasticsearch --set client.serviceType=NodePort --set client.httpNodePort=30200 --set replicas=1 --set minimumMasterNodes=1

# --set cluster.plugins=[analysis-phonetic]

# From https://discuss.elastic.co/t/install-only-one-elasticsearch-master-with-helm/167875
# helm install --namespace efk --name elasticsearch elastic/elasticsearch
# --version 6.6.0-alpha1
# --set replicas=1
# --set minimumMasterNodes=1
# --set resources.requests.memory=1Gi
# --set volumeClaimTemplate.storageClassName=nfs
# --set volumeClaimTemplate.resources.requests.storage=200Gi
