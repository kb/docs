Digital Ocean
*************

.. highlight:: bash

.. note:: I followed the instructions in
          `Salt-Cloud to Spin Up DigitalOcean Resources`_ which uses
          ``salt-cloud`` on the ``master``.  This works very nicely.

These are the commands I ran on the ``master`` to install ``salt-cloud``,
set-up the configuration and create a new server (copies of the configuration
files are in our private repository)::

  apt-get install salt-cloud

  vim /etc/salt/cloud

  salt-cloud --list-providers

  mkdir -p /etc/salt/pki/cloud
  cat ~/.ssh/id_rsa > /etc/salt/pki/cloud/do.pem
  chmod 700 /etc/salt/pki/cloud
  chmod 600 /etc/salt/pki/cloud/do.pem

  vim /etc/salt/cloud.providers.d/do.conf
  mkdir -p /etc/salt/cloud.profiles.d
  vim /etc/salt/cloud.profiles.d/dev-profiles.conf

  salt-cloud --list-locations do
  salt-cloud --list-profiles do

  # I don't think we use 'dev-environment.map'
  mkdir /etc/salt/cloud.maps.d
  vim /etc/salt/cloud.maps.d/dev-environment.map

  # update the bootstrap script
  salt-cloud -u

  # create a server called ``kb-c`` using the ``web`` profile.
  salt-cloud -p web kb-c
  salt '*' test.ping
  salt 'kb-c' state.highstate > /tmp/xx 2>&1

_______________________________________________________________________________

.. warning:: The following notes are the old instructions.

.. important:: These instructions are for version 2015.8.3 (Beryllium)

Configuration
=============

.. note::

  Upload the public key for your root user to
  https://cloud.digitalocean.com/settings/security

  Give the key a *Name* (e.g. ``name-of-my-key.pub``) and add to
  ``ssh_key_name`` in ``cloud.providers`` (see below).

  It took me a while to understand this.  Thank you to Russell Ballestrini for
  this article:
  http://russell.ballestrini.net/create-your-own-fleet-of-servers-with-digital-ocean-and-salt-cloud/

Profile
-------

.. note:: Replace ``yb`` with your *Company Abbreviation* (:doc:`../checklist`)

::

  vim ~/repo/dev/module/deploy/salt-cloud/yb.profiles.conf

::

  droplet_512:
    provider: digitalocean
    size: 1GB
    image: ubuntu-14-04-x32
    location: London 1

Providers
---------

.. note:: Replace ``yb`` with your *Company Abbreviation* (:doc:`../checklist`)

Log into your Digital Ocean Control Panel and *Generate new token* in the *API*
section.  The *Personal Access Token* is used in the ``personal_access_token``
field (see below)::

  vim ~/repo/dev/module/deploy/salt-cloud/yb.providers.conf

::

  digitalocean:
    driver: digital_ocean
    personal_access_token: your-personal-access-token
    location: London 1
    ssh_key_file: /root/.ssh/id_rsa.pub
    ssh_key_name: name-of-my-key.pub
    minion:
      master: master.yourbiz.co.uk

The ``name-of-my-key.pub`` is the same name as you entered in the Digital Ocean
Control Panel (see above).

.. warning::

  The actual **name** must match... so if the name of the key in the Digital
  Ocean Control Panel is ``name-of-my-key.pub``, then this must match the value
  contained in ``ssh_key_name`` in ``yb.providers.conf``.

Usage
=====

To create a server called ``drop-temp``::

  sudo -i
  salt-cloud -p droplet_512 drop-temp

.. tip:: For logging, add ``--log-level=debug`` to the ``salt-cloud`` command.

.. note:: Take a note of the IP address of the newly created server

To log into the server::

  sudo -i
  ssh the.server.ip.address

Provision
=========

You will now want to set-up your server: :doc:`../salt-provision`...

Utility
=======

Locations
---------

To list all locations for a provider (in this example for ``digitalocean``)::

  sudo -i
  salt-cloud --list-location digitalocean

.. note::

  You can also use ``--list-images`` and ``--list-sizes``


.. _`Salt-Cloud to Spin Up DigitalOcean Resources`: https://www.digitalocean.com/community/tutorials/saltstack-infrastructure-configuring-salt-cloud-to-spin-up-digitalocean-resources
