WordPress (Old Notes)
*********************

- :doc:`../sys-wordpress`

Backup Files
============

I had no good plan for backing up and restoring WordPress files.
Try one of the following:

- lftp_
- ftp_
- cPanel_
- UpdraftPlus_

lftp
----

Find your FTP login details and log into your server.

Identify the folder containing your WordPress site e.g. ``public_html``.

.. tip:: Depending on your site, you could download the whole of the site
         folder or just the ``wp-content`` folder and the ``wp-config.php``
         settings file.

Use ``lftp`` to mirror the folder to your cloud server.
You may need to install `lftp``::

  apt install lftp

.. tip:: The `mirror_remote_directory_to_local_directory`_ script works very
         well (copy in the ``misc`` folder for our customer).
         Remember to update your user name, password, host name and folder.

ftp
---

If you prefer not to use ``lftp``, then ``wget`` should work::

  wget -r -l 0 --ftp-user="user@name.co.uk" --ftp-password=abcdefgh ftp://72.720.72.72/public_html/*

.. note:: My user name was an email address.  Using the separate parameters
          allows us to enclose this in double quotes.

cPanel
------

If you ask cPanel to create a *Backup* in the root folder, download it::

  wget ftp://72.720.72.72/backup-10.25.2021_09-04-06_abcdef.tar.gz --ftp-user=user@name.co.uk --ftp-password=abcdefgh

UpdraftPlus
-----------

If you use the *UpdraftPlus* plugin, *Backup Now*, make sure to
*Include your files in the backup* and *Send this backup to remote storage*...

- `How do I restore my site with UpdraftPlus?`_
- I want to restore, but cannot or have failed to do so from the WP Admin
  console, `can I restore manually?`_.


.. _`can I restore manually?`: https://updraftplus.com/faqs/i-want-to-restore-but-have-either-cannot-or-have-failed-to-do-so-from-the-wp-admin-console/
.. _`How do I restore my site with UpdraftPlus?`: https://updraftplus.com/faqs/restore-site-updraftplus/
.. _`mirror_remote_directory_to_local_directory`: https://gist.github.com/pixeline/0f9f922cffb5a6bba97a
