Django Debug Toolbar
********************

Add to ``requirements/local.txt``::

  django-debug-toolbar

In ``settings/dev_patrick.py`` (update to your own user name)::

  INSTALLED_APPS += (
      'debug_toolbar',
  )

12/09/2020m What would our *standard* config look like?  The following is copied
from one of our projects::

  import socket

  ALLOWED_HOSTS = [
      "127.0.0.1",
      "127.0.1.1",
      "localhost",
      socket.gethostbyname(socket.gethostname()),
  ]

  INSTALLED_APPS += (
      'debug_toolbar',
      'debug_panel',
  )

  MIDDLEWARE_CLASSES += (
      'debug_panel.middleware.DebugPanelMiddleware',
  )

  def show_toolbar(request):
      return DEBUG

  DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": lambda r: DEBUG}
