WordPress - Issues
******************

- :doc:`sys-wordpress`
- :doc:`sys-wordpress-plugin`
- :doc:`sys-wordpress-security`
- :doc:`sys-wordpress-update`

Redirect
========

Firewall
--------

Check `aios-bootstrap.php`.  It may contain a (file) redirect e.g::

  $GLOBALS['aiowps_firewall_data'] = array(
      'ABSPATH' => '/home/web/repo/project/hatherleigh.info/live/',
  );

To solve the redirect, update this as required and restart PHP FPM::

  systemctl reload php8.1-fpm.service

Options - URL
-------------

If your site redirects, then update all references to the site URL:

.. code-block:: sql

  # MySQL
  update wp_options set option_value = "https://testing.hatherleigh.info" where option_name = "siteurl" or option_name = "home";
