OnBuy
*****

.. highlight:: python

https://gitlab.com/kb/onbuy

::

  # .env.fish
  set -x ONBUY_API_URL "https://api.onbuy.com/v2/"
  set -x ONBUY_CONSUMER_KEY "my-onbuy-consumer-key"
  set -x ONBUY_SECRET_KEY "my-onbuy-secret-key"
  set -x ONBUY_SITE_ID "1234"

  # .gitlab-ci.yml
  - export ONBUY_API_URL="https://api.onbuy.com/v2/"
  - export ONBUY_CONSUMER_KEY="my-onbuy-consumer-key"
  - export ONBUY_SECRET_KEY="my-onbuy-secret-key"
  - export ONBUY_SITE_ID="1234"

::

  # settings/base.py
  ONBUY_API_URL = get_env_variable("ONBUY_API_URL")
  ONBUY_CONSUMER_KEY = get_env_variable("ONBUY_CONSUMER_KEY")
  ONBUY_SECRET_KEY = get_env_variable("ONBUY_SECRET_KEY")
  ONBUY_SITE_ID = get_env_variable("ONBUY_SITE_ID")
