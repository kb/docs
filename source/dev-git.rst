git
***

.. highlight:: bash

Links
=====

- :doc:`dev-gitlab`
- http://gitref.org/
- `the staging area`_

.. _git_branch:

Git Branches
============

On your local machine you can delete the untracked remotes using::

  git remote purge origin

.. note:: That does not delete the branches you have checked out.

1. To *Delete Merged Branches* on GitLab, see :ref:`gitlab_branch`
2. We have a script to delete :ref:`scripts_untracked_branches`

Configuration
=============

Set user name and password::

  git config --global user.name "Patrick Kimber"
  git config --global user.email code@pkimber.net
  git config -l

Enable colours::

  git config --global color.diff auto
  git config --global color.status auto
  git config --global color.branch auto

Set the default editor (to ``vim``)::

  git config --global core.editor vim

View configuration::

  git config -l

Basic usage
===========

Initialise
----------

::

  git init
  git add .
  git commit

Branch
------

This article is a nice little intro to branching:
`Git Tutorial : Starting with git using just 10 commands`_.

Clone
-----

::

  git clone url
  git pull

diff
----

To get a ``diff`` of the stuff in the index::

  git diff --cached

To get a ``diff`` of the stuff not yet staged (in the index)::

  git diff

Ignore
------

Edit the ``.gitignore`` file in the project folder e.g::

  # Can ignore specific files
  .DS_Store

  # Use wildcards as well
  *~
  *.swp

  # Can also ignore all directories and files in a directory.
  tmp/**/*

Log
---

::

  git log
  # list file names
  git log --name-status

GUI::

  gitk

Modify
------

.. note:: You have to **stage** a file before you can commit it.  You can do
          this automatically by using the ``-a`` parameter on the ``commit``
          command.

::

  git commit -a -m "Rename activation code."

...or... by adding the file to the staging error before committing::

  git add my-file.py
  git commit -m "Update colours."

If you enter an incorrect commit message, you can amend the message using::

  git commit --amend

Reset (revert)
--------------

Note: `'git revert' is not equivalent to 'svn revert'`_::

  git checkout filename

To actually remove a commit (from `Delete commits from a branch in Git`_):

.. warning:: ``git reset --hard`` WILL DELETE YOUR WORKING DIRECTORY CHANGES.
             Be sure to stash any local changes you want to keep before running
             this command.

Assuming you are sitting on that commit, then this command will wack it::

  git reset --hard HEAD~1

The ``HEAD~1`` means the commit before head.

Or, you could look at the output of ``git log``, find the commit id of the
commit you want to back up to, and then do this::

  git reset --hard <sha1-commit-id>

If you already pushed it, you will need to do a force push to get rid of it::

  git push origin HEAD --force

Stash
-----

::

  git stash
  git stash list
  git stash apply

Status
------

::

  git status
  git status -s

Remote repository configuration
===============================

To view the URL of a remote repository::

  git remote -v

To change the URI::

  git remote set-url origin git@github.com:pkimber/story.git

Server
------

.. note:: I don't think we need the ``.git`` on the end of the ssh path.

To create an empty repository on a remote server::

  cd ~/repo/temp/
  mkdir pillar.git
  cd pillar.git
  git init --bare

To push to this repository::

  git remote set-url origin ssh://patrick@46.10.8.55:/home/patrick/repo/temp/pillar.git
  git push origin master


Working with a branch
=====================

List and Checkout a branch
--------------------------

List::

  git branch -a

To checkout a remote branch e.g::

  git branch -a
  * master
    remotes/origin/747-board

To checkout ``747-board``::

  git checkout -b 747-board origin/747-board

Cherry Pick from a branch to another branch
-------------------------------------------

`How to merge a specific commit in git`::

  git cherry-pick aeca961

.. warning:: Checkout the `consequence of cherry-picking`_

Create a branch
---------------

::

  git checkout -b ticket-0488

This is shorthand for::

  git branch ticket-0488
  git checkout ticket-0488

Merge a branch into master
--------------------------

::

  git checkout master
  git merge ticket-0488

Switch branch
-------------

::

  git checkout master
  git checkout ticket-0488

A more advanced version (which I don't understand)::

  git checkout remotes/origin/0.7.X

Delete a branch
---------------

To delete the ``hotfix`` branch::

  git branch -d hotfix

Log
---

To see the branch on the log::

  git log --decorate

Rename a branch
---------------

From `How do I rename a local Git branch?`_::

  git branch -m <oldname> <newname>

If you want to rename the current branch, you can simply do::

  git branch -m <newname>

You can use this command to rename ``master`` e.g::

  git branch -m master 1010-angular
  git checkout live-site
  git branch -m live-site master

Tags
----

We can switch to tags in the same was as we switch branches (see above).


.. _`'git revert' is not equivalent to 'svn revert'`: http://bryan-murdock.blogspot.com/2007/07/git-revert-is-not-equivalent-to-svn.html
.. _`Delete commits from a branch in Git`: http://stackoverflow.com/questions/1338728/delete-commits-from-a-branch-in-git
.. _`Git Tutorial : Starting with git using just 10 commands`: http://blog.xkoder.com/2008/08/13/git-tutorial-starting-with-git-using-just-10-commands/
.. _`the staging area`: http://www.gitready.com/beginner/2009/01/18/the-staging-area.html
.. _`consequence of cherry-picking`: http://stackoverflow.com/questions/880957/pull-all-commits-from-a-branch-push-specified-commits-to-another/881014#881014
.. _`How do I rename a local Git branch?`: http://stackoverflow.com/questions/6591213/how-do-i-rename-a-local-git-branch
.. _`How to merge a specific commit in git`: http://stackoverflow.com/questions/881092/how-to-merge-a-specific-commit-in-git
