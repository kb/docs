Google
******

- Click here for :doc:`project-simple-site`
- Click here for :ref:`base_google_analytics` in the ``base`` app
- Click here for :ref:`block_google_analytics` in the ``block`` app
- Click here for :doc:`dev-captcha`
- Click here for :doc:`dev-google`
- Click here for :doc:`sys-google-apps`

Set up Google Analytics for a website
=====================================

.. note:: Copied from Greg's notes, 17/12/2018

Add the site map code as per these instructions:
https://www.pkimber.net/open/dev-sitemap.html

Add the website as a container to Google Tag Manager:
https://tagmanager.google.com/#/home

To the right of *KB Software* click the 3 dots and select *Create Container*

Add the site name: www.xxxx.co.uk

Add the code to each page as requested

git push the amended code

You now need to add the Google Analytics (Universal) tag

First you need the Tracking ID from Google Analytics:
https://www.google.com/analytics/web/?hl=en#home

Add the site by clicking *Create new Property* under the Properties column,
from the Property section pull down menu

Fill out the details and click Get Tracking ID

Copy and paste the Tracking ID into the Tag Manager

Track Type - Page View; Fire on - All Pages

In GA don't forget to publish the changes

Deploy the amended pages to the site

In Webmaster Tools (Search Console):
https://www.google.com/webmasters/tools/home

Add the site as a property and when asked verify the account against the Tag
Manager account Click continue

Under Crawl first:
- select Fetch as Google and then Submit - Crawl this URL and its direct links
- select Sitemaps and add a site map - https://www.xxx.co.uk/sitemap.xml
- run test site map - check all pages submitted
- If all Ok submit the site map and refresh the page

You should receive an email ref:
Improve the search presence of https://www.xxx.co.uk/

Follow instructions 1 - 4

- In 1 add a property for
  https://www.xxx.co.uk,
  https://xxx.co.uk,
  www.xxx.co.uk and
  xxx.co.uk - verify, set up a site map and do a fetch for each
- In 2 select prefer www. for each property
  - Tools (right hand cog)/site settings
- In 4 add another user for each url variation, full rights
  - Tools (right hand cog)/site settings/Users and Property Owners
