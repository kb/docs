Tailwind UI
***********

- :doc:`dev-tailwind`
- :doc:`dev-tailwind-ember`

Forms
=====

We are using `Two-column cards with separate submit actions`_

Mapping
=======

::

  KbForm                = KbForm::Container
                        = KbForm::Form
                        = KbForm::Form::Field
  KbForm::Boolean       =
  KbForm::String        = KbForm::Form::Field::Input
  KbForm::Error         = KbForm::Form::Field::ErrorMessageChangeset
                          KbForm::Form::Field::ErrorMessageServer
  KbForm::SubmitButtons = KbForm::Form::Button


.. _`Two-column cards with separate submit actions`: https://tailwindui.com/components/application-ui/forms/form-layouts#component-db11f83176d113e39bf2559da9344b1c
