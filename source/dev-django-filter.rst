Django Filter
*************

.. highlight:: python

https://django-filter.readthedocs.io/

Add the following to ``requirements/base.txt``::

  django-filter==

.. tip:: Check the :doc:`dev-requirements` for the current version...

In your ``views.py`` module::

  import django_filters

  from django_filters.views import FilterView

  # create a 'FilterSet'
  class RejectedEmailFilter(django_filters.FilterSet):
      email = django_filters.CharFilter(lookup_expr="icontains")
  
      class Meta:
          model = RejectedEmail
          fields = ["email"]
  
  
  # create a 'FilterView'
  class RejectedEmailListView(
      LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, FilterView
  ):
  
      filterset_class = RejectedEmailFilter
      paginate_by = 20
  
      def get_queryset(self):
          return RejectedEmail.objects.all().order_by("-event_date")

.. tip:: Source code copied from ``mail/views.py``

Include the ``_form_filter.html`` template:

.. code-block:: html

  <div class="pure-g">
    <div class="pure-u-1">
      {% with form=filter.form %}
        {% include '_form_filter.html' %}
      {% endwith %}
    </div>
  </div>

.. tip:: Source code copied from
         ``mail/templates/mail/rejectedemail_filter.html``

.. warning:: 08/07/2021, I only just created the ``_form_filter.html`` template
             so feel free to improve it!
