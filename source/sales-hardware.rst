Hardware Sales
**************

Before Ordering
===============

Dell
----

.. tip:: We like the *Small Form Factor* (which we sold to AgBag).

Monitor / Display
-----------------

- Check the connector on the PC and the monitor.  Is it the same?
  (Display Port is the standard for new equipment I think).
- Do we have a cable?

Network
-------

- Does the office have a wired network?
  If not, do we have a WiFi adapter?

Software
--------

- Microsoft Windows Pro (for Remote Desktop).
- Do they use Microsoft Office?  If so, do they have a valid licence?
- Security - should we use `Microsoft 365 Defender`_?

Configuration
=============

Install
-------

- Zoho Assist
- Office 365
- Virus Checker (we will try `Microsoft 365 Defender`_).


.. _`Microsoft 365 Defender`: https://docs.microsoft.com/en-us/microsoft-365/security/defender/get-started
