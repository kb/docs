WordPress
*********

.. highlight:: bash

- :doc:`sys-wordpress-issues`
- :doc:`sys-wordpress-plugin`
- :doc:`sys-wordpress-security`
- :doc:`sys-wordpress-transfer`
- :doc:`sys-wordpress-update`
- :doc:`sys-wordpress-woocommerce`
- :doc:`sys-swap-space`
- :doc:`app-woocommerce`

Diagnostics
===========

.. tip:: Some initial notes...

Scripts to count the number of requests::

  count-nginx-access
  count-mysql-slow

Useful logs::

  /var/log/php8.1-fpm.log
  /var/log/php-slow-hatherleigh.info.log

PHP

We have the following settings in the Salt pillar::

  php_max_children: 15
  php_start_servers: 2
  php_min_spare_servers: 1
  php_max_spare_servers: 3

Click here to block a :ref:`wordpress-security-user-agent` using Salt

Firewall

- MB to ask MD - How do we exclude a specific IP address from the Firewall?

Server
======

Create a server (following the usual process).

Configure the Salt pillar (see existing WordPress sites for an example).

.. tip:: If you are using the ``www`` subdomain, then don't forget to add a
         ``redirect`` e.g. ``redirect: hatherleigh.info``.

:doc:`sys-swap-space`...

Create a MySQL database using ``fabric``, :doc:`fabric-database`.

To view the database on the server::

  mysql --user=www_hatherleigh_info --password=MyPassword www_hatherleigh_info

Migrate / Transfer
==================

:doc:`sys-wordpress-transfer`

Configure
=========

SSL Certificate
---------------

Check that a DNS record exists for the domain, then create an SSL certificate::

  init-letsencrypt www.hatherleigh.info - /home/web/repo/project/www.hatherleigh.info/live/

If your site redirects from ``www`` to a bare domain name, then::

  init-letsencrypt hatherleigh.info www.hatherleigh.info /home/web/repo/project/hatherleigh.info/live

.. tip:: For more information, see `Chat ref LetEncrypt on a bare domain`_

.. tip:: The above commands work perfectly, but if you need to use a
         DNS challenge, see :doc:`detail/ssl-letsencrypt-dns`

To check the certificates::

  sudo certbot certificates

Config
------

.. tip:: ``wp-config.php`` is found in the ``live`` folder e.g.
         ``~/repo/project/www.hatherleigh.info/live/wp-config.php``.

Update ``wp-config.php``::

  # update the database details
  define('DB_NAME', 'www_hatherleigh_info');
  define('DB_USER', 'www_hatherleigh_info');
  define('DB_PASSWORD', 'MyPassword');

To override the domain name (for test sites etc), try setting::

  define( 'WP_HOME', 'https://www.hatherleigh.info' );
  define( 'WP_SITEURL', 'https://www.hatherleigh.info' );

Logging
-------

Use the following in ``wp-config.php``::

  define('WP_DEBUG', true);
  define('WP_DEBUG_DISPLAY', false);
  @ini_set('display_errors', 0);

  // Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
  // define( 'SCRIPT_DEBUG', true );

.. tip:: ``define('WP_DEBUG', true);`` sends debug output to the log files
         (which is what we want)...

.. warning:: The important thing is to set ``@ini_set('display_errors', 0);``,
             so the user doesn't see errors on the screen.

.. warning:: It would be a security issue to log to the ``wp-content`` folder.
             To avoid this issue, our Salt states configure the PHP pool
             (see ``php/fpm.conf``).

Check
-----

Check memory usage::

  atopsar -m

.. tip:: For details, see https://www.kbsoftware.co.uk/crm/ticket/6121/

Check your site health,
https://www.hatherleigh.info/wp-admin/site-health.php?tab=debug

  *Filesystem Permissions* shows whether WordPress is able to write to it's
  own directories.

Mail
----

Follow instructions in :doc:`sys-postfix` (check existing sites to make sure
notes are up to date)!

Media Library
-------------

Check the ``upload_path`` for the *Media Library*::

  mysql> SELECT * FROM wp_options WHERE option_name = "upload_path";
  +-----------+-------------+---------------------------------------------------------------------+----------+
  | option_id | option_name | option_value                                                        | autoload |
  +-----------+-------------+---------------------------------------------------------------------+----------+
  |        51 | upload_path | /home/web/repo/project/www.hatherleigh.info/live/wp-content/uploads | yes      |
  +-----------+-------------+---------------------------------------------------------------------+----------+
  1 row in set (0.01 sec)

This example uses ``wp_`` as the table prefix.  This prefix may vary.
To find the prefix for your current database::

  cat ~/repo/project/www.hatherleigh.info/live/wp-config.php | grep prefix
  # or
  cd ~/repo/project/www.hatherleigh.info/live/
  cat wp-config.php | grep prefix

e.g.::

   UPDATE wp_options SET option_value = "/home/web/repo/project/www.hatherleigh.info/live/wp-content/uploads" WHERE option_name = "upload_path";

Swap Space
----------

You may want to :doc:`sys-swap-space` ...

Staging / Test Site
===================

The Salt states create a ``dump-`` script for *live* sites and ``stage-``
scripts for *test* sites.  The idea is to only allow dump for live sites,
so we don't accidentally stage to a live site.

e.g::

  /home/web/dev/managed/dump-stage/dump-www-hatherleigh.info.py
  /home/web/dev/managed/dump-stage/stage-hatherleigh-kbsoftware-co-uk.py

- ``dump-`` scripts are created for *live* sites.
  They save ``sql`` and ``tar.gz`` files to the ``/home/web/repo/dump/`` folder.
- ``stage-`` scripts are created for test sites (``testing: True`` in the ``pillar``).
  (to try and prevent us overwriting live sites).
- The ``stage-`` script will restore the ``tar.gz`` archive to the ``live`` folder for the test
  site.  If a ``live`` folder already exists, it will be renamed e.g.
  ``/home/web/repo/project/hatherleigh.kbsoftware.co.uk/live-until-20220408-1152``

.. warning:: Unless you create a new database for the staging site, the MySQL restore will
             merge the tables etc...

.. tip:: The scripts for the salt states are in the ``php`` folder
         (``base.py``, ``dump.py``, ``README.rst``, ``stage.py``).

Command line (``wp``)
=====================

::

  sudo -i -u web
  # list
  wp --path=/home/web/repo/project/www.protoolz.co.uk/live plugin list
  # status
  wp --path=/home/web/repo/project/www.protoolz.co.uk/live plugin status


.. _`Chat ref LetEncrypt on a bare domain`: https://chat.kbsoftware.co.uk/kb/pl/d6utd4cjat8n9pyi3y7wh13cur
.. _`Example wp-config.php for Debugging`: https://wordpress.org/support/article/debugging-in-wordpress/
