Nginx
*****

.. highlight:: bash

Scripts
=======

We have a ``bash`` script which counts the number of requests made to the
Nginx server::

  count-nginx-access | more
