Job
***

.. highlight:: python

Index
=====

To re-index the jobs::

  django-admin init_job_search

Candidate
=========

To remove a candidate profile, follow the notes in this ticket:
https://www.kbsoftware.co.uk/crm/ticket/4666/

.. tip:: Don't forget to check the list of consents for the contact to make
         sure they are correct.

Contact
=======

In the interests of reuse the ``Recruiter`` and ``Candidate`` models use a
contact model which is defined in the project or another app.

The contact model must be specified in ``settings.CONTACT_MODEL`` using the
form <app name>.<contact model name> e.g::

  CONTACT_MODEL = 'example_job.Contact'

The contact model must have the following fields, methods and URLs:

Fields::

  address_1
  address_2
  address_3
  company_name
  country
  county # technically this is the region but called county for historical reasons
  dob
  mobile
  nationality
  position
  postcode
  town
  user # as defined in settings.AUTH_USER_MODEL
  website

Properties::

  full_name
  is_recruiter
  is_candidate

You must also have a ``contact.detail`` URL which takes you to the contact
detail for the staff user e.g::

  url(regex=r'^contact/(?P<pk>\d+)/$',
      view=ContactDetailView.as_view(),
      name='contact.detail'
      ),

Management Commands
===================

::

  django-admin process-feed-simply-law-jobs

Tasks
=====

We have a scheduled task to ``send_email_latest_jobs``.

This task requires:

1. An email template which can be initialised using ``init_app_job``
2. An unsubscribe template with a URL of ``web.unsubscribe`` e.g::

     path(
         "unsubscribe/<str:token>/",
         view=TestUserConsentUnsubscribeUpdateView.as_view(),
         name="web.unsubscribe",
     ),

.. tip:: For more information on GDPR, see :ref:`gdpr_unsubscribe`.

Testing
-------

To send a test email, use the ``send_job_notify_email`` management command e.g::

  django-admin.py send_job_notify_email patrick@kbsoftware.co.uk

- This management command by-passes the selection of candidates by checking
  their GDPR consent and just sends an email to the specified email address.
- You will need to make sure the user has given consent for
  *Candidate Jobs Notification*

Unit Testing
============

Use the ``check_contact`` function to check your model and URLs are correct
e.g::

  from job.tests.helper import check_contact
  check_contact(ContactFactory())
