Remove a site from a server
***************************

.. hint:: this document gives examples using a site called
          'www.hatherleigh.info' and server called 'server1' replace this with
          the name of the name of your actual server and the site you wish to
          remove.


DNS
===

Remove the site from dns - probably requires removing an A record
with the IP address and CNAME record for the www name.  Follow the instructions
provided by your isp to achieve this.


Salt Configuration
==================

Log on to your master server and remove the site from the pillar sls file for
the server::

  cd /path/to/pillar
  vim sites/<server>.sls

e.g.::

  vim sites/server1.sls

Remove or comment the entry for the site which might look like this::

  <site name>:
    site-key-1: site-value-1
    site-key-2: site-value-2
    site-key-3: site-value-3

e.g to comment out a site called www.hatherleigh.info::

  # www.hatherleigh.info:
  #   site-key-1: site-value-1
  #   site-key-2: site-value-2
  #   site-key-3: site-value-3


Perform a ``state.apply``::

  sudo salt '<server name>' state.apply --state-verbose=False

e.g.::

  sudo salt 'server1' state.apply --state-verbose=False

On the server
=============

Log on to the server and manually remove the vassal files::

  sudo rm /home/web/repo/uwsgi/vassals/<site name>.ini
  sudo rm /home/web/repo/uwsgi/vassals/<site name>.celery-beat.ini
  sudo rm /home/web/repo/uwsgi/vassals/<site name>.celery-worker.ini

e.g.::

  sudo rm /home/web/repo/uwsgi/vassals/www.hatherleigh.info.ini
  sudo rm /home/web/repo/uwsgi/vassals/www.hatherleigh.info.celery-beat.ini
  sudo rm /home/web/repo/uwsgi/vassals/www.hatherleigh.info.celery-worker.ini

  the uwsgi emperor automatically detect that a change has occured and will
  stop all the processes associated with the site

Remove the nginx include file::

  rm /etc/nginx/include/<site name>.conf

e.g.::

  rm /etc/nginx/include/www.hatherleigh.info.conf

Perform a full backup::

  sudo -u web -i

  backup.<site name>.sh full

e.g.::

  backup.www.hatherleigh.info.sh full

Remove the backup from the crontab entry::

  rm /etc/cron.d/<site name with dots replaced with _>

e.g.::

  rm /etc/cron.d/www_hatherleigh_info

Remove the ssl certificate from letsencrypt.  The certbot does not currently
have an option to stop renewing a certificate.  To do this manually perform
the following procedure::

  sudo rm -r /etc/letsencrypt/live/<site name>
  sudo rm -r /etc/letsencrypt/archive/<site name>
  sudo rm /etc/letsencrypt/renewal/<site name>.conf

Optionally remove site files from the 'project' 'files' and backup
directories and remove the database (e.g a postgresql database)::

  sudo rm -r /home/web/repo/project/<site name>
  sudo rm -r /home/web/repo/files/<site name>
  sudo rm -r /home/web/repo/backup/<site name>
  psql -X -U postgres -c "DROP DATABASE <site name with dots replaced with _>"

e.g.::

  sudo rm -r /home/web/repo/project/www.hatherleigh.info
  sudo rm -r /home/web/repo/files/www.hatherleigh.info
  sudo rm -r /home/web/repo/backup/www.hatherleigh.info
  psql -X -U postgres -c "DROP DATABASE www_hatherleigh_info"
