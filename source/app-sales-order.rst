Sales Order
***********

- GIT
  https://gitlab.com/kb/sales_order
- Fixed price work ticket:
  https://www.kbsoftware.co.uk/crm/ticket/1017/
- WIP - `Fixed Price Work`_

Icon::

  <i class="fa fa-shopping-cart"></i>

Introduction
============

Find a way to get visibility of work done by partners and how to charge
customers...

Work done by partners
---------------------

1. A *partner* will record all of their chargeable time as *Billable*.
   *Chargeable* time is defined as work they expect to be paid for.
2. The *Time Analysis by User / Ticket (last 3 months)* report
   (``TimeAnalysisByUserTicketReport``) will display *Chargeable* time
   (in minutes) for each partner.
3. The time on this report should be reconciled to the partner's monthly
   invoice
   (03/12/2020, email from Paul:
   *Yes that report was what I needed, so I was able to confirm my times*).

How to charge customers
-----------------------

Every ticket with chargeable time should have a sales order.  The sales order
will control the customer invoicing.

1. To identify tickets with chargeable time, but no sales order, run the
   *Tickets with Chargeable Time but No Sales Order* report
   (``TicketChargeableTimeNoSalesOrder``).
   (This report only checks chargeable time recorded since 01/02/2022).
2. Make sure all the tickets listed by
   *Tickets with Chargeable Time but No Sales Order* report
   are included on a sales order.

.. warning:: Why do we need the *Funded Tickets with No Sales Order* report?

Definitions
===========

``funded``
  The ``funded`` field on the ``Ticket`` model uses the ``Funded`` model to
  determine how the ticket will be funded e.g. *Free of Charge* or *Sales Order*

``sales_order``
  A ``SalesOrder`` is linked to a ``Ticket`` using the ``SalesOrderTicket``
  model.

Methods
=======

1. ``check_tickets``
   Tickets with chargeable time, but no sales order.

2. ``needs_sales_order`` (``Ticket``)
   Is this a ``funded`` ticket?  Should it have a sales order?

3. ``tickets_without_sales_order`` (``SalesOrderTicket``)
   Tickets with chargeable time, but no sales order.


.. _`Fixed Price Work`: https://docs.google.com/document/d/1JIigXrTU7PuGBhc1vouZbMLo8ihFC9ZBiWnRzDXYYDQ/edit?usp=sharing
