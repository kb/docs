Scripts to aid the development process
**************************************

We've create a number of scripts that aid our development process.

Installation::

  cd ~/dev/module
  git clone https://github.com/mdinsmore/dev-scripts.git
  cd dev-scripts/
  ./createbin

The README.rst file in the repo has more information.

``release``
===========

To release of a project you can simply use ``release`` rather than creating a
symlink to ``kb.py``.

It also has the added benefits of making sure you have the latest version of
``toolbox``, displaying the requirements, checking the version against the
``kb pypi``,  running ``grunt`` (if there's a ``Gruntfile.js`` in the project)
doing doing an ``npm run scss-minify`` (if there's a ``scss`` directory).


``set-branches``
================

This script sets the branch for each of the apps used by the project.

The branch configuration file
-----------------------------

The script requires a file called ``requirements/branch.txt``. This should
contain a line for each app used in the project using the format::

  app|branch

E.g. for a project which requires the block app to be on the delete-image
branch and the base, compose, login, and mail apps on master you'd set up
like this::

  base|master
  block|delete-image
  compose|master
  login|master
  mail|master

Run the command
---------------

To use the set-branches script just change to the project directory and
type::

  set-branches

.. _scripts_untracked_branches:

``untracked-branches``
======================

Deletes the reference branches that are not on the remote repository and then
optionally prompts to delete the local ones.

1. To *delete untracked remotes* on your local machine, see :ref:`git_branch`
2. To *Delete Merged Branches* on GitLab, see :ref:`gitlab_branch`
