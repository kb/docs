Kubernetes for Development - Install
************************************

.. highlight:: bash

.. tip:: If you have the Requirements_ already installed, you can jump to
         `Create a Cluster`_...

.. warning:: The Helm chart for Flowable is in version 2 format, so we can't
             use Helm 3.

Requirements
============

Scripts
-------

Clone our https://gitlab.com/kb/k3d GIT repository e.g::

  cd ~/dev/module/
  git clone git@gitlab.com:kb/k3d.git

Docker
------

::

  apt install docker.io
  usermod -aG docker patrick

.. tip:: Replace ``patrick`` with your own Linux user name.

.. tip:: Log out of your workstation (and back in again)...

k3d
---

k3d_ is https://k3s.io/ running in a Docker_ container...

::

  wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | TAG=v1.7.0 bash

kubectl
-------

Install ``kubectl``::

  curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.3/bin/linux/amd64/kubectl
  mv kubectl ~/bin/
  chmod +x ~/bin/kubectl

helm
----

From `Initialize Helm (Install Tiller)`_
(and `GitHub issue, tiller won't install #489`_)

Download ``helm`` from https://github.com/helm/helm/releases/tag/v2.16.1

.. note:: These instructions work for ``helm`` version 2.x.x, but not 3.x.x.

For example:

- Download ``helm-v2.16.12-linux-amd64.tar.gz``.
- Extract ``helm`` and move it to your ``/usr/local/bin/helm`` folder.

Check::

  helm version

Repositories::

  helm repo add stable https://charts.helm.sh/stable
  # Bitnami
  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm repo update
  # search / find charts
  helm repo list redis

.. tip:: Repositories added using ``helm repo add`` are listed by typing
         ``helm repo list`` (and in ``~/.helm/repository/repositories.yaml``).

Create a Cluster
================

::

  k3d create

Set the ``KUBECONFIG`` environment variable::

  # bash
  export KUBECONFIG=$(k3d get-kubeconfig)
  # fish
  set -x KUBECONFIG (k3d get-kubeconfig)

.. tip:: If you get ``The connection to the server localhost:8080 was refused``
         then I solved the issue as follows:
         ``k3d delete k3s-default``, ``rm -rf ~/.kube`` and ``k3d create``.

Check::

  kubectl cluster-info

Configure::

  kubectl -n kube-system create serviceaccount tiller
  kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
  helm init --service-account tiller

  # test (wait for deployment)
  kubectl -n kube-system  rollout status deploy/tiller-deploy

Change into our Scripts_ folder and run the Rancher local storage script e.g::

  cd ~/dev/module/k3d/
  kubectl apply -f rancher-local-path-storage.yaml
  # check
  kubectl get storageclass

.. note:: Not sure if this is needed, but I don't think Postgres_ will initilise
          without it
          (``rancher-local-path-storage.yaml`` is a copy of the file from
          https://github.com/rancher/local-path-provisioner).

Postgres
========

Install k3d_ and Helm_ (we install Postgres using the Bitnami repository)...

Change into our Scripts_ folder and run the ``init-postgres.sh`` script e.g::

  cd ~/dev/module/k3d/
  ./init-postgres.sh

  # to monitor progress
  kubectl get pods -w

.. tip:: To delete ``postgres``, (``helm delete kb-dev-db``)
         (or ``helm del --purge kb-dev-db``).

04/05/2020
----------

Upgraded to ``bitnami/postgresql``.  To upgrade::

  helm upgrade kb-dev-db bitnami/postgresql

Connect to psql
---------------

.. tip:: You may need to wait a minute or two to get a connection...

::

  # bash
  export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
  PGPASSWORD="postgres" psql --host $NODE_IP --port $NODE_PORT -U postgres -d postgres

  # fish
  set -x NODE_IP (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x NODE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
  export PGPASSWORD="postgres"; psql --host $NODE_IP --port $NODE_PORT -U postgres -d postgres

.. _kubernetes_install_flowable:

Flowable
========

Install k3d_, Helm_ **and** `Postgres`_

Add the flowable repo::

  helm repo add flowable https://flowable.org/helm/

`Connect to psql`_ and create the ``flowable`` table:

If you need to... drop the database::

  DROP DATABASE flowable;

If you have ``other sessions using the database`` and you still want to DROP::

  SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND datname = 'flowable';

Create the database::

  CREATE DATABASE flowable TEMPLATE=template0 ENCODING='utf-8';

Change into our Scripts_ folder and run the ``init-flowable.sh`` script e.g::

  cd ~/dev/module/k3d/
  ./init-flowable.sh

  # to monitor progress
  kubectl get pods -w

Create the Ingres::

  cd ~/dev/module/k3d/
  kubectl apply -f flowable-ingres.yaml

Find the IP address for ``flowable-rest``::

  kubectl get ing
  # NAME                  HOSTS   ADDRESS      PORTS   AGE
  # flowable-rest         *       172.20.0.2   80      2m6s

Browse to::

  http GET http://rest-admin:test@172.20.0.2/service/repository/deployments/

To add Flowable to your project, see :doc:`dev-flowable` development notes...

.. tip:: To delete ``flowable``, (``helm delete kb-flowable``)
         (or ``helm del --purge kb-flowable``).

Microsoft SQL Server
====================

- Install k3d_ and Helm_ before installing the SQL Server package.
- Click here for `SQL Server - Linux - Restore Backup`_ notes...

Change into our Scripts_ folder and run the ``init-postgres.sh`` script e.g::

  cd ~/dev/module/k3d/
  ./init-microsoft-sql-server.sh

  # to monitor progress
  kubectl get pods -w

To `Install tools on Ubuntu 16.04`_::

  curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
  curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list
  apt update
  apt install mssql-tools unixodbc-dev

To connect, you first need to initialise your environment::

  # fish shell e.g. '.env.fish'
  set -x MICROSOFT_SQL_SERVER_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x MICROSOFT_SQL_SERVER_PASS (kubectl get secret --namespace default kb-microsoft-mssql-linux-secret -o jsonpath="{.data.sapassword}" | base64 --decode)
  set -x MICROSOFT_SQL_SERVER_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-microsoft-mssql-linux)

  # then connect
  export SQLCMDPASSWORD=$MICROSOFT_SQL_SERVER_PASS; sqlcmd -S $MICROSOFT_SQL_SERVER_HOST,$MICROSOFT_SQL_SERVER_PORT -U sa

Restore
-------

.. note:: The data is *lost* between sessions, so we have to repeat the
          following each time we restart the cluster...

::

  # find the name of the pod e.g. 'kb-microsoft-mssql-linux-76c8b6967d-p86lw'
  # get access to the file system using the name of the pod e.g.
  kubectl exec -it kb-microsoft-mssql-linux-76c8b6967d-p86lw -- /bin/bash
  # create a folder for your backup files:
  mkdir /var/opt/mssql/backup/

  # list the namespaces in your cluster
  kubectl get pods --all-namespaces
  # copy your backup file to the pod e.g. 'Record.bak'
  # kubectl cp /local/path namespace/podname:path/to/directory
  # e.g.
  kubectl cp ~/repo/backup/Record.bak default/kb-microsoft-mssql-linux-76c8b6967d-p86lw:/var/opt/mssql/backup/

List the logical file names and paths inside the backup.
This is done with the ``RESTORE FILELISTONLY`` Transact-SQL statement::

  export SQLCMDPASSWORD=$MICROSOFT_SQL_SERVER_PASS; \
    sqlcmd -S $MICROSOFT_SQL_SERVER_HOST,$MICROSOFT_SQL_SERVER_PORT -U sa \
    -Q "RESTORE FILELISTONLY FROM DISK = '/var/opt/mssql/backup/Record.bak'"

If you need to, drop the existing database::

  export SQLCMDPASSWORD=$MICROSOFT_SQL_SERVER_PASS; \
    sqlcmd -S $MICROSOFT_SQL_SERVER_HOST,$MICROSOFT_SQL_SERVER_PORT -U sa \
    -Q "DROP DATABASE Record"

Restore the backup::

  export SQLCMDPASSWORD=$MICROSOFT_SQL_SERVER_PASS; \
    sqlcmd -S $MICROSOFT_SQL_SERVER_HOST,$MICROSOFT_SQL_SERVER_PORT -U sa \
    -Q "RESTORE DATABASE Record FROM DISK = '/var/opt/mssql/backup/Record.bak' WITH MOVE 'Record' TO '/var/opt/mssql/data/Record.mdf', MOVE 'Record_log' TO '/var/opt/mssql/data/Record_log.ldf'"

Commands
--------

Run some SQL::

  export SQLCMDPASSWORD=$MICROSOFT_SQL_SERVER_PASS; \
    sqlcmd -S $MICROSOFT_SQL_SERVER_HOST,$MICROSOFT_SQL_SERVER_PORT -U sa

  1> SELECT Name FROM sys.Databases
  2> GO

  1> Use Record
  2> GO
  Changed database context to 'Record'.

  1> SELECT name FROM SYSOBJECTS WHERE xtype='U';
  2> GO

Logging
-------

::

  # find the name of the pod e.g. 'kb-microsoft-mssql-linux-76c8b6967d-p86lw'
  kubectl exec -it kb-microsoft-mssql-linux-76c8b6967d-f24zs -- /bin/bash
  # you now have a command prompt inside the container
  tail -f /var/opt/mssql/log/errorlog

ODBC Connection String
----------------------

The ``Server`` parameter needs to in the format ``172.20.0.2,32200``
(where the IP address of the Microsoft SQL Server is ``172.20.0.2``
and the port is ``32200``)::

  "Driver={ODBC Driver 17 for SQL Server};Server=172.20.0.2,32200;Database=Record;Uid=sa;Pwd=X1oy;"

.. tip:: This format, ``Server=172.20.0.2;Port=32200``, does not seem to work
         for SQL Server using the ``stable/mssql-linux`` helm package.

For help on connection strings, see:
`Using Connection String Keywords with SQL Server`_

Delete Microsoft SQL Server
---------------------------

.. warning:: This deletes Microsoft SQL Server **and data** from your cluster.

::

  helm delete kb-microsoft
  helm del --purge kb-microsoft;

redis
=====

Install k3d_ and Helm_

Change into our Scripts_ folder and run the ``init-redis.sh`` script e.g::

  cd ~/dev/module/k3d/
  ./init-redis.sh

Connect::

  # bash
  export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-redis-master)
  redis-cli -h $NODE_IP -p $NODE_PORT

  # fish
  set -x NODE_IP (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x NODE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-redis-master)
  redis-cli -h $NODE_IP -p $NODE_PORT

Kubernetes
==========

- :doc:`dev-kubernetes`


.. _`GitHub issue, tiller won't install #489`: https://github.com/rancher/k3s/issues/489
.. _`Initialize Helm (Install Tiller)`: https://rancher.com/docs/rancher/v2.x/en/installation/ha/helm-init/
.. _`Install tools on Ubuntu 16.04`: https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools?view=sql-server-ver15#ubuntu
.. _`SQL Server - Linux - Restore Backup`: https://www.pkimber.net/howto/microsoft/dev/sql-server/linux.html#restore-backup
.. _`Using Connection String Keywords with SQL Server`: https://docs.microsoft.com/en-us/sql/relational-databases/native-client/applications/using-connection-string-keywords-with-sql-server-native-client
.. _k3d: https://github.com/rancher/k3d
