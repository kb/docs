Microsoft Office 365
********************

Links
=====

- :doc:`app-msgraph`
- :doc:`sys-azure`
- :doc:`sys-microsoft`

.. _sys-office-365-email:

email
=====

See `Setting up email addresses and shared mailboxes`_ or...

To create an email address for a user, create the user in the Azure portal
(:doc:`sys-azure`), then log into the *Microsoft 365 admin center*
https://portal.office.com/

Assign a couple of licenses:

.. image:: ./misc/2020-11-21-office-365-admin-licenses.png

Clicking on the *Mail* tab and you will see *preparing a mailbox*:

.. image:: ./misc/2020-11-21-office-365-admin-mail-preparing.png

Finally... you will see the *Mail* details...

.. image:: ./misc/2020-11-21-office-365-admin-mail.png

Office 365
----------

To enable OneDrive, add the following licences:

.. image:: ./misc/2021-06-10-office-365-apps.png

Follow a group in Outlook
=========================

If needed in the future you can subscribe to a group from you desktop by
following these instructions: `Subscribe to a group in Outlook`_

Or you can check you are subscribed (and subscribe if you are not subscribed)
by following these instruction (which are what we actually did in the end!)

- Go to Office 365 Online
- In Groups - click the Group name "Office" - This was the group whose emails
  were not being sent to your Inbox
- Click Conversations
- To the RH side click the down arrow by Joined
- Select "Subscribe to this group by email"
- Send a test email to the Group email address.  It should arrive in both your
  Inbox and the Office group

.. note:: if it says "Unsubscribe from emails for this group" then you are not
          unsubscribed so check your desktop version, if subscribed there, call
          us!!

A screen shot for the above instructions to help (I hope):

.. image:: ./misc/office-365.png
   :scale: 60

.. _`Subscribe to a group in Outlook`: https://support.office.com/en-us/article/Subscribe-to-a-group-in-Outlook-e147fc19-f548-4cd2-834f-80c6235b7c36

Setting up email addresses and shared mailboxes
===============================================

Sign in::

  https://portal.office.com/AdminPortal/Home#/homepage

Add a user

- Go to Users/Active Users then "Add a user"
- Create the email address e.g. webmaster@hatherleigh.info

Add Domain

- Click Show all/Setup/Domains/Add domain
- Enter a Domain name e.g. hatherleigh.info
- Create a MX record to match the Office setting given
- Click Verify - this may take a while as the DNS has to propagate
- Go back to Setup/Domains and select the domain e.g. hatherleigh.info
- Click "Check DNS" and if there are any errors add the required records to your
  hosting provider

Modify User

- Now go to Users/Active Users and select a User
  e.g. webmaster@hatherleigh.info.onmicrosoft.com
- Change the Domain Name to the new Domain e.g. hatherleigh.info

Get the free portal offer

- Go to Billing/Purchase services/Non profit
- Choose Office 365 Business Essentials (Nonprofit Staff Pricing) - free
- Pay £0.00 but they collect your card details

Add the App to the Users
- Go to Users/Active Users then select the Users
- Select the Licenses and Apps tab
- Under Licenses tick the Office 365 Business Essential App
- Save the changes and Exit

Shared Mailbox
--------------

*Microsoft 365 admin center*, https://admin.microsoft.com/, *Teams & groups*,
*Shared mailboxes*...

.. image:: ./misc/microsoft/2024-09-03-microsoft-365-shared-mailboxes.jpg

Add *Members* to the mailbox using *Read and manage mailbox permissions*:

.. image:: ./misc/microsoft/2024-09-03-microsoft-365-shared-mailbox-permissions.jpg

You can add the shared mailbox to https://outlook.office.com/ by right clicking on
*Folders*, then *Add shared folder*...

The shared mailbox can also be accessed on their own URL e.g.
https://outlook.office.com/mail/info@hatherleigh.info/

.. tip:: If you create the same mailbox for different domains
         (e.g. info@hatherleigh.info and info@pkimber.net) you will get
         issues. These *can* be solved, by *Edit name*, *Edit email addresses*
         and *Show all*, *Admin centers*, *Exchange*.
