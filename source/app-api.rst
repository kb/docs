api
***

.. highlight:: python

An example API view is ``ActivityLogViewSet``,
https://gitlab.com/kb/api/blob/master/api/views.py

The ``router`` for this API view is here,
https://gitlab.com/kb/api/blob/master/example_api/urls.py

Testing
=======

The ``api`` app has a test fixture called ``api_client``.

An example of usage can be found here,
https://gitlab.com/kb/api/blob/master/api/tests/test_api.py

Django
======

``settings/base.py``::

  THIRD_PARTY_APPS = (
      "rest_framework",
      # http://www.django-rest-framework.org/api-guide/authentication#tokenauthentication
      "rest_framework.authtoken",

``project/urls.py``::

  from rest_framework.authtoken import views
  url(regex=r"^token/$", view=views.obtain_auth_token, name="api.token.auth"),

Ember Error Handler
===================

:ref:`ember-error-handling`
