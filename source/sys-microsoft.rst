Microsoft (Group Policies)
**************************

Links
=====

- :doc:`app-msgraph`
- :doc:`sys-azure`
- :doc:`sys-office-365`

Group Policy
============

Check the Group Policy at the command prompt using::

  gpresult /H <output-filename>.html

You can see if the macro is being pushed out using group policy
(*User Accounts* means it should be applied to every user).
