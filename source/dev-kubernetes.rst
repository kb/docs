Development with Kubernetes
***************************

.. highlight:: bash

- :doc:`dev-kubernetes-install`

Daily Workflow
==============

After Kubernetes is installed (:doc:`dev-kubernetes-install`)...

::

  k3d list
  k3d start k3s-default
  kubectl get pods -w

When the pods are all running, then you can ``source .env.fish`` e.g::

  set folder (basename $PWD)
  source venv-"$folder"/bin/activate.fish
  set -x KUBECONFIG (k3d get-kubeconfig)

  set -x DATABASE_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x DATABASE_NAME dev_app_"$folder"_"$USER"
  set -x DATABASE_PASS "postgres"
  set -x DATABASE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
  set -x DATABASE_USER "postgres"
  # for a project
  # set -x DJANGO_SETTINGS_MODULE "settings.dev_$USER"
  # for an app
  # set -x DJANGO_SETTINGS_MODULE example_"$folder".dev_"$USER"
  set -x REDIS_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x REDIS_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-redis-master)

  source .private

  echo "KUBECONFIG:" $KUBECONFIG
  echo "DATABASE_NAME:" $DATABASE_NAME
  echo "DATABASE_HOST:" $DATABASE_HOST
  echo "DATABASE_PORT:" $DATABASE_PORT
  echo "DATABASE_USER:" $DATABASE_USER
  echo "REDIS_HOST:" $REDIS_HOST
  echo "REDIS_PORT:" $REDIS_PORT
  echo "DJANGO_SETTINGS_MODULE": $DJANGO_SETTINGS_MODULE

k3d
===

::

  # find the names of the clusters on your workstation
  k3d list
  # start a cluster
  k3d start k3s-default

Set the ``KUBECONFIG`` environment variable::

  # bash
  export KUBECONFIG=$(k3d get-kubeconfig)
  # fish
  set -x KUBECONFIG (k3d get-kubeconfig)

Run ``kubectl`` commands e.g::

  kubectl cluster-info

Other Commands
--------------

::

  # delete
  k3d delete k3s-default

kubectl
=======

ConfigMap::

  kubectl get configmaps -o yaml

EndPoints::

  kubectl get endpoints

Ingres::

  kubectl get ing

Pod::

  kubectl get pod
  kubectl get pods --all-namespaces
  kubectl describe pod
  # a specific pod (get the name from the 'get pod' command)
  kubectl describe pod postgres-78b64dc858-d8ktt
  # logs for a pod (get the name from the 'get pod' command)
  kubectl logs postgres-78b64dc858-d8ktt -f
