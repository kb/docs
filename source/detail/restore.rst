Restore (in detail)
*******************

.. highlight:: bash

Click here for the :doc:`../backup` notes...  For general postgres notes, see
:doc:`../sys-postgres`.

.. note:: In the examples below, I am restoring to a ``test`` server.  The
          procedure is more or less the same for a live site.  **Just be more
          careful!!**

.. note:: I am restoring to date folders in the ``~/repo/temp/`` folder
          (``~/repo/temp/2015-04-27-backup/`` and
          ``~/repo/temp/2015-04-27-files/``).  Please change the date to the
          day when you do the work.

.. note:: The database dump file used in this example is ``20150427_0100.sql``.
          Please select the most recent file for your restore.

.. warning:: 11/07/2016, We are using an older version of Duplicity
             (:ref:`install_duplicity_0_6`) which doesn't seem to work with
             spaces in the folder names.  For now we have to restore the whole
             folder!

Database
========

.. note:: For manual backup notes, see :doc:`../sys-postgres`.

On the server as user ``web``, restore the data::

  # download the backup from duplicity
  PASSPHRASE="****" \
  duplicity \
    ssh://123@usw-s011.rsync.net/www.hatherleigh.info/backup \
    /home/web/repo/temp/www.hatherleigh.info/

On the server as user ``web``, backup the current data::

  backup.www.hatherleigh.info.sh full

On the workstation, in the ``fabric`` folder, drop and re-create the database::

  fab domain:::www.hatherleigh.info drop_db:28/07/2016-17:04
  fab domain:::www.hatherleigh.info create_db

On the server as user ``web``, restore the data::

  cd /home/web/repo/temp/www.hatherleigh.info/
  psql -U www_hatherleigh_info -d www_hatherleigh_info -f 20160304_0004.sql 2> out.log

.. tip:: Check the ``out.log`` file for import issues.

Files
=====

On the server as user ``web``::

  # download the files from duplicity
  PASSPHRASE="****" \
  duplicity \
    ssh://123@usw-s011.rsync.net/hatherleigh_info/files \
    /home/web/repo/temp/2015-04-27-files/

  # restore the duplicity files
  cd ~/repo/files/hatherleigh_info/
  # move (or remove) the existing public and private folders before replacing
  # with the restored folders:
  mv ~/repo/temp/2015-04-27-files/private .
  mv ~/repo/temp/2015-04-27-files/public .
