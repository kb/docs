LetsEncrypt - DNS
*****************

Out Salt states will have installed the LetsEncrypt package.

.. note:: ``letsencrypt-auto`` has been renamed to ``certbot-auto`` on Ubuntu.

.. note:: ``certbot-auto`` has been renamed to ``certbot`` on Ubuntu.

On your server::

  sudo -i
  # replace 'pypi.myserver.co.uk' with your domain name
  certbot certonly --manual -d pypi.myserver.co.uk --preferred-challenges "dns"

You will be asked to::

  Please deploy a DNS TXT record under the name
  _acme-challenge.pypi.myserver.co.uk with the following value:

Create the ``TXT`` record in your DNS e.g:

.. image:: ../misc/lets-encrypt-dns-txt.png

.. tip:: When deploying to Digital Ocean, enter the *Hostname* without the
         domain e.g. ``_acme-challenge.pypi``

To *verify the record is deployed*, *Before continuing*::

  dig -t txt _acme-challenge.pypi.myserver.co.uk +short

.. note:: I don't know if this works with our auto-renewal script.
