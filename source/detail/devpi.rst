devpi (in detail)
*****************

If you are creating your own SSL certificate (:doc:`../ssl`):

Copy the SSL certificate from your workstation to the server::

  scp server.key devpi.yourbiz.co.uk:/home/patrick/repo/temp/
  scp ssl-unified.crt devpi.yourbiz.co.uk:/home/patrick/repo/temp/

Create the folder for the certificates and update permissions::

  ssh devpi.yourbiz.co.uk
  sudo -i
  mkdir -p /srv/ssl/devpi/
  chown www-data:www-data /srv/ssl
  chmod 0400 /srv/ssl
  chown www-data:www-data /srv/ssl/devpi
  chmod 0400 /srv/ssl/devpi

  mv /home/patrick/repo/temp/server.key /srv/ssl/devpi/
  mv /home/patrick/repo/temp/ssl-unified.crt /srv/ssl/devpi/
  chown www-data:www-data /srv/ssl/devpi/*
  chmod 0400 /srv/ssl/devpi/*

Re-start the nginx server::

  service nginx restart
