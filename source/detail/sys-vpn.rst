VPN (Detail)
************

.. highlight:: bash

Links

- :ref:`wireguard-troubleshooting`
- https://www.stavros.io/posts/how-to-configure-wireguard/
- https://wiki.archlinux.org/index.php/WireGuard#Key_generation
- https://www.wireguard.com/quickstart/
- https://wiki.debian.org/Wireguard

Testing on ``kb-vpn`` server and *X220* laptop...

Create a Droplet on Digital Ocean (smallest one possible).

``ssh`` into the server, then ``apt update`` and ``apt upgrade``

.. tip:: The following needs to be done on all the peers (client and server)

Install::

  sudo -i
  apt install wireguard

Create keys::

  cd /etc/wireguard/
  umask 077
  wg genkey | tee privatekey | wg pubkey > publickey

.. tip:: ``umask 077`` sets the default for files created from this point.

Create ``/etc/wireguard/wg0.conf``::

  vim /etc/wireguard/wg0.conf

In ``/etc/wireguard/wg0.conf`` on the server:

.. code-block:: ini

  [Interface]
  Address = 10.10.2.1
  PrivateKey = <server's privatekey>
  ListenPort = 51820

  [Peer]
  PublicKey = <client1's publickey>
  AllowedIPs = 10.10.2.2/32

  [Peer]
  PublicKey = <client2's publickey>
  AllowedIPs = 10.10.2.3/32

On the server, ``/etc/sysctl.conf``::

  # Uncomment the next line to enable packet forwarding for IPv4
  net.ipv4.ip_forward=1

In ``/etc/wireguard/wg0.conf`` on the clients::

  [Interface]
  Address = 10.10.2.2
  PrivateKey = <client's privatekey>
  # 13/11/2020, Malcolm thinks we don't need a 'ListenPort' on the client
  # ListenPort = 51820

  [Peer]
  PublicKey = <server's publickey>
  Endpoint = <server's ip>:51820
  AllowedIPs = 10.10.2.0/24

  # This is for if you're behind a NAT and want the connection to be kept alive.
  PersistentKeepalive = 25

To test the VPN, run the following::

  wg-quick up wg0
  # to take the interface down
  wg-quick down wg0

Auto-start the service::

  systemctl enable wg-quick@wg0.service

To start or stop the service::

  sudo systemctl start wg-quick@wg0.service
  sudo systemctl stop wg-quick@wg0.service

Troubleshooting
===============

:ref:`wireguard-troubleshooting`

.. Older notes...
.. Create the ``wireguard`` network interface::
..   ip link add dev wg0 type wireguard
.. Decide on an IP address for the LAN of the VPN e.g. my home network is
.. ``192.168.1``, so my VPN network could be ``192.168.50``.
.. Assign the IP address to the network interface e.g::
..   # e.g. for the first peer
..   ip address add dev wg0 192.168.50.1/24
..   # e.g. for the second peer
..   ip address add dev wg0 192.168.50.2/24
.. Tell the peers about each other,
.. From https://wiki.archlinux.org/index.php/WireGuard#Peer_A_setup
.. ::
..   # Note, the default port is 51820
..   # peer 1 (server in our example)
..   wg set wg0 listen-port 48574 private-key ./privatekey
..   wg set wg0 peer [Peer B public key] persistent-keepalive 25 allowed-ips 192.168.50.2/32
..   ip link set wg0 up
..   # peer 2
..   wg set wg0 listen-port 39814 private-key ./privatekey
..   wg set wg0 peer [Peer A public key] persistent-keepalive 25 allowed-ips 192.168.50.1/32 endpoint 10.10.10.1:48574
..   ip link set wg0 up
.. .. note:: The ``endpoint`` is the external IP address of our server.
.. From,
.. https://www.ckn.io/blog/2017/11/14/wireguard-vpn-typical-setup/
.. Add the following to your ``iptables`` config::
..   -A INPUT -p udp -m udp --dport 48574 -m conntrack --ctstate NEW -j ACCEPT
.. To dislay the config::
..   wg showconf wg0
.. Save the config::
..   wg showconf wg0 > /etc/wireguard/wg0.conf
