SEO
***

.. tip:: From
         `English Google Webmaster Central office-hours hangout, 05/04/2019`_:

         *What information do you have that users are interested in searching
         for and how can you provide it in a way that answers their needs and
         helps them to move forward?*

         *Is this something that (Google would) want to recommend to other
         users?*

Links
=====

- Your ultimate SEO health check guide – How to climb the ranks:
  http://www.growthbusiness.co.uk/ultimate-seo-health-check-guide-2552724/ 
- How to Build SEO Strategies Effectively (and Make Them Last):
  https://moz.com/blog/seo-strategy 

Basic Checks
============

Title tags and meta descriptions

- Avoid duplication in your title tags.
- Aim to have a title and description under 55 and 155 characters respectively.

On-page copy

- Aim for a word count of 300+ words.
- Do NOT duplicate content

Page load times

- https://developers.google.com/speed/pagespeed/insights/
- Check the size of your images

Mobile friendly

- https://search.google.com/test/mobile-friendly 

Check for broken pages

- Google Search Console: Navigate to Crawl, Crawl Errors, “Not Found”

HTTPS vs HTTP

Pop-Ups
=======

The `English Google Webmaster Central office-hours hangout, 22/02/2019`_ talks
about interstitial (pop-up) dialogs resulting in lower SEO rankings.

interstitial
  an advertisement that appears while a chosen website or page is downloading.

Redirect
========

If Google finds a persistent 302 (temporary redirect) for a long period of time,
it will start treating it like a permanent redirect.
(from `English Google Webmaster Central office-hours hangout, 22/02/2019`_).

Sub-Domains
===========

Sub-domains are effectively treated as separate sites for the purposes of SEO:

Here's Why Your Site Structure Matters for SEO by Eric Enge:
https://www.youtube.com/watch?v=xvSuaVuJuJM


.. _`English Google Webmaster Central office-hours hangout, 22/02/2019`: https://www.youtube.com/watch?v=ZZMerzi8eZ8
.. _`English Google Webmaster Central office-hours hangout, 05/04/2019`: https://youtu.be/oCNi7dTircw?t=823
