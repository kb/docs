GDPR
****

.. highlight:: python

Icon::

  <i class="fa fa-shield"></i>

Requirements::

  # until we get to python 3.6 on our servers, add to 'requirements/ci.txt'
  python2-secrets==1.0.5

Consent List for a User
=======================

The ``_user_consent_list.html`` template, expects:

1. The ``object`` to have a ``user`` object (i.e. for ``object.user.pk``)
2. A view which redirects from a user ``pk`` to a contact detail view:
   :ref:`user_contact_redirect_view`

To list the consent records for a contact / user::

  from gdpr.models import UserConsent

  def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      context.update(
          {
              "consent_list": UserConsent.objects.for_user(
                  self.object.user
              ).order_by("consent__name")
          }
      )
      return context

In the template:

.. code-block:: html

  {% include 'gdpr/_user_consent_list.html' with consent_user=object.user %}

.. note:: Replace ``object.user`` with the user from your template.

Form Notice
===========

The ``ConsentFormSettings`` model is (currently) only used to add a notice to a
form.  This notice can include a URL (and other HTML tags).

To set-up the form settings:

1. *Settings*, *GDPR*, *Form Settings*
2. Choose a slug for your form e.g. ``generic-form``
3. Add the text for the *Form notice*.

You can use the rich text editor to include HTML in the form notice e.g:

.. code-block:: html

  <strong>Take a look</strong> at our
  <a href="/article/ilspas-privacy-policy/" target="_blank" rel="noopener noreferrer">
    Privacy Notice
  </a>
  for more information.

For enquiry forms, inherit from ``EnquiryCreateMixin``::

  class EnquiryCreateView(EnquiryCreateMixin, CreateView):

For other forms, inherit from ``ConsentMaintenanceMixin`` e.g::

  class ApplicationCreateView(ConsentMaintenanceMixin, CreateView):

In your view, set ``consent_form_settings`` to the form settings slug e.g::

  class ApplicationCreateView(ConsentMaintenanceMixin, CreateView):
      consent_form_settings = "generic-form"

If your template includes our standard ``_form.html`` it will automatically
display the ``form_notice``.

.. _gdpr_unsubscribe:

Unsubscribe
===========

To create an *unsubscribe* page...

Create the view::

  from base.view_utils import BaseMixin, RedirectNextMixin
  from gdpr.views import UserConsentUnsubscribeUpdateMixin

  class UserConsentUnsubscribeUpdateView(
      RedirectNextMixin, UserConsentUnsubscribeUpdateMixin, BaseMixin, UpdateView
  ):
      template_name = "example/user_consent_unsubscribe.html"

The URL for this view must capture the ``token`` e.g::

    path(
        "unsubscribe/<str:token>/",
        view=UserConsentUnsubscribeUpdateView.as_view(),
        name="web.unsubscribe",
    ),

Create a template which includes ``_unsubscribe.html``::

  {% include 'gdpr/_unsubscribe.html' %}

.. tip:: This template may display a link for the *Contact* page of your site,
         so check your project has a ``web.contact`` URL.

To create an unsubscribe URL and pass it to an email template::

  with transaction.atomic():
      token = UserConsentUnsubscribe.objects.create_unsubscribe_token(
          contact.user, SPECIAL_OFFERS
      )
      url = urllib.parse.urljoin(
          settings.HOST_NAME, reverse("web.unsubscribe", args=[token])
      )
      context = {
          email: {
              "name": contact.full_name,
              "unsub": url,
          }
      }
      queue_mail_template(
          contact, MAIL_TEMPLATE_NOTIFY, context
      )

.. tip:: Click here for information on adding the unsubscribe URL to the
         :ref:`mandrill_template`.
