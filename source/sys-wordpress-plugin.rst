WordPress - Plugins
*******************

- :doc:`sys-swap-space`
- :doc:`sys-wordpress`
- :doc:`sys-wordpress-issues`
- :doc:`sys-wordpress-security`
- :doc:`sys-wordpress-transfer`
- :doc:`sys-wordpress-update`
- :doc:`sys-wordpress-woocommerce`
- :doc:`app-veeqo`
- :doc:`app-woocommerce`

.. tip:: Also see :doc:`sys-wordpress-woocommerce` for WooCommerce plugins.

My Private Site
===============

My Private Site Getting Started
https://www.youtube.com/watch?v=5bW88-4BlF4

MC4WP - Mailchimp Plugin
========================

This is a plugin to intergrate mailchimp with WordPress

Simply install the plugin, visit your mailchimp account

Grab an API key from accounts/api

Then post the api key into the mailchip settings on the wordpress dashboard

You should then get a mailing list in your forms across wordpress with the name of what account you've chosen

Simply select that list and any email you sign up with will be added onto that list.

Yoast SEO
=========

Post to Facebook
----------------

This is a fantastic video which explains everything:
`Facebook Preview – Posting Links Correctly (using Yoast SEO and Facebook Debugger)`_

Check the site configuration in *SEO*, *Social - Yoast SEO*:

- *Facebook Page URL*
- On the *Facebook* tab, check *Open Graph meta data* is enabled.

WordPress Post

1. Fill in all the Facebook information on the post
   (including the *Facebook description*)
2. Publish the post (before posting to Facebook).
3. Get the URL of the post by browsing to the page on the website or copying
   the permalink at the top of the post.
4. Create a new post in Facebook and paste in the URL.

If you have any issues, post the URL into the Facebook, *Sharing Debugger*:
https://developers.facebook.com/tools/debug/


.. _`Facebook Preview – Posting Links Correctly (using Yoast SEO and Facebook Debugger)`: https://www.youtube.com/watch?v=DHapW6mKCXA
