PDF Object
**********

.. highlight:: html

Using https://pdfobject.com/

Usage
=====

.. note:: In this example, I am using ``pdf-viewer`` as the ID of my ``div``.

Create a container to hold your PDF::

  <div id="pdf-viewer"></div>

Add the following to your form template (probably below the
``{% endblock content %}`` block)::

  {% block script_extra %}
    {{ block.super }}
    {% if flow_link.pdf_url %}
      {% include 'base/_pdfobject.html' %}
      <script>PDFObject.embed("{{ flow_link.pdf_url }}", "#pdf-viewer");</script>
      <style>
        .pdfobject-container { height: 800px;}
        .pdfobject { border: 1px solid #666; }
      </style>
    {% endif %}
  {% endblock script_extra %}

.. note:: In this example, the ``flow_link.pdf_url`` variable contains the URL
          of a PDF.
