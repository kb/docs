Digital Ocean
*************

Managed Databases
=================

Connection
----------

Copy the *Connection string*...

.. image:: ./misc/digital-ocean-db-connection-string.png

Use the *Connection string* in ``psql`` as follows::

  psql "postgresql://doadmin:k01@kb-user-17.db.ondigitalocean.com:25060/defaultdb?sslmode=require"

.. note:: You can *Download the CA certificate*, but we didn't need to use it...

.. tip:: Make sure you click on the *Public network* **and** ``show-password``
         (in the *Connection string*).
         I forgot both of these things...!

Security
--------

Security is managed by adding a *Trusted source*.
A *Trusted source* is the IP address of your laptop (or workstation):

.. image:: ./misc/digital-ocean-db-trusted-sources.png

Data Center Locations
=====================

.. image:: ./misc/2024-05-14-digital-ocean-data-center-locations.png

Project
=======

One of our servers was in the wrong project.  We could not see a way to remove
it, so created another project added it to the new project.

.. note:: The Digital Ocean API does not include the project name in the
          ``droplets`` API call, so we use the ``tags`` to identify the
          *owner* of the droplet.

PTR
===

email conversation with support@digitalocean.com ref an *unknown* ticket

  I understand that the PTR records are not published for host IP 178.62.14.86.

  We automatically create PTR records for Droplets based on the name you give
  that Droplet in the control panel. The name must be a valid FQDN, so using
  ``example.com`` it as the Droplet name will create a PTR record, but
  ``ubuntu-s-4vcpu-8gb-fra1-01`` or ``my-droplet`` will not.

  Droplets with IPv6 enabled will only have PTR records enabled for the first
  IPv6 address assigned to it, not to all 16 addresses available.

  Please rename the Droplet ``kb-vpn`` to ``vpn.kb.com`` and our systems will
  automatically create a PTR record.

  Once done check the status of the DNS propagation here for the PTR record
  here, https://www.whatsmydns.net/

.. tip:: Don't forget to select ``PTR`` in the dropdown on the whatsmydns.net
         page.

I appreciate you for following up with us.

  I understand your concerns here. Please note that we automatically configure
  the reverse DNS entry (PTR record) on our end, based on the hostname of your
  droplet. Currently, we do not offer direct access for users to modify PTR
  records via DNS. This is the only method available at present to create a
  PTR record on our platform.
