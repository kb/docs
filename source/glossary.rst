Glossary
********

RFC
  Defines that a request is extra work not part if the original scope.
  There are loads of terms for this. RFC, variation (i.e. v to order),
  additional work, scope change, compensation event etc...
  it often depends on the contract type.
  Its more about that a change is related to an existing order than a new order.
