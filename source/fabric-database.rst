Database
********

.. highlight:: bash

If you are ready to deploy a Django site, start by creating a database...

Create
======

.. note::

  Make sure the Salt pillar is configured correctly for the site.  You need to
  include the database password and a folder containing the database settings.

  For details, see :doc:`site-config`

For MySQL::

  ssh the.server.ip.address
  sudo -i
  mysql
  mysql> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '';

For details, see:

- `MySQL Error: : 'Access denied for user 'root'@'localhost'`_
- `Allow linux root user mysql root access without password`_

To create a database for a site::

  cd fabric
  fab create-db domain:www.hatherleigh.info

  # if your would like to use a Postgres table space name
  fab domain:www.hatherleigh.info create-db:cbs

To create a workflow database for a site::

  fab domain:www.hatherleigh.info create-db-workflow

You are now ready to :doc:`fabric-deploy` your site...

Drop/Delete
===========

To drop a database, you will need to append the current date and time e.g::

  fab domain:www.hatherleigh.info drop_db:02/02/2015-16:54

To drop a database role::

  fab domain:www.hatherleigh.info drop_db_role

.. note:: This command should fail if the role is in use.

Maintenance
===========

- :doc:`backup`
- :doc:`dev-restore`


.. _`Allow linux root user mysql root access without password`: https://serverfault.com/questions/563714/allow-linux-root-user-mysql-root-access-without-password
.. _`MySQL Error: : 'Access denied for user 'root'@'localhost'`: https://stackoverflow.com/questions/41645309/mysql-error-access-denied-for-user-rootlocalhost
