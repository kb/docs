Documentation
*************

To build the documentation, make sure you ``uv`` is installed,
https://docs.astral.sh/uv/getting-started/installation/

::

  uv run make html

Open the documentation::

  firefox build/html/index.html &

To deploy the documentation::

  ./deploy.sh
